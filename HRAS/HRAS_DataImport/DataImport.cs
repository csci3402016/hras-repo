﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using HRAS_Middleware;

namespace HRAS_DataImport
{
    class DataImport : DataControl
    {
        private static SqlConnection con = new SqlConnection(connectionString);

        static void Main(string[] args)
        {
            string rooms = "Rooms.txt";                         // These files are in ...\HRAS_DataImport\bin\Debug
            string medicalRecord = "MedicalRecords.txt";
            string inventory = "Inventory.txt";

            con.Open();
            importRooms(rooms);
            importSymptomsAndPatients(medicalRecord);
            importMedicalRecords(medicalRecord);
            importInventory(inventory);
            con.Close();
        }

        public static void importRooms(string rooms)
        {
            String roomNumber;
            double roomRate;          
            DateTime efectiveDate;
            String completeLine;

            try
            {
                StreamReader inFile = new StreamReader(rooms);

                while ((completeLine = inFile.ReadLine()) != null)
                {
                    roomNumber = completeLine.Substring(0, 9);
                    roomRate = Double.Parse(completeLine.Substring(9, 5)) / 100; //$$$.cc
                    efectiveDate = new DateTime(Int32.Parse(completeLine.Substring(18, 4)), 
                        Int32.Parse(completeLine.Substring(14, 2)), Int32.Parse(completeLine.Substring(16, 2)), 0, 0, 0);

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "InsertRoom";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    cmd.Parameters.Add("@Room_Number", SqlDbType.VarChar).Value = roomNumber;
                    cmd.ExecuteNonQuery();

                    SqlCommand cmd2 = new SqlCommand();
                    cmd2.CommandText = "InsertRoomRate";
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.Connection = con;
                    cmd2.Parameters.Add("@Effective_Date", SqlDbType.DateTime).Value = efectiveDate;
                    cmd2.Parameters.Add("@Room_Number", SqlDbType.VarChar).Value = roomNumber;
                    cmd2.Parameters.Add("@Hourly_Rate", SqlDbType.Real).Value = roomRate;
                    cmd2.ExecuteNonQuery();

                    //testing
                    //Console.WriteLine(roomNumber + " " + roomRate + " " + efectiveDate);//
                }
                inFile.Close();
            }
            catch (IOException e) { throw new Exception("Error opening or closing file: " + rooms); }
        }

        public static String removeExtraSpaces(String oldString)
        {
            String newString = "";
            if (oldString[0] != ' ') newString += oldString[0];
            for (int i = 1; i < oldString.Length - 1; i++)
            {
                if (oldString[i] != ' ') newString += oldString[i];
                else if ((oldString[i - 1] != ' ') && (oldString[i + 1] != ' ')) newString += oldString[i];
            }
            if (oldString[oldString.Length - 1] != ' ') newString += oldString[oldString.Length - 1];
            return newString;
        }

        public static void importSymptomsAndPatients(string medicalRecord)
        {
            PatientHandler handler = new PatientHandler();
            Patient patient = new Patient();
            List<String> uniquePatientSSNs = new List<String>();
            List<String> uniqueSymptoms = new List<String>();
            String[] symptoms = new String[6];
            String patientSSN;
            String completeLine;
            bool unique;
            //int lineCountTest = 0;//test

            try
            {
                StreamReader inFile = new StreamReader(medicalRecord);

                while ((completeLine = inFile.ReadLine()) != null)
                {
                    patientSSN = completeLine.Substring(77, 9);
                    //lineCountTest++;//test

                    unique = true;
                    for (int i = 0; i < uniquePatientSSNs.Count; i++)
                    {
                        if (patientSSN == uniquePatientSSNs[i])
                        {
                            unique = false;
                            break;
                        }
                    }
                    if (unique)
                    {
                        uniquePatientSSNs.Add(patientSSN);

                        patient.SSN = patientSSN; 
                        patient.lastName = removeExtraSpaces(completeLine.Substring(0, 50));
                        patient.firstName = removeExtraSpaces(completeLine.Substring(50, 25));
                        patient.middleInitial = (completeLine.Substring(75, 1))[0];
                        if (completeLine.Substring(76, 1).ToUpper() == "M") patient.gender = 'M';
                        else patient.gender = 'F';         
                        patient.birthDate = DateTime.ParseExact(completeLine.Substring(86, 8), "MMddyyyy", new CultureInfo("en-US"));
                        patient.address = removeExtraSpaces(completeLine.Substring(454, 35));
                        patient.address2 = removeExtraSpaces(completeLine.Substring(489, 4) + completeLine.Substring(523, 1));
                        patient.city = removeExtraSpaces(completeLine.Substring(524, 25));
                        patient.state = completeLine.Substring(549, 2);
                        patient.zipCode = completeLine.Substring(551, 5);
                        if (completeLine.Substring(557, 1).ToUpper() == "Y") patient.organDonor = 'Y';
                        else patient.organDonor = 'N';

                        handler.createPatient(patient);

                        //testing
                        /*Console.WriteLine(patient.lastName + " " + patient.firstName + " " + patient.middleInitial + " " + patient.gender + " " + patient.SSN + " " + patient.birthDate + " " +
                             patient.address + " " + patient.address2 + " " + patient.city + " " + patient.state + " " + patient.zipCode + " " + patient.organDonor);*/               
                    }

                    symptoms[0] = removeExtraSpaces(completeLine.Substring(124, 25));
                    symptoms[1] = removeExtraSpaces(completeLine.Substring(149, 25));
                    symptoms[2] = removeExtraSpaces(completeLine.Substring(174, 25));
                    symptoms[3] = removeExtraSpaces(completeLine.Substring(199, 25));
                    symptoms[4] = removeExtraSpaces(completeLine.Substring(224, 25));
                    symptoms[5] = removeExtraSpaces(completeLine.Substring(249, 25));
                
                    for (int i = 0; i < 6; i++)
                    {
                        unique = true;              
                        for (int j = 0; j < uniqueSymptoms.Count; j++)
                        {
                            if (symptoms[i] == uniqueSymptoms[j])
                            {
                                unique = false;
                                break;
                            }
                        }
                        if (unique)
                        {
                            uniqueSymptoms.Add(symptoms[i]);

                            SqlCommand cmd = new SqlCommand();
                            cmd.CommandText = "InsertSymptom";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Connection = con;
                            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = symptoms[i];
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                inFile.Close();

                //testing
                //for (int j = 0; j < uniqueSymptoms.Count; j++) Console.WriteLine(uniqueSymptoms[j]);//             
                //Console.WriteLine(uniqueSymptoms.Count + " unique symptoms in doc.");// 
                                
                //for (int j = 0; j < uniquePatientSSNs.Count; j++) Console.WriteLine(uniquePatientSSNs[j]);//             
                //Console.WriteLine(uniquePatientSSNs.Count + " unique patients in doc.\n" + lineCountTest + " total patient entries in doc.");// 
            }
            catch (IOException e) { throw new Exception("Error opening or closing file: " + medicalRecord); }
        }

        public static void importMedicalRecords(string medicalRecord)
        {
            Patient patient = new Patient();
            MedicalRecord record = new MedicalRecord();
            PatientHandler handler = new PatientHandler();
            Symptom[] symptoms = 
            {
                new Symptom(),
                new Symptom(),
                new Symptom(),
                new Symptom(),
                new Symptom(),
                new Symptom()
            };
            String completeLine;

            try
            {
                StreamReader inFile = new StreamReader(medicalRecord);

                while ((completeLine = inFile.ReadLine()) != null)
                {
                    patient.SSN = completeLine.Substring(77, 9);
                    record.entryDate = new DateTime(Int32.Parse(completeLine.Substring(98, 4)),
                        Int32.Parse(completeLine.Substring(94, 2)), Int32.Parse(completeLine.Substring(96, 2)), 0, 0, 0);
                    record.exitDate = new DateTime(Int32.Parse(completeLine.Substring(106, 4)),
                        Int32.Parse(completeLine.Substring(102, 2)), Int32.Parse(completeLine.Substring(104, 2)), 0, 0, 0);
                    record.physician = completeLine.Substring(110, 5);
                    record.roomNumber = completeLine.Substring(115, 9);

                    symptoms[0].name = removeExtraSpaces(completeLine.Substring(124, 25));
                    symptoms[1].name = removeExtraSpaces(completeLine.Substring(149, 25));
                    symptoms[2].name = removeExtraSpaces(completeLine.Substring(174, 25));
                    symptoms[3].name = removeExtraSpaces(completeLine.Substring(199, 25));
                    symptoms[4].name = removeExtraSpaces(completeLine.Substring(224, 25));
                    symptoms[5].name = removeExtraSpaces(completeLine.Substring(249, 25));
                    record.getSymptoms().Clear();
                    if (!record.getSymptoms().Contains(symptoms[0])) record.addSymptom(symptoms[0]);
                    if (!record.getSymptoms().Contains(symptoms[1])) record.addSymptom(symptoms[1]);
                    if (!record.getSymptoms().Contains(symptoms[2])) record.addSymptom(symptoms[2]);
                    if (!record.getSymptoms().Contains(symptoms[3])) record.addSymptom(symptoms[3]);
                    if (!record.getSymptoms().Contains(symptoms[4])) record.addSymptom(symptoms[4]);
                    if (!record.getSymptoms().Contains(symptoms[5])) record.addSymptom(symptoms[5]);

                    record.diagnosis = removeExtraSpaces(completeLine.Substring(274, 75));
                    record.notes = removeExtraSpaces(completeLine.Substring(349, 100));
                    record.insurer = completeLine.Substring(449, 5);
                    if (completeLine.Substring(556, 1).ToUpper() == "Y") record.doNotResuscitate = true;
                    else record.doNotResuscitate = false;

                    if (!isMedicalRecordInTheDB(patient, record) && isRoomInTheDB(record))
                    {
                        handler.createMedicalRecord(patient, record);
                    }

                    //testing 
                    /*Console.WriteLine(patient.SSN + " " + record.entryDate + " " + record.exitDate + " " + record.physician + " " + record.roomNumber + " " +
                        record.getSymptoms()[0].name + " " + record.getSymptoms()[1].name + " " + record.getSymptoms()[2].name + " " + record.getSymptoms()[3].name + " " +
                        record.getSymptoms()[4].name + " " + record.getSymptoms()[5].name + " " + record.diagnosis + " " + record.notes + " " + record.insurer + " " +
                        record.doNotResuscitate);*/
                }
                inFile.Close();
            }
            catch (IOException e) { throw new Exception("Error opening or closing file: " + medicalRecord); } 
        }

        public static void importInventory(string inventory)
        {
            InventoryItem itemToAdd = new InventoryItem();
            ItemHandler itemControl = new ItemHandler();

            string completeLine;
            string quantity;
            string size;
            string cost;
            double realCost;
            int amount;
            double doubleSize;

            try
            {
                StreamReader inFile = new StreamReader(inventory);

                while ((completeLine = inFile.ReadLine()) != null)
                {
                    itemToAdd.id = completeLine.Substring(0, 5);
                    quantity = completeLine.Substring(5, 5);
                    itemToAdd.description = removeExtraSpaces(completeLine.Substring(10, 35));
                    size = completeLine.Substring(45, 3);
                    cost = completeLine.Substring(48, 8);
                    itemToAdd.isVIRTL = false;

                    if (quantity.ToUpper() == "VIRTL")
                    {
                        itemToAdd.isVIRTL = true;
                    }

                    else itemToAdd.quantity = Int32.Parse(quantity);

                    doubleSize = double.Parse(size);
                    realCost = double.Parse(cost);
                    realCost = realCost / 100;
                    //toAdd.quantity = amount;
                    itemToAdd.size = doubleSize;
                    itemToAdd.cost = realCost;

                    itemControl.createItem(itemToAdd);

                    //testing
                    //Console.WriteLine(toAdd.id + " " + toAdd.quantity + " " + toAdd.description + " " + toAdd.size + " " + toAdd.cost);//
                }
                inFile.Close();
            }
            catch (IOException e) { throw new Exception("Error opening or closing file: " + inventory); }
        }

        public static bool isRoomInTheDB(MedicalRecord m)
        {
            bool isRoomInTheDB = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = "SelectRoomByNumber";
            cmd.Parameters.Add("@Room_Number", SqlDbType.VarChar).Value = m.roomNumber;
            cmd.CommandType = CommandType.StoredProcedure;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    isRoomInTheDB = true;
                }
            }
            return isRoomInTheDB;
        }

        public static bool isMedicalRecordInTheDB(Patient p, MedicalRecord m)
        {
            bool isMedicalRecordInTheDB = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            cmd.CommandText = "SelectMedicalRecordByID";
            cmd.Parameters.Add("@Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = m.entryDate;
            cmd.CommandType = CommandType.StoredProcedure;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    isMedicalRecordInTheDB = true;
                }
            }
            return isMedicalRecordInTheDB;
        }
    }
}