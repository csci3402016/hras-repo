﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRAS_Middleware
{
    public class AuditInfo
    {
        public string ssn { get; set; }
        public DateTime entryDate { get; set; }
        public DateTime dateChanged { get; set; }
        public string username { get; set; }
        public string field_changed { get; set; }
        public string prviousValue { get; set; }
        public string newValue { get; set; }
        
    }
}
