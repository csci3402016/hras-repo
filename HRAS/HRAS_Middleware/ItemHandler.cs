﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRAS_Middleware
{
    public class ItemHandler
    {
       
        public void createItem(InventoryItem newItem)
        {

            if(DataControlInventoryItem.isItemInTheDb(newItem))
            {
                throw new Exception("Item is already in the database");
            }
            else
            {
                DataControlInventoryItem.insertItem(newItem);
            }
                            
        }
        public void editItem(InventoryItem item)
        {
            //data massaging here

            DataControlInventoryItem.updateItem(item);
        }
        public List<InventoryItem> searchForItems(InventoryItem item)
        {
            //data massaging here

            return DataControlInventoryItem.getItems(item);
        }

        public List<InventoryItem> getItemsUsedByStay(MedicalRecord medRec, Patient toPass)
        {
            return DataControlInventoryItem.getItemsUsed(medRec, toPass);
        }
    }
}
