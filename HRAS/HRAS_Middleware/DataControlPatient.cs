﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace HRAS_Middleware
{
    internal class DataControlPatient : DataControl
    {
        public static void updateMedicalRecord(MedicalRecord m, Patient p)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "UpdateMedicalRecord";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = m.entryDate;
            if (m.exitDate != null) cmd.Parameters.Add("@NewExit_Date", SqlDbType.DateTime).Value = m.exitDate;
            cmd.Parameters.Add("@NewAttending_Physician", SqlDbType.VarChar).Value = m.physician;
            if (!m.diagnosis.Equals("")) cmd.Parameters.Add("@NewDiagnosis", SqlDbType.VarChar).Value = m.diagnosis;
            if (!m.notes.Equals("")) cmd.Parameters.Add("@NewNotes", SqlDbType.VarChar).Value = m.notes;
            if (!m.insurer.Equals("")) cmd.Parameters.Add("@NewInsurer", SqlDbType.VarChar).Value = m.insurer;
            cmd.Parameters.Add("@DNR", SqlDbType.Bit).Value = m.doNotResuscitate;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            
        }

        public static void updatePatient(Patient p)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "UpdatePatient";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@NewFirst_Name", SqlDbType.VarChar).Value = p.firstName;
            cmd.Parameters.Add("@NewLast_Name", SqlDbType.VarChar).Value = p.lastName;
            cmd.Parameters.Add("@NewMiddle_Initial", SqlDbType.Char).Value = p.middleInitial;           
            cmd.Parameters.Add("@NewBirth_Date", SqlDbType.DateTime).Value = p.birthDate;
            cmd.Parameters.Add("@NewAddress_Line_1", SqlDbType.VarChar).Value = p.address;
            if (!p.address2.Equals("")) cmd.Parameters.Add("@NewAddress_Line_2", SqlDbType.VarChar).Value = p.address2;
            cmd.Parameters.Add("@NewAddress_City", SqlDbType.VarChar).Value = p.city;
            cmd.Parameters.Add("@NewAddress_State", SqlDbType.Char).Value = p.state;
            cmd.Parameters.Add("@NewAddress_Zip_Code", SqlDbType.Char).Value = p.zipCode;

            if (p.gender.Equals('M') || p.gender.Equals('m')) cmd.Parameters.Add("@NewGender", SqlDbType.Bit).Value = true;
            else cmd.Parameters.Add("@NewGender", SqlDbType.Bit).Value = false; ;

            if (p.organDonor.Equals('Y') || p.organDonor.Equals('y')) cmd.Parameters.Add("@Neworgan_Donor", SqlDbType.Bit).Value = true;
            else cmd.Parameters.Add("@Neworgan_Donor", SqlDbType.Bit).Value = false;


            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            
        }

        public static bool isRoomInTheDB(MedicalRecord m)
        {
            bool isRoomInTheDB = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SelectRoomByNumber";
            cmd.Parameters.Add("@Room_Number", SqlDbType.VarChar).Value = m.roomNumber;

            cmd.CommandType = CommandType.StoredProcedure;
            

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    isRoomInTheDB = true;
                }
        
            }
            return isRoomInTheDB;
        }

        public static bool isSymptomInTheDB(Symptom s)
        {
            bool isSymptomInTheDB = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SelectSymptomByName";
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = s.name;

            cmd.CommandType = CommandType.StoredProcedure;
            

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    isSymptomInTheDB = true;
                }
                
            }
            return isSymptomInTheDB;
        }

        public static void insertSymptom(Symptom s)
        {
            SqlCommand cmd = new SqlCommand();

            //insert Symptom
            cmd.CommandText = "InsertSymptom";
            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = s.name;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public static bool isPatientInTheDB(Patient p)
        {
            bool isPatientInTheDB = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SelectPatientBySSN";
            cmd.Parameters.Add("@SSN", SqlDbType.Char).Value = p.SSN;

            cmd.CommandType = CommandType.StoredProcedure;
            

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    isPatientInTheDB = true;
                }
                
            }
            return isPatientInTheDB;
        }

        public static bool isMedicalRecordInTheDB(Patient p, MedicalRecord m)
        {
            bool isMedicalRecordInTheDB = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SelectMedicalRecordByID";
            cmd.Parameters.Add("@Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = m.entryDate;

            cmd.CommandType = CommandType.StoredProcedure;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    isMedicalRecordInTheDB = true;
                }
                
            }

            return isMedicalRecordInTheDB;
        }
        public static bool isMedicalRecordLocked(Patient p, MedicalRecord m)
        {
            bool isLocked = false;


            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SelectMedicalRecordLock";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Med_Rec_Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Med_Rec_Entry_Date", SqlDbType.DateTime).Value = m.entryDate;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();


                if (reader.Read())
                {
                    isLocked = true;
                }

                
            }
            return isLocked;
        }

        public static void insertPatient(Patient p)
        {
            SqlCommand cmd = new SqlCommand();

            //insert Patient
            cmd.CommandText = "InsertPatient";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@SSN", SqlDbType.Char, 9).Value = p.SSN;
            cmd.Parameters.Add("@First_Name", SqlDbType.VarChar).Value = p.firstName;
            cmd.Parameters.Add("@Last_Name", SqlDbType.VarChar).Value = p.lastName;
            cmd.Parameters.Add("@Middle_Initial", SqlDbType.Char).Value = p.middleInitial;            
            cmd.Parameters.Add("@Birth_Date", SqlDbType.Date).Value = p.birthDate;
            cmd.Parameters.Add("@Address_Line_1", SqlDbType.VarChar).Value = p.address;
            if (!p.address2.Equals("")) cmd.Parameters.Add("@Address_Line_2", SqlDbType.VarChar).Value = p.address2;
            cmd.Parameters.Add("@Address_City", SqlDbType.VarChar).Value = p.city;
            cmd.Parameters.Add("@Address_State", SqlDbType.Char, 2).Value = p.state;
            cmd.Parameters.Add("@Address_Zip_Code", SqlDbType.Char, 5).Value = p.zipCode;

            if (p.gender.Equals('M') || p.gender.Equals('m')) cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = true;
            else cmd.Parameters.Add("@Gender", SqlDbType.Bit).Value = false; 

            if (p.organDonor.Equals('Y') || p.organDonor.Equals('y')) cmd.Parameters.Add("@Organ_Donor", SqlDbType.Bit).Value = true;
            else cmd.Parameters.Add("@Organ_Donor", SqlDbType.Bit).Value = false;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
           
        }

        public static void deleteMedicalRecordSymptom(Patient p, MedicalRecord m, Symptom s)
        {
            SqlCommand cmd = new SqlCommand();

            //delete Medical_Record_Symptom
            cmd.CommandText = "DeleteMedicalRecordSymptom";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Symptom_Name", SqlDbType.VarChar).Value = s.name;
            cmd.Parameters.Add("@Med_Rec_Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Med_Rec_Entry_Date", SqlDbType.DateTime).Value = m.entryDate;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            
        }

        public static void insertMedicalRecordSymptom(Patient p, MedicalRecord m, Symptom s)
        {
            SqlCommand cmd = new SqlCommand();

            //insert Medical_Record_Symptom
            cmd.CommandText = "InsertMedicalRecordSymptom";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Symptom_Name", SqlDbType.VarChar).Value = s.name;
            cmd.Parameters.Add("@Med_Rec_Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Med_Rec_Entry_Date", SqlDbType.DateTime).Value = m.entryDate;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            
        }

        public static void insertMedicalRecord(Patient p, MedicalRecord m)
        {
            SqlCommand cmd = new SqlCommand();

            //insert Medical Record
            cmd.CommandText = "InsertMedicalRecord";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = m.entryDate;
            cmd.Parameters.Add("@Room_Number", SqlDbType.VarChar).Value = m.roomNumber;
            if (m.exitDate != null) cmd.Parameters.Add("@Exit_Date", SqlDbType.DateTime).Value = m.exitDate;
            cmd.Parameters.Add("@Attending_Physician", SqlDbType.VarChar).Value = m.physician;
            if (!m.diagnosis.Equals("")) cmd.Parameters.Add("@Diagnosis", SqlDbType.VarChar).Value = m.diagnosis;
            if (!m.notes.Equals("")) cmd.Parameters.Add("@Notes", SqlDbType.VarChar).Value = m.notes;
            if (!m.insurer.Equals("")) cmd.Parameters.Add("@Insurer", SqlDbType.VarChar).Value = m.insurer;
            cmd.Parameters.Add("@DNR", SqlDbType.Bit).Value = m.doNotResuscitate;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }

        }

        public static List<Symptom> getSymptoms()
        {
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            List<Symptom> symptomList = new List<Symptom>();

            cmd.CommandText = "SelectAllSymptoms";
            cmd.CommandType = CommandType.StoredProcedure;
            

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                int columnNumber;
                while (reader.Read())
                {
                    Symptom symptomResult = new Symptom();
                    columnNumber = reader.GetOrdinal("Name");
                    symptomResult.name = reader.GetString(columnNumber);
               
                    symptomList.Add(symptomResult);
                }
            }
            
            return symptomList;
        }

        public static List<Patient> getPatients(Patient patient,MedicalRecord inRecord)
        {
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader; 
            List<Patient> patients = new List<Patient>();

            cmd.CommandText = "SelectPatients";
            if (!patient.SSN.Equals("")) cmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = patient.SSN;
            if (!patient.firstName.Equals("")) cmd.Parameters.Add("@First_Name", SqlDbType.VarChar).Value = patient.firstName;
            if (!patient.lastName.Equals("")) cmd.Parameters.Add("@Last_Name", SqlDbType.VarChar).Value = patient.lastName;
            if (!inRecord.roomNumber.Equals("")) cmd.Parameters.Add("@Room_Number", SqlDbType.VarChar).Value = inRecord.roomNumber;

            cmd.CommandType = CommandType.StoredProcedure;
            

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                bool gender;
                bool organDonor;
                while (reader.Read())
                {
                    Patient newPatient = new Patient();
                    int columnNumber = reader.GetOrdinal("SSN");
                    newPatient.SSN = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("First_Name");
                    newPatient.firstName = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Last_Name");
                    newPatient.lastName = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Middle_Initial");
                    newPatient.middleInitial = reader.GetString(columnNumber)[0];

                    columnNumber = reader.GetOrdinal("Gender");
                    gender = reader.GetBoolean(columnNumber);
                    if (gender) newPatient.gender = 'M';
                    else newPatient.gender = 'F';
                    
                    columnNumber = reader.GetOrdinal("Organ_Donor");
                    organDonor = reader.GetBoolean(columnNumber);
                    if (organDonor) newPatient.organDonor = 'Y';
                    else newPatient.organDonor = 'N';

                    columnNumber = reader.GetOrdinal("Birth_Date");
                    newPatient.birthDate = reader.GetDateTime(columnNumber);

                    columnNumber = reader.GetOrdinal("Address_Line_1");
                    newPatient.address = reader.GetString(columnNumber);
                    try
                    {
                        columnNumber = reader.GetOrdinal("Address_Line_2");
                        newPatient.address2 = reader.GetString(columnNumber);
                    }
                    catch (Exception) { }

                    columnNumber = reader.GetOrdinal("Address_City");
                    newPatient.city = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Address_State");
                    newPatient.state = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Address_Zip_Code");
                    newPatient.zipCode = reader.GetString(columnNumber);
                    patients.Add(newPatient);
                }
            }
            
            foreach (Patient tempPatient in patients)
            {
                tempPatient.medicalRecords = getRecords(tempPatient.SSN);
            }

            return patients;
        }
       
        public static void insertMedicalRecordLock(Patient p, MedicalRecord m, User u, DateTime currentTime)
        {
            SqlCommand cmd = new SqlCommand();

            //insert Medical_Record_Lock
            cmd.CommandText = "InsertMedicalRecordLock";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Med_Rec_Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Med_Rec_Entry_Date", SqlDbType.DateTime).Value = m.entryDate;
            cmd.Parameters.Add("@User_Username", SqlDbType.VarChar).Value = u.userName;
            cmd.Parameters.Add("@Lock_Date", SqlDbType.DateTime).Value = currentTime;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }

        }
        public static void deleteMedicalRecordLock(Patient p, MedicalRecord m)
        {
            SqlCommand cmd = new SqlCommand();

            //insert Medical_Record_Lock
            cmd.CommandText = "DeleteMedicalRecordLock";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Med_Rec_Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Med_Rec_Entry_Date", SqlDbType.DateTime).Value = m.entryDate;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            
        }
        public static bool hasMedicalRecord(Patient p)
        {
            bool hasRecord = false;

            SqlCommand recordCommand = new SqlCommand();

            recordCommand.CommandText = "SelectMedicalRecordBySSN";
            recordCommand.Parameters.Add("@Patient_SSN", SqlDbType.VarChar).Value = p.SSN;

            recordCommand.CommandType = CommandType.StoredProcedure;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                recordCommand.Connection = conn;
                conn.Open();

                SqlDataReader reader = recordCommand.ExecuteReader();

                while (reader.Read())
                {
                    hasRecord = true;
                }
            }
            return hasRecord;
        }

        public static List<MedicalRecord> getRecords(string ssn)
        {
            List<MedicalRecord> records = new List<MedicalRecord>();

            SqlCommand recordCommand = new SqlCommand();

            recordCommand.CommandText = "SelectMedicalRecordBySSN";
            recordCommand.Parameters.Add("@Patient_SSN", SqlDbType.VarChar).Value = ssn;

            recordCommand.CommandType = CommandType.StoredProcedure;
            

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                recordCommand.Connection = conn;
                conn.Open();

                SqlDataReader reader = recordCommand.ExecuteReader();

                
                while (reader.Read())
                {
                    MedicalRecord record = new MedicalRecord();
                    int columnNumber = reader.GetOrdinal("Entry_Date");
                    record.entryDate = reader.GetDateTime(columnNumber);

                    columnNumber = reader.GetOrdinal("Room_Number");
                    record.roomNumber = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Attending_Physician");
                    record.physician = reader.GetString(columnNumber);
                    try
                    {
                        columnNumber = reader.GetOrdinal("Exit_Date");
                        record.exitDate = reader.GetDateTime(columnNumber);
                    }
                    catch (Exception) { }
                    
                    try
                    {
                        columnNumber = reader.GetOrdinal("Diagnosis");
                        record.diagnosis = reader.GetString(columnNumber);
                    }
                    catch (Exception) { }
                    
                    try
                    {
                        columnNumber = reader.GetOrdinal("Notes");
                        record.notes = reader.GetString(columnNumber);
                    }
                    catch (Exception) { }
                    
                    try
                    {
                        columnNumber = reader.GetOrdinal("Insurer");
                        record.insurer = reader.GetString(columnNumber);
                    }
                    catch (Exception) { }
                    
                    records.Add(record);
                }
            }

            foreach (MedicalRecord record in records)
            {
                List<Symptom> symptoms = getSyptomsForMedicalRecord(ssn, record.entryDate);
                foreach (Symptom symptom in symptoms)
                {
                    record.addSymptom(symptom);
                }
            }

            return records;
        }

        public static List<Symptom> getSyptomsForMedicalRecord(string ssn, DateTime entryDate)
        {
            SqlCommand recordCommand = new SqlCommand();
            List<Symptom> symptoms = new List<Symptom>();

            recordCommand.CommandText = "MedicalRecordInnerJoinMedicalRecordSymptom";
            recordCommand.Parameters.Add("@Patient_SSN", SqlDbType.VarChar).Value = ssn;
            recordCommand.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = entryDate;

            recordCommand.CommandType = CommandType.StoredProcedure;


            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                recordCommand.Connection = conn;
                conn.Open();

                SqlDataReader reader = recordCommand.ExecuteReader();
                
                while (reader.Read())
                {
                    Symptom symptom = new Symptom();
                    int columnNumber = reader.GetOrdinal("Symptom_Name");
                    symptom.name = reader.GetString(columnNumber);
                    symptoms.Add(symptom);
                }
            }
            
            return symptoms;
        }
        public static List<AuditInfo> getAuditHistory(Patient p , MedicalRecord m)
        {
            SqlCommand recordCommand = new SqlCommand();
            List<AuditInfo> auditInfo = new List<AuditInfo>();

            recordCommand.CommandText = "MedicalRecordInnerJoinAuditMedicalRecord";
            recordCommand.Parameters.Add("@Patient_SSN", SqlDbType.VarChar).Value = p.SSN;
            recordCommand.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = m.entryDate;

            recordCommand.CommandType = CommandType.StoredProcedure;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                recordCommand.Connection = conn;
                conn.Open();

                SqlDataReader reader = recordCommand.ExecuteReader();

                int columnNumber;
                while (reader.Read())
                {
                                      
                    AuditInfo audit = new AuditInfo();

                    columnNumber = reader.GetOrdinal("Patient_SSN");
                    audit.ssn = reader.GetString(columnNumber);
                    columnNumber = reader.GetOrdinal("Entry_Date");
                    audit.entryDate = reader.GetDateTime(columnNumber);
                    columnNumber = reader.GetOrdinal("Date_Changed");
                    audit.dateChanged = reader.GetDateTime(columnNumber);
                    columnNumber = reader.GetOrdinal("User_Username");
                    audit.username = reader.GetString(columnNumber);
                    columnNumber = reader.GetOrdinal("Field_Changed");
                    audit.field_changed = reader.GetString(columnNumber);
                    columnNumber = reader.GetOrdinal("Previous_Value");
                    audit.prviousValue = reader.GetString(columnNumber);
                    columnNumber = reader.GetOrdinal("New_Value");
                    audit.newValue = reader.GetString(columnNumber);

                    auditInfo.Add(audit);
                }
            }
            
            return auditInfo;

        }
        public static void insertInAuditTable(Patient p,MedicalRecord m, User currentUser,DateTime currentDate,string fieldChanged ,string oldValue,string newValue)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "InsertAuditMedicalRecord";
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@Med_Rec_Patient_SSN", SqlDbType.Char).Value = p.SSN;
            cmd.Parameters.Add("@Med_Rec_Entry_Date", SqlDbType.DateTime).Value = m.entryDate;
            cmd.Parameters.Add("@Date_Changed", SqlDbType.DateTime).Value = currentDate;
            cmd.Parameters.Add("@User_Username", SqlDbType.VarChar).Value = currentUser.userName;
            cmd.Parameters.Add("@Field_Changed", SqlDbType.VarChar).Value = fieldChanged;
            cmd.Parameters.Add("@Previous_Value", SqlDbType.VarChar).Value = oldValue;
            cmd.Parameters.Add("@New_Value", SqlDbType.VarChar).Value = newValue;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            

        }
    }
}