﻿namespace HRAS_Middleware
{
    public class User
    {

        public string userName { get; set; }
        public string password { get; set; }
        public bool isInventoryAdministrator { get; set; }

        /// <summary>
        ///     Creates a medical record.
        /// </summary>
        /// <returns></returns>
        public MedicalRecord CreateMedicalRecord()
        {
            var newMedicalRecord = new MedicalRecord();
            return newMedicalRecord;
        }
    }
}