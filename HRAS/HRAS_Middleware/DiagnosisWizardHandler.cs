﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRAS_Middleware
{
    public class DiagnosisWizardHandler
    {
        public  string getSymptom(User user) { return DataControlDiagnosisWizard.getSymptom(user); }
        public  List<Diagnosis> getDiagnoses(User user) { return DataControlDiagnosisWizard.getDiagnoses(user); }
        public  void insertSymptom(User user, Symptom symptom, bool hasSymptom) { DataControlDiagnosisWizard.insertSymptom(user, symptom, hasSymptom); }
        public  void deleteSymptoms(User user) { DataControlDiagnosisWizard.deleteSymptoms(user); }
    }
}
