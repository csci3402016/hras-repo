﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRAS_Middleware
{
    class DataControlDiagnosisWizard : DataControl
    {
        private static int mostProbableDiagnoses = 5;

        public static string getSymptom(User user)
        {
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "GetSymptom";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = user.userName;
            string symptomToReturn;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                int index = reader.GetOrdinal("Symptom_Name");
                symptomToReturn = reader.GetString(index);
            }

            return symptomToReturn;
        }

        public static List<Diagnosis> getDiagnoses(User user)
        {
            List<Diagnosis> list = new List<Diagnosis>();

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "GetMostProbableDiagnoses";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = user.userName;
            cmd.Parameters.Add("@n", SqlDbType.Int).Value = mostProbableDiagnoses;


            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                Diagnosis d;
                int index;
                for (int i = 0; reader.Read() && i < mostProbableDiagnoses; i++)
                {
                    d = new Diagnosis();
                    index = reader.GetOrdinal("Diagnosis");
                    d.name = reader.GetString(index);
                    index = reader.GetOrdinal("Probability");
                    d.probability = reader.GetDouble(index);
                    list.Add(d);
                } 
            }

            return list;
        }

        public static void insertSymptom(User user, Symptom symptom, bool hasSymptom)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "InsertHRASUserSymptom";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@User_Username", SqlDbType.VarChar).Value = user.userName;
            cmd.Parameters.Add("@Symptom_Name", SqlDbType.VarChar).Value = symptom.name;
            cmd.Parameters.Add("@Has_Symptom", SqlDbType.Bit).Value = hasSymptom;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public static void deleteSymptoms(User user)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "DeleteHRASUserSymptom";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@User_Username", SqlDbType.VarChar).Value = user.userName;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery(); 
            }
        }

    }
}
