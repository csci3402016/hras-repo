﻿using System;

namespace HRAS_Middleware
{
    public class Bill
    {
        public DateTime processedTime { get; set; }
        public double cost { get; set; }
        public int totalStayTime { get; set; }
        public Bill() { }

        public void deepCopy(Bill inBill)
        {
            processedTime = inBill.processedTime;
            cost = inBill.cost;
            totalStayTime = inBill.totalStayTime;
        }
    }
}