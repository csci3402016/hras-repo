﻿using System;
using System.Collections.Generic;

namespace HRAS_Middleware
{
    public class MedicalRecord
    {


        
        private List<double> roomRates;
        private List<Symptom> symptoms;
        public  List<InventoryItem> inventoryItems;
        public bool doNotResuscitate { get; set; }
        public string diagnosis { get; set; }
        public DateTime entryDate { get; set; }
        public Nullable<DateTime> exitDate { get; set; }
        public string notes { get; set; }
        public string physician { get; set; }
        public string roomNumber { get; set; }
        public Bill bill { get; set; }
        public string insurer { get; set; }

        public MedicalRecord()
        {
            inventoryItems = new List<InventoryItem>();
            roomRates = new List<double>();
            symptoms = new List<Symptom>();
        }

        public void deepCopy(MedicalRecord inRecord)
        {
            symptoms.Clear();
            foreach(Symptom s in inRecord.symptoms)
            {
                symptoms.Add(s);
            }

            inventoryItems.Clear();
            foreach(InventoryItem item in inRecord.inventoryItems)
            {
                inventoryItems.Add(item);
            }

            roomRates.Clear();
            foreach(double rate in inRecord.roomRates)
            {
                roomRates.Add(rate);
            }

            doNotResuscitate = inRecord.doNotResuscitate;
            entryDate = inRecord.entryDate;
            exitDate = inRecord.exitDate;
            diagnosis = inRecord.diagnosis;
            notes = inRecord.notes;
            physician = inRecord.physician;
            roomNumber = inRecord.roomNumber;
            insurer = inRecord.insurer;
            
            //bill.deepCopy(inRecord.bill);

        }


        /// <summary>
        ///     Gets the inventory items.
        /// </summary>
        /// <returns></returns>
        public List<InventoryItem> getInventoryItems()
        {
            return inventoryItems;
        }

        /// <summary>
        ///     Adds an inventory item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void addInventoryItem(InventoryItem item)
        {
            inventoryItems.Add(item);
        }

        /// <summary>
        ///     Removes an inventory item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void removeInventoryItem(InventoryItem item)
        {
            inventoryItems.Remove(item);
        }

        /// <summary>
        ///     Gets the room rates.
        /// </summary>
        /// <returns></returns>
        public List<double> getRoomRates()
        {
            return roomRates;
        }

        /// <summary>
        ///     Adds to the roomRates list.
        /// </summary>
        /// <param name="rate">The rate.</param>
        public void addRoomRate(double rate)
        {
            roomRates.Add(rate);
        }

        /// <summary>
        ///     Removes the room rate at.
        /// </summary>
        /// <param name="index">The index.</param>
        public void removeRoomRateAt(int index)
        {
            roomRates.RemoveAt(index);
        }

        /// <summary>
        ///     Gets the symptoms.
        /// </summary>
        /// <returns></returns>
        public List<Symptom> getSymptoms()
        {
            return symptoms;
        }

        /// <summary>
        ///     Adds the symptom.
        /// </summary>
        /// <param name="symptom">The symptom.</param>
        public void addSymptom(Symptom symptom)
        {
            symptoms.Add(symptom);
        }

        /// <summary>
        ///     Removes the symptom.
        /// </summary>
        /// <param name="symptom">The symptom.</param>
        public void removeSymptom(Symptom symptom)
        {
            symptoms.Remove(symptom);
        }


        /// <summary>
        ///     Calculates the bill.
        /// </summary>
        public void calculateBill()
        {
        }

        /// <summary>
        ///     Checkouts the patient.
        /// </summary>
        public void checkOut()
        {
        }
    }
}