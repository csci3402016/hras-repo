﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRAS_Middleware
{
    public class UserHandler
    {
        public bool login(User newUser)
        {
            //data massage here
            newUser.userName = newUser.userName.ToUpper(); //username not case sensitive.
            newUser.password = newUser.password.ToUpper();

            //call DataController here
            return DataControlUser.validUsernamePassword(newUser);

        }
        public static User getUser(User user)
        {

            return DataControlUser.getUser(user);

        }
    }
}
