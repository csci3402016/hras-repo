﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HRAS_Middleware
{
    public class PatientHandler 
    {

        public void createMedicalRecord(Patient p,MedicalRecord m) 
        {

       
            if(!DataControlPatient.isRoomInTheDB(m))
            {
                throw new Exception("There is no such room in our records");
            }
            else if (DataControlPatient.isMedicalRecordInTheDB(p, m))
            {
                throw new Exception("Medical Record is already in the database");
            }
            else
            {

                //insert Medical Record
                DataControlPatient.insertMedicalRecord(p, m);
                for (int i = 0; i < m.getSymptoms().Count; i++)
                {
                   //insert in Medical_Record_Symptom
                    DataControlPatient.insertMedicalRecordSymptom(p, m, m.getSymptoms().ElementAt(i));

                }

            }

        }
        public static void insertMedicalRecordLock(Patient p,MedicalRecord m,User u,DateTime currentTime)
        {
            DataControlPatient.insertMedicalRecordLock(p, m, u, currentTime);
        }
        public static void deleteMedicalRecordLock(Patient p, MedicalRecord m)
        {
            DataControlPatient.deleteMedicalRecordLock(p, m);
        }
        public static bool isMedicalRecordLocked(Patient p,MedicalRecord m)
        {
            return DataControlPatient.isMedicalRecordLocked(p, m);
        }
        public static bool hasRecord(Patient p)
        {
            return DataControlPatient.hasMedicalRecord(p);
        }

        public static List<Symptom> getSymptoms()
        {
            return DataControlPatient.getSymptoms();
        }

        public void createPatient(Patient p)
        {
            if (DataControlPatient.isPatientInTheDB(p))
            {
                throw new Exception("Patient is already in the database");
            }
            else
            {
                DataControlPatient.insertPatient(p);
            }

        }
        public void updatePatient(Patient p)
        {
            DataControlPatient.updatePatient(p);

        }
        public void updateMedicalRecord(MedicalRecord oldRecord, MedicalRecord newRecord, Patient p,User currentUser)
        {

            if (!DataControlPatient.isRoomInTheDB(newRecord))
            {
                throw new Exception("There is no such room in our records");
            }
            else
            {
                DataControlPatient.updateMedicalRecord(newRecord, p);

                //if new record list does not contain given symptom from the old record list,
                //delete row in Med_Rec_Symptom table with  symptom,oldRecord

                Symptom s = new Symptom();
                for (int i = 0; i < oldRecord.getSymptoms().Count; i++)
                {
                    s = oldRecord.getSymptoms().ElementAt(i);
                    if (!newRecord.getSymptoms().Contains(s))
                    {
                        //since we only need PK from record , i could send olRecord too
                        DataControlPatient.deleteMedicalRecordSymptom(p, newRecord, s);
                    }
                }


                //if old record list does not contain given symptom from the new record list,
                //insert row in Med_Rec_Symptom table with  syptom,oldRecord

                for (int i = 0; i < newRecord.getSymptoms().Count; i++)
                {
                    s = newRecord.getSymptoms().ElementAt(i);
                    if (!oldRecord.getSymptoms().Contains(s))
                    {
                        DataControlPatient.insertMedicalRecordSymptom(p, newRecord, s);
                    }
                }

                //convert old symptoms list to string
                string oldSymptoms = "";
                foreach(Symptom sm in oldRecord.getSymptoms())
                {
                    if (oldSymptoms.Length == 0)
                    {
                        oldSymptoms = sm.name;
                    }
                    else
                    {
                        oldSymptoms = oldSymptoms + "," + sm.name;
                    }
                }
                
                //convert new symptoms list to string
                string newSymptoms = "";
                foreach (Symptom sm in newRecord.getSymptoms())
                {
                    if (newSymptoms.Length == 0)
                    {
                        newSymptoms = sm.name;
                    }
                    else
                    { 
                        newSymptoms = newSymptoms + "," + sm.name;
                    }
                }


                //insert edited fields in audit table

                DateTime currentTime = DateTime.Now;
                if (!oldRecord.diagnosis.Equals(newRecord.diagnosis))
                {
                    DataControlPatient.insertInAuditTable(p, newRecord, currentUser, currentTime, "diagnosis", oldRecord.diagnosis, newRecord.diagnosis);
                }
                if (!oldRecord.physician.Equals(newRecord.physician))
                {
                    DataControlPatient.insertInAuditTable(p, newRecord, currentUser, currentTime, "physician", oldRecord.physician, newRecord.physician);
                }
                if (!oldRecord.notes.Equals(newRecord.notes))
                {
                    DataControlPatient.insertInAuditTable(p, newRecord, currentUser, currentTime, "notes", oldRecord.notes, newRecord.notes);
                }
                if (!oldRecord.doNotResuscitate.Equals(newRecord.doNotResuscitate))
                {
                    string oldDnr = Convert.ToString(oldRecord.doNotResuscitate);
                    string newDnr = Convert.ToString(newRecord.doNotResuscitate);
                    DataControlPatient.insertInAuditTable(p, newRecord, currentUser, currentTime, "dnr", oldDnr, newDnr);
                }
                if (!oldRecord.insurer.Equals(newRecord.insurer))
                {
                    DataControlPatient.insertInAuditTable(p, newRecord, currentUser, currentTime, "insurer", oldRecord.insurer, newRecord.insurer);
                }
                if(!oldSymptoms.Equals(newSymptoms))
                {
                    DataControlPatient.insertInAuditTable(p, newRecord, currentUser, currentTime, "symptoms", oldSymptoms, newSymptoms);
                }
              
              
            }

        }
        public static List<AuditInfo> getAuditInfo(Patient p, MedicalRecord m)
        {
            return DataControlPatient.getAuditHistory(p, m);
        }
        public List<Patient> searchForPatient(Patient patient, MedicalRecord inRecord)
        {
            return DataControlPatient.getPatients(patient, inRecord);
        }
        public Patient getMedicalRecord(string ssn, DateTime entryDate)
        {
            Patient p = new Patient();
            // comunicate with the DataControler
            return p;
        }
        
    }
}
