﻿namespace HRAS_Middleware
{
    public class Symptom
    {
        public string name;
        public double probability;

        public Symptom()
        {

        }
        public Symptom(string name)
        {
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType()) return false;
            Symptom s = (Symptom)obj;
            return name.Equals(s.name);
        }
    }
}