﻿using System;
using System.Collections.Generic;

namespace HRAS_Middleware
{
    public class Patient
    {
        public List<MedicalRecord> medicalRecords { get; set; }
        public char organDonor { get; set; }
        public DateTime birthDate { get; set; }
        public string SSN { get; set; }
        public string address { get; set; }
        public string address2 { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public char middleInitial { get; set; } 
        public char gender { get; set; } 
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }

        public Patient()
        {
            medicalRecords = new List<MedicalRecord>();
        }

        public void deepCopy(Patient inPatient)
        {
            //copy each data member of patient
            
            medicalRecords = new List<MedicalRecord>();
            foreach (MedicalRecord record in inPatient.medicalRecords)
            {
                medicalRecords.Add(record);
            }

            organDonor = inPatient.organDonor;
            birthDate = inPatient.birthDate;
            SSN = inPatient.SSN;
            firstName = inPatient.firstName;
            lastName = inPatient.lastName;
            middleInitial = inPatient.middleInitial;
            zipCode = inPatient.zipCode;
            state = inPatient.state;
            city = inPatient.city;
            gender = inPatient.gender;
            address = inPatient.address;
            address2 = inPatient.address2;

        }
    }
}