﻿using System;

namespace HRAS_Middleware
{
    public class InventoryItem
    {
        public double cost { get; set; }
        public double size { get; set; }
        public string id { get; set; }
        public Nullable<Int32> quantity { get; set; }
        public string description { get; set; }
        public bool isVIRTL { get; set; }
        public void deepCopy(InventoryItem item)
        {
            cost = item.cost;
            size = item.size;
            id = item.id;
            quantity = item.quantity;
            description = item.description;
            isVIRTL = item.isVIRTL;
        }
    }
}