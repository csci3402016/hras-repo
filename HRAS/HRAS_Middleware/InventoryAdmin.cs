﻿namespace HRAS_Middleware
{
    public class InventoryAdmin : User
    {
        /// <summary>
        ///     Changes the rate.
        /// </summary>
        /// <returns></returns>
        public double changeRate()
        {
            double newRate = 0;
            return newRate;
        }

        /// <summary>
        ///     Creates a inventory item.
        /// </summary>
        /// <returns></returns>
        public InventoryItem createsInventoryItem()
        {
            var newInventoryItem = new InventoryItem();
            return newInventoryItem;
        }
    }
}