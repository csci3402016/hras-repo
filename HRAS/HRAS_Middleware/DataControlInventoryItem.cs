﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRAS_Middleware
{
    class DataControlInventoryItem : DataControl
    {
        public DataControlInventoryItem()
        {

        }

        public static bool isItemInTheDb(InventoryItem item)
        {
            bool isItemInTheDB = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SelectInventoryItemByPKOrUniqueKey";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Stock_ID", SqlDbType.VarChar).Value = item.id;
            cmd.Parameters.Add("@Size", SqlDbType.VarChar).Value = item.size;
            cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = item.description;      

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    isItemInTheDB = true;
                }
                
                
            }

            return isItemInTheDB;
        }
        public static List<InventoryItem> getItems(InventoryItem item)
        {
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            List<InventoryItem> itemList = new List<InventoryItem>();

            if (item.description == "")
            {
                cmd.CommandText = "SelectAllInventoryItems";
            }
            else
            {
                cmd.CommandText = "SelectInventoryItemByDescription";
                cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = item.description;
            }

            cmd.CommandType = CommandType.StoredProcedure;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    InventoryItem itemResult = new InventoryItem();
                    int columnNumber = reader.GetOrdinal("Stock_ID");
                    itemResult.id = reader.GetString(columnNumber);

                    try {
                        columnNumber = reader.GetOrdinal("Quantity");
                        itemResult.quantity = (int)reader.GetSqlInt32(columnNumber);
                    }catch(Exception ex) { }

                    columnNumber = reader.GetOrdinal("Description");
                    itemResult.description = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Size");
                    itemResult.size = (int)reader.GetSqlSingle(columnNumber);

                    columnNumber = reader.GetOrdinal("Cost");
                    itemResult.cost = (double)reader.GetSqlSingle(columnNumber);

                    columnNumber = reader.GetOrdinal("is_Virtual");
                    itemResult.isVIRTL = (bool)reader.GetSqlBoolean(columnNumber);


                    itemList.Add(itemResult);
                }

            }

            return itemList;
        }



        public static List<InventoryItem> getItemsUsed(MedicalRecord visit, Patient toBill)
        {
            
            
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            List<InventoryItem> itemList = new List<InventoryItem>();



            cmd.CommandText = "MedicalRecordInnerJoinInventoryUsage";
            cmd.Parameters.Add("@Patient_SSN", SqlDbType.VarChar).Value = toBill.SSN;
            cmd.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = visit.entryDate;
            

            cmd.CommandType = CommandType.StoredProcedure;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    InventoryItem itemResult = new InventoryItem();
                    int columnNumber = reader.GetOrdinal("Stock_ID");
                    itemResult.id = reader.GetString(columnNumber);

                    try
                    {
                        columnNumber = reader.GetOrdinal("Quantity");
                        itemResult.quantity = (int)reader.GetSqlInt32(columnNumber);
                    }
                    catch (Exception ex) { }

                    itemResult.description = "";

                    itemResult.size = 0.0;


                    //This may be roundabout, but the stored prcedure "MedicalRecordInnerJoinInventoryUsage" doesn't return cost of items
                    //and I did not want to change that procedure for fear that it is beign used somewhere else and the columns there are being
                    //accessed by index number. 
                    itemResult.cost = getItems(itemResult).ElementAt(0).cost;


                    columnNumber = reader.GetOrdinal("Cost");
                    itemResult.cost = (double)reader.GetSqlSingle(columnNumber);

                    itemResult.isVIRTL = false;


                    itemList.Add(itemResult);
                }

            }

            return itemList;
        }



        public static void updateItem(InventoryItem item)
        {
            SqlCommand cmd = new SqlCommand();


            cmd.CommandText = "UpdateInventoryItem";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@Stock_ID", SqlDbType.VarChar).Value = item.id;
            if (!item.isVIRTL) cmd.Parameters.Add("@NewQuantity", SqlDbType.Int).Value = item.quantity;
            cmd.Parameters.Add("@NewDescription", SqlDbType.VarChar).Value = item.description;
            cmd.Parameters.Add("@NewSize", SqlDbType.Real).Value = item.size;
            cmd.Parameters.Add("@NewCost", SqlDbType.Real).Value = item.cost;
            cmd.Parameters.Add("@NewIs_Virtual", SqlDbType.Bit).Value = item.isVIRTL;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {

                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }

        }

        public static void insertItem(InventoryItem newItem)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "InsertInventoryItem";
            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.Add("@Stock_ID", SqlDbType.VarChar).Value = newItem.id;
            if (!newItem.isVIRTL) cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = newItem.quantity;
            cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = newItem.description;
            cmd.Parameters.Add("@Size", SqlDbType.Real).Value = newItem.size;
            cmd.Parameters.Add("@Cost", SqlDbType.Real).Value = newItem.cost;
            cmd.Parameters.Add("@Is_Virtual", SqlDbType.Bit).Value = newItem.isVIRTL;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }

        }
    }
}
