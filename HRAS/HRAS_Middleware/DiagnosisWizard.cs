﻿using System.Collections.Generic;

namespace HRAS_Middleware
{
    public class DiagnosisWizard
    {
        private List<Diagnosis> diagnoses;
        private List<string> probableDiagnoses;
        private List<Symptom> symptoms;
        public bool userResponse { get; set; }

        /// <summary>
        ///     Gets the probable diagnoses.
        /// </summary>
        /// <returns></returns>
        public List<string> getProbableDiagnoses()
        {
            return probableDiagnoses;
        }

        /// <summary>
        ///     Asks the question.
        /// </summary>
        public void askQuestion()
        {
        }
    }
}