﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRAS_Middleware
{
    class DataControlUser : DataControl
    {
        public DataControlUser()
        {

        }
        public static User getUser(User newUser)
        {
          
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            User user = new User();

            cmd.CommandText = "SelectHRASUser";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = newUser.userName;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = newUser.password;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                
                cmd.Connection = conn;
                conn.Open();

                user = new User();      //Will declarations within the "using block" all try to compete for the same resources declared above? -jjl
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int columnNumber = reader.GetOrdinal("Username");
                    user.userName = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Password");
                    user.password = reader.GetString(columnNumber);

                    columnNumber = reader.GetOrdinal("Is_Inventory_Administator");
                    user.isInventoryAdministrator = reader.GetBoolean(columnNumber);
                }
            }
            

            return user;
        }
        public static bool validUsernamePassword(User newUser)
        {
            bool valid = false;

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "SelectHRASUser";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = newUser.userName;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = newUser.password;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                
                cmd.Connection = conn;
                conn.Open();

                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    valid = true;
                }
            }

            return valid;
        }

    }
}
