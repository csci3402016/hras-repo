﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Patient_Search_Results_View : Form
    {
        private readonly List<Patient> patientList;

        public HRAS_Patient_Search_Results_View(List<Patient> patients)
        {
            InitializeComponent();
            patientList = patients;
            FillData(patients);
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void HRAS_Patient_Search_Results_View_Load(object sender, EventArgs e)
        {
        }

        
        private void ToHomeView_Click(object sender, EventArgs e)
        {
        }

        
        private void FillData(List<Patient> list)
        {
            patientSearchResultGrid.DataSource = list;

            // add view button
            DataGridViewButtonColumn viewButtonColumn = new DataGridViewButtonColumn();
            viewButtonColumn.Name = "View";
            viewButtonColumn.Text = "View";
            viewButtonColumn.UseColumnTextForButtonValue = true;
            int columnIndex = patientSearchResultGrid.DisplayedColumnCount(false) + 1;

            if (patientSearchResultGrid.Columns["View"] == null)
            {
                patientSearchResultGrid.Columns.Insert(columnIndex, viewButtonColumn);
            }

            // add edit button
            //DataGridViewButtonColumn editButtonColumn = new DataGridViewButtonColumn();
            //editButtonColumn.Name = "Edit";
            //editButtonColumn.Text = "Edit";
            //editButtonColumn.UseColumnTextForButtonValue = true;
            //int columnIndexEdit = patientSearchResultGrid.DisplayedColumnCount(false) + 2;

            //if (patientSearchResultGrid.Columns["Edit"] == null)
            //{
            //    patientSearchResultGrid.Columns.Insert(columnIndexEdit, editButtonColumn);
            //}

            patientSearchResultGrid.AutoGenerateColumns = false;
            patientSearchResultGrid.Columns["firstName"].DisplayIndex = 0;
            patientSearchResultGrid.Columns["firstName"].HeaderText = "First Name";

            patientSearchResultGrid.Columns["middleInitial"].DisplayIndex = 1;
            patientSearchResultGrid.Columns["middleInitial"].HeaderText = "Middle Initial";

            patientSearchResultGrid.Columns["lastName"].DisplayIndex = 2;
            patientSearchResultGrid.Columns["lastName"].HeaderText = "Last Name";

            patientSearchResultGrid.Columns["gender"].DisplayIndex = 3;
            patientSearchResultGrid.Columns["gender"].HeaderText = "Gender";

            patientSearchResultGrid.Columns["birthDate"].DisplayIndex = 4;
            patientSearchResultGrid.Columns["birthDate"].HeaderText = "Birth Date";

            patientSearchResultGrid.Columns["SSN"].DisplayIndex = 5;

            //hide these columns
            patientSearchResultGrid.Columns["address"].Visible = false;
            patientSearchResultGrid.Columns["address2"].Visible = false;
            patientSearchResultGrid.Columns["city"].Visible = false;
            patientSearchResultGrid.Columns["organDonor"].Visible = false;
            patientSearchResultGrid.Columns["state"].Visible = false;
            patientSearchResultGrid.Columns["zipCode"].Visible = false;
        }

        private void patientSearchResultGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == patientSearchResultGrid.Columns["View"].Index)
            {
                
                Hide();
                HRAS_Patient_View_View patientView = new HRAS_Patient_View_View(patientList.ElementAt(e.RowIndex));
                patientView.Closed += (s, args) => Close();
                patientView.Show();
            }
            //else if (e.ColumnIndex == patientSearchResultGrid.Columns["Edit"].Index)
            //{
                
            //    Hide();
            //    HRAS_Patient_Edit_View editView = new HRAS_Patient_Edit_View(patientList.ElementAt(e.RowIndex));
            //    editView.Closed += (s, args) => Close();
            //    editView.Show();
            //}
        }
    }
}