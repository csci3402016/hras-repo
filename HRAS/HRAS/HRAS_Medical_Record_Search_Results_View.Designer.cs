﻿namespace HRAS
{
    partial class HRAS_Medical_Record_Search_Results_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoutButton = new System.Windows.Forms.Button();
            this.ToHomeView = new System.Windows.Forms.Label();
            this.BackButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.medicalRecordSearchResultsGrid = new System.Windows.Forms.DataGridView();
            this.firstName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.Label();
            this.ssnTitle = new System.Windows.Forms.Label();
            this.ssn = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.medicalRecordSearchResultsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoutButton
            // 
            this.LogoutButton.Location = new System.Drawing.Point(878, 24);
            this.LogoutButton.Margin = new System.Windows.Forms.Padding(4);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(100, 28);
            this.LogoutButton.TabIndex = 0;
            this.LogoutButton.Text = "Logout";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // ToHomeView
            // 
            this.ToHomeView.AutoSize = true;
            this.ToHomeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToHomeView.Location = new System.Drawing.Point(394, 15);
            this.ToHomeView.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ToHomeView.Name = "ToHomeView";
            this.ToHomeView.Size = new System.Drawing.Size(261, 91);
            this.ToHomeView.TabIndex = 1;
            this.ToHomeView.Text = "HRAS";
            this.ToHomeView.Click += new System.EventHandler(this.ToHomeView_Click);
            // 
            // BackButton
            // 
            this.BackButton.Location = new System.Drawing.Point(913, 633);
            this.BackButton.Margin = new System.Windows.Forms.Padding(4);
            this.BackButton.Name = "BackButton";
            this.BackButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.BackButton.Size = new System.Drawing.Size(100, 28);
            this.BackButton.TabIndex = 3;
            this.BackButton.Text = "Back";
            this.BackButton.UseVisualStyleBackColor = true;
            this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(63, 133);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(261, 31);
            this.label5.TabIndex = 12;
            this.label5.Text = "Medical Records of :";
            // 
            // medicalRecordSearchResultsGrid
            // 
            this.medicalRecordSearchResultsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.medicalRecordSearchResultsGrid.Location = new System.Drawing.Point(69, 307);
            this.medicalRecordSearchResultsGrid.Margin = new System.Windows.Forms.Padding(4);
            this.medicalRecordSearchResultsGrid.Name = "medicalRecordSearchResultsGrid";
            this.medicalRecordSearchResultsGrid.ReadOnly = true;
            this.medicalRecordSearchResultsGrid.Size = new System.Drawing.Size(944, 318);
            this.medicalRecordSearchResultsGrid.TabIndex = 21;
            this.medicalRecordSearchResultsGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.medicalRecordSearchResultsGrid_CellContentClick);
            // 
            // firstName
            // 
            this.firstName.AutoSize = true;
            this.firstName.Location = new System.Drawing.Point(190, 196);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(0, 17);
            this.firstName.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Last Name:";
            // 
            // lastName
            // 
            this.lastName.AutoSize = true;
            this.lastName.Location = new System.Drawing.Point(190, 227);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(0, 17);
            this.lastName.TabIndex = 28;
            // 
            // ssnTitle
            // 
            this.ssnTitle.AutoSize = true;
            this.ssnTitle.Location = new System.Drawing.Point(97, 261);
            this.ssnTitle.Name = "ssnTitle";
            this.ssnTitle.Size = new System.Drawing.Size(40, 17);
            this.ssnTitle.TabIndex = 27;
            this.ssnTitle.Text = "SSN:";
            // 
            // ssn
            // 
            this.ssn.AutoSize = true;
            this.ssn.Location = new System.Drawing.Point(190, 261);
            this.ssn.Name = "ssn";
            this.ssn.Size = new System.Drawing.Size(0, 17);
            this.ssn.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(66, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "First Name:";
            // 
            // HRAS_Medical_Record_Search_Results_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1055, 684);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.ssnTitle);
            this.Controls.Add(this.ssn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.medicalRecordSearchResultsGrid);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BackButton);
            this.Controls.Add(this.ToHomeView);
            this.Controls.Add(this.LogoutButton);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HRAS_Medical_Record_Search_Results_View";
            this.Text = "Medical Record Search Results";
            this.Load += new System.EventHandler(this.HRAS_Patient_Search_Results_View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.medicalRecordSearchResultsGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.Label ToHomeView;
        private System.Windows.Forms.Button BackButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView medicalRecordSearchResultsGrid;
        private System.Windows.Forms.Label firstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lastName;
        private System.Windows.Forms.Label ssnTitle;
        private System.Windows.Forms.Label ssn;
        private System.Windows.Forms.Label label6;
    }
}