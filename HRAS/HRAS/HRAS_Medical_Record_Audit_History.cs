﻿using HRAS_Middleware;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRAS
{
    public partial class HRAS_Medical_Record_Audit_History_View : Form
    {
        private List<AuditInfo> auditList;
        private MedicalRecord currentRecord;
        private Patient currentPatient;

        public HRAS_Medical_Record_Audit_History_View(Patient inPatient, MedicalRecord inRecord)
        {
            InitializeComponent();

            currentPatient = new Patient();
            //need to remember these to send them back to Med Rec View upon clicking back button
            currentPatient.deepCopy(inPatient);
            
            currentRecord = new MedicalRecord();
            currentRecord.deepCopy(inRecord);

            displayInfoToFields();
           
            auditList = PatientHandler.getAuditInfo(inPatient, inRecord);        
            FillData(auditList);
        }
        private void displayInfoToFields()
        {
            lastName.Text = currentPatient.lastName;
            firstName.Text = currentPatient.firstName;
            ssn.Text = currentPatient.SSN;

            string month;
            string day;
            //entrydate
            if (Convert.ToString(currentRecord.entryDate.Month).Length == 1) { month = "0" + currentRecord.entryDate.Month; }         
            else { month = Convert.ToString(currentRecord.entryDate.Month); }

            if (Convert.ToString(currentRecord.entryDate.Day).Length == 1) { day = "0" + currentRecord.entryDate.Day; }         
            else { day = Convert.ToString(currentRecord.entryDate.Day); }
            entryDate.Text = Convert.ToString(month + "/" + day + "/" + currentRecord.entryDate.Year);
        }
        private void FillData(List<AuditInfo> list)
        {
            auditHistoryGrid.DataSource = list;
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }
        private void back_Click(object sender, EventArgs e)
        {
            Hide();
            HRAS_Medical_Record_View_View loginWindow = new HRAS_Medical_Record_View_View(currentRecord, currentPatient);
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void HRASLB_Click(object sender, EventArgs e)
        {

        }

        private void auditHistoryGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }
                   
        private void HRAS_Medical_Record_Audit_History_View_Load(object sender, EventArgs e)
        {

        }
    }
}
