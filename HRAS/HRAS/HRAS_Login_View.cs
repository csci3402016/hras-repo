﻿using System;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Login_View : Form
    {
        //need it static so it is provided to every class in project
        public static User currentUser;

        public HRAS_Login_View()
        {
            InitializeComponent();                     
        }
        static HRAS_Login_View()
        {
            currentUser = new User();
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            // Put username and password validation here
            UserHandler connection = new UserHandler();
            User newUser = new User();

            newUser.userName = Username_textbox.Text;
            newUser.password = Password_textbox.Text;
         
            bool allowed = connection.login(newUser);
            
            //if login is valid take user to HRAS_Home_View
           if (allowed)
            {
                //if user is allowed , set currentUser to it
                currentUser.userName = newUser.userName;
                currentUser.password = newUser.password;
                //get data if user is admin or not
                currentUser.isInventoryAdministrator = UserHandler.getUser(newUser).isInventoryAdministrator;

                Hide();
                HRAS_Home_View harsMain = new HRAS_Home_View();
                harsMain.Closed += (s, args) => Close();
                harsMain.Show();
           }           
           else
           { 
               LoginErrorLB.Show();
           }
        }
        public static User getUser()
        {
            return currentUser;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            //set textboxes to empty

            Username_textbox.Text = string.Empty;
            Password_textbox.Text = string.Empty;
            LoginErrorLB.Hide();
        }


        private void Password_textbox_TextChanged(object sender, EventArgs e)
        {
        }

        private void Username_textbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoginErrorLB_Click(object sender, EventArgs e)
        {

        }
        

        private void HRAS_Login_View_Load(object sender, EventArgs e)
        {

        }
    }
}