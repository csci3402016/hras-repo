﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Medical_Record_Search_Results_View : Form
    {
        private readonly Patient patient;
        private readonly List<MedicalRecord> recordList;

        public HRAS_Medical_Record_Search_Results_View(List<MedicalRecord> records, Patient currentPatient)
        {
            InitializeComponent();
            recordList = records;
            FillData(records);

            //remember it so we can send it back (for "Back" button)
            patient = new Patient();
            patient.deepCopy(currentPatient);
            displayInfoToFields();
        }

        private void displayInfoToFields()
        {
            firstName.Text = patient.firstName;
            lastName.Text = patient.lastName;
            ssn.Text = patient.SSN;
        }

        private void FillData(List<MedicalRecord> records)
        {
            medicalRecordSearchResultsGrid.DataSource = records;

            // add view button
            DataGridViewButtonColumn viewButtonColumn = new DataGridViewButtonColumn();
            viewButtonColumn.Name = "View";
            viewButtonColumn.Text = "View";
            viewButtonColumn.UseColumnTextForButtonValue = true;
            int columnIndex = medicalRecordSearchResultsGrid.DisplayedColumnCount(false) + 1;

            if (medicalRecordSearchResultsGrid.Columns["View"] == null)
            {
                medicalRecordSearchResultsGrid.Columns.Insert(columnIndex, viewButtonColumn);
            }

            // add edit button
            //DataGridViewButtonColumn editButtonColumn = new DataGridViewButtonColumn();
            //editButtonColumn.Name = "Edit";
            //editButtonColumn.Text = "Edit";
            //editButtonColumn.UseColumnTextForButtonValue = true;
            //int columnIndexEdit = medicalRecordSearchResultsGrid.DisplayedColumnCount(false) + 2;

            //if (medicalRecordSearchResultsGrid.Columns["Edit"] == null)
            //{
            //    medicalRecordSearchResultsGrid.Columns.Insert(columnIndexEdit, editButtonColumn);
            //}

            medicalRecordSearchResultsGrid.AutoGenerateColumns = false;

            medicalRecordSearchResultsGrid.Columns["entryDate"].DisplayIndex = 0;
            medicalRecordSearchResultsGrid.Columns["entryDate"].HeaderText = "Entry Date";

            medicalRecordSearchResultsGrid.Columns["exitDate"].DisplayIndex = 1;
            medicalRecordSearchResultsGrid.Columns["exitDate"].HeaderText = "Exit Date";

            medicalRecordSearchResultsGrid.Columns["physician"].DisplayIndex = 2;
            medicalRecordSearchResultsGrid.Columns["physician"].HeaderText = "Physician";

            medicalRecordSearchResultsGrid.Columns["roomNumber"].DisplayIndex = 3;
            medicalRecordSearchResultsGrid.Columns["roomNumber"].HeaderText = "Room Number";

            // hide these columns
            medicalRecordSearchResultsGrid.Columns["diagnosis"].Visible = false;
            medicalRecordSearchResultsGrid.Columns["doNotResuscitate"].Visible = false;
            medicalRecordSearchResultsGrid.Columns["notes"].Visible = false;
            medicalRecordSearchResultsGrid.Columns["bill"].Visible = false;
            medicalRecordSearchResultsGrid.Columns["insurer"].Visible = false;
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            //navigates to Patient_View

            Hide();
            HRAS_Patient_View_View home = new HRAS_Patient_View_View(patient);
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void HRAS_Patient_Search_Results_View_Load(object sender, EventArgs e)
        {
        }

        
        private void ToHomeView_Click(object sender, EventArgs e)
        {
        }

        private void medicalRecordSearchResultsGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == medicalRecordSearchResultsGrid.Columns["View"].Index)
            {
                Hide();
                HRAS_Medical_Record_View_View recordView =
                    new HRAS_Medical_Record_View_View(recordList.ElementAt(e.RowIndex), patient);
                recordView.Closed += (s, args) => Close();
                recordView.Show();
            }
            //else if (e.ColumnIndex == medicalRecordSearchResultsGrid.Columns["Edit"].Index)
            //{
            //    Hide();
            //    HRAS_Medical_Record_Edit_View editView =
            //        new HRAS_Medical_Record_Edit_View(recordList.ElementAt(e.RowIndex), patient);
            //    editView.Closed += (s, args) => Close();
            //    editView.Show();
            //}
        }
    }
}