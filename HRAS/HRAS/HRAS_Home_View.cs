﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Home_View : Form
    {
        public HRAS_Home_View()
        {
            InitializeComponent();
           
            if(!HRAS_Login_View.getUser().isInventoryAdministrator)
            {
                adminMessage.Text = "You Are Not Inventory Administrator";
                newInventoryItem.Enabled = false;
            }
        }

        private void search_Click(object sender, EventArgs e)
        {      
            if (patientsRB.Checked)
            {
                Patient newPatient = new Patient();
                MedicalRecord newMedicalRecord = new MedicalRecord();

                newPatient.firstName = firstNameTB.Text;
                newPatient.lastName = lastNameTB.Text;
                newPatient.SSN = ssnTB.Text;
                newMedicalRecord.roomNumber = roomNumberTB.Text;

                PatientHandler connection = new PatientHandler();
                List<Patient> patients = connection.searchForPatient(newPatient,newMedicalRecord);

                Hide();
                HRAS_Patient_Search_Results_View patientSearchResults = new HRAS_Patient_Search_Results_View(patients);
                patientSearchResults.Closed += (s, args) => Close();
                patientSearchResults.Show();
            }

            if (inventoryItemRB.Checked)
            {
                InventoryItem itemToSearch = new InventoryItem();
                itemToSearch.description = inventoryItemNameTB.Text;

                ItemHandler conn = new ItemHandler();
                List<InventoryItem> list = conn.searchForItems(itemToSearch);
                Hide();
                HRAS_Inventory_Item_Search_Results_View itemSearchResults = new HRAS_Inventory_Item_Search_Results_View(list,itemToSearch);
                itemSearchResults.Closed += (s, args) => Close();
                itemSearchResults.Show();
            }

        }

        private void newPatient_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Patient_Create_View

            Hide();
            HRAS_Patient_Create_View newPatient = new HRAS_Patient_Create_View(); 
            newPatient.Closed += (s, args) => Close();
            newPatient.Show();
        }

        private void newInventoryItem_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Inventory_Item_Create_View

            Hide();
            HRAS_Inventory_Item_Create_View newInventoryItem = new HRAS_Inventory_Item_Create_View();
            newInventoryItem.Closed += (s, args) => Close();
            newInventoryItem.Show();
        }

        private void diagnosisWizard_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Diagnosis_Wizard_View

            Hide();
            HRAS_Diagnosis_Wizard_View diagnosisWizard = new HRAS_Diagnosis_Wizard_View();
            diagnosisWizard.Closed += (s, args) => Close();
            diagnosisWizard.Show();
        }

        private void logout_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void HRAS_HomeLabel_Click(object sender, EventArgs e)
        {
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
        }

        private void patientNameLabel_Click(object sender, EventArgs e)
        {
        }

        private void firstNameTB_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Home_View_Load(object sender, EventArgs e)
        {

        }
    }
}
