﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;
using System.Globalization;

namespace HRAS
{
    public partial class HRAS_Medical_Record_Edit_View : Form
    {
        private MedicalRecord currentRecord;
        private MedicalRecord oldRecord;
        private Patient currentPatient;

        public HRAS_Medical_Record_Edit_View(MedicalRecord inRecord,Patient inPatient)
        {
           
            InitializeComponent();

            oldRecord = new MedicalRecord();
            oldRecord.deepCopy(inRecord);
           
            currentPatient = new Patient();
            currentPatient.deepCopy(inPatient);

            currentRecord = new MedicalRecord();
            currentRecord.deepCopy(inRecord); //in case users exits before saving ,we keep old values

            DateTime localTime = DateTime.Now;
            //as soon as somebody enters edit view lock the record
            PatientHandler.insertMedicalRecordLock(currentPatient, oldRecord, HRAS_Login_View.getUser(), localTime);

            displaySymptomsFromRecords();
            displayInfoToFields();
           
            entryDate.ReadOnly = true;
            exitDate.ReadOnly = true;
            roomNumber.ReadOnly = true;

            view.Enabled = false;
        }

        private void displaySymptomsFromRecords()
        {
            
            //display all symptoms in the dropdown list
            foreach (Symptom s in PatientHandler.getSymptoms())
            {
                combo.Items.Add(s.name);
            }

            if (combo.Items.Count == 0)
            {
                symptomsMessage.Text = "There are no symptoms in our records";
            }

            List<Symptom> symptoms = new List<Symptom>();
            symptoms = oldRecord.getSymptoms();

            //display each symptom from med rec to listcheckbox from user to see,
            //and remove same symptoms from dropdown box
            foreach (Symptom symptom in symptoms)
            {
                checkedList.Items.Add(symptom.name);
                combo.Items.Remove(symptom.name);
            }
        }

        private void displayInfoToFields()
        {
            //display patient info in the texboxes
            firstName.Text = currentPatient.firstName;
            lastName.Text = currentPatient.lastName;
            ssn.Text = currentPatient.SSN;

            //display med rec info
            insurer.Text = oldRecord.insurer;
            physician.Text = oldRecord.physician;
            diagnosis.Text = oldRecord.diagnosis;
            notes.Text = oldRecord.notes;
            roomNumber.Text = oldRecord.roomNumber;

            if (oldRecord.doNotResuscitate == true) { dnr.Text = "Y"; }
            else { dnr.Text = "N"; };

            string month;
            string day;

            //if day or month a single digit concatinate '0' to it
            //entrydate
            if (Convert.ToString(oldRecord.entryDate.Month).Length == 1) { month = "0" + oldRecord.entryDate.Month; }          
            else { month = Convert.ToString(oldRecord.entryDate.Month); }

            if (Convert.ToString(oldRecord.entryDate.Day).Length == 1) { day = "0" + oldRecord.entryDate.Day; }           
            else { day = Convert.ToString(oldRecord.entryDate.Day); }
            entryDate.Text = Convert.ToString(month + "/" + day + "/" + oldRecord.entryDate.Year);

            //exitdate
            if (Convert.ToString(oldRecord.exitDate).Length > 1)
            {
                if (Convert.ToString(oldRecord.exitDate.Value.Month).Length == 1) { month = "0" + oldRecord.exitDate.Value.Month; }
                else { month = Convert.ToString(oldRecord.exitDate.Value.Month); }

                if (Convert.ToString(oldRecord.exitDate.Value.Day).Length == 1) { day = "0" + oldRecord.exitDate.Value.Day; }
                else { day = Convert.ToString(oldRecord.exitDate.Value.Day); }
                exitDate.Text = Convert.ToString(month + "/" + day + "/" + oldRecord.exitDate.Value.Year);
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            PatientHandler handler = new PatientHandler();

            if (isValidInput())
            {
                //take new,edited values and store them into currentRecord
                currentRecord.entryDate = DateTime.Parse(entryDate.Text);
                if (exitDate.Text.Length > 0) currentRecord.exitDate = DateTime.Parse(exitDate.Text);
                currentRecord.roomNumber = roomNumber.Text;
                currentRecord.physician = physician.Text;
                currentRecord.notes = notes.Text;
                currentRecord.diagnosis = diagnosis.Text;
                currentRecord.insurer = insurer.Text;

                if (dnr.Text.Equals("y") || dnr.Text.Equals("Y")) currentRecord.doNotResuscitate = true;
                else currentRecord.doNotResuscitate = false;

                //empty the list first to avoid duplicates when editing
                currentRecord.getSymptoms().Clear();

                //add each symptom from checked list to medical record
                foreach (string s in checkedList.Items.OfType<string>().ToList())
                {
                    currentRecord.addSymptom(new Symptom(s));
                }
                try
                {
                    handler.updateMedicalRecord(oldRecord, currentRecord, currentPatient,HRAS_Login_View.getUser());

                    view.Enabled = true;
                    viewLB.Hide();

                    message.Text = "Medical Record Edited";
                    message.ForeColor = System.Drawing.Color.ForestGreen;
                    save.Enabled = false;

                    setFieldsToReadOnly();

                }
                catch (Exception ex)
                {
                    if (ex.Message.Equals("There is no such room in our records"))
                    {
                        roomNumberError.Text = ex.Message;
                    }
                    else
                    {
                        message.Text = ex.Message;
                        message.ForeColor = System.Drawing.Color.Red;
                    }

                    currentRecord.getSymptoms().Clear();
                }
            }
        }
        private void setFieldsToReadOnly()
        {
            exitDate.ReadOnly = true;
            physician.ReadOnly = true;
            insurer.ReadOnly = true;
            notes.ReadOnly = true;
            roomNumber.ReadOnly = true;
            diagnosis.ReadOnly = true;
            dnr.ReadOnly = true;
            combo.Enabled = false;
            checkedList.Enabled = false;
            remove.Enabled = false;
        }
        private void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //add selected symptom to checkedList and remove that symptom from dropdown list
            int selectedIndex = combo.SelectedIndex;
            Object selectedItem = combo.SelectedItem;
            string symptomName = selectedItem.ToString();
            checkedList.Items.Add(symptomName);
            combo.Items.Remove(symptomName);
        }

        private void remove_Click(object sender, EventArgs e)
        {
            //remove each checked symptom from checkedlistbox and add that symptom back to dropdownlist         
            foreach (string symptomName in checkedList.CheckedItems.OfType<string>().ToList())
            {
                checkedList.Items.Remove(symptomName);             
                combo.Items.Add(symptomName);
            }
        }

        private bool isValidInput()
        {
            bool isValid = true;
            //checking input validity textbox by texbox

            //room number
            if (roomNumber.Text.Length == 0)
            {
                roomNumberError.Text = "Required Field";
                isValid = false;
            }
            else if (roomNumber.Text.Length > 9)
            {
                roomNumberError.Text = "Must be at most 9 chars";
                isValid = false;
            }
            else { roomNumberError.Text = ""; }

            //physician
            if (physician.Text.Length == 0)
            {
                physicianError.Text = "Required Field";
                isValid = false;
            }
            else if (physician.Text.Length > 100)
            {
                physicianError.Text = "Must be at most 100 chars";
                isValid = false;
            }
            else { physicianError.Text = ""; }


            //diagnosis           
            if (diagnosis.Text.Length > 200)
            {
                diagnosisError.Text = "Must be at most 200 chars";
                isValid = false;
            }
            else { diagnosisError.Text = ""; }

            //dnr
            if (dnr.Text.Length == 0)
            {
                dnrError.Text = "Required Field";
                isValid = false;
            }
            else if (!dnr.Text.Equals("Y") && !dnr.Text.Equals("y") && !dnr.Text.Equals("N") && !dnr.Text.Equals("n"))
            {
                dnrError.Text = "Must be Y or N";
                isValid = false;
            }
            else { dnrError.Text = ""; }

            //notes          
            if (notes.Text.Length > 8000)
            {
                notesError.Text = "Must be at most 8000 chars";
                isValid = false;
            }
            else { notesError.Text = ""; }

            //exitDate
            DateTime Test2;

            if (exitDate.Text.Length > 0)
            {
                if (!DateTime.TryParseExact(exitDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Test2))
                {
                    exitDateError.Text = "Must be in given format";
                    isValid = false;
                }
                else if (Test2.Year > 9999 || Test2.Year < 1753)
                {
                    exitDateError.Text = "Must be between 1753 and 9999";
                    isValid = false;
                }
                else { exitDateError.Text = ""; }
            }

            else { exitDateError.Text = ""; }

            DateTime entry;
            DateTime exit;
            //entrydate exitdate comparison
            if (exitDate.Text.Length > 0)
            {
                if (DateTime.TryParseExact(exitDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out exit) &&
                    DateTime.TryParseExact(entryDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out entry) &&
                     (exit.Year < 9999 && exit.Year > 1753) &&
                     (entry.Year < 9999 && entry.Year > 1753))
                {
                    if (DateTime.Compare(entry, exit) > 0)
                    {
                        dateComparisonError.Text = "Must be later than entry date";
                        isValid = false;
                    }
                    else { dateComparisonError.Text = ""; }
                }

                else { dateComparisonError.Text = ""; }
            }

            else { dateComparisonError.Text = ""; }

            //insurer

            if (insurer.Text.Length > 50)
            {
                insurerError.Text = "Must be at most 50 chars";
                isValid = false;
            }
            else { insurerError.Text = ""; }


            if (!isValid)
            {
                message.Text = "";
                view.Enabled = false;
            }
            return isValid;
        }
        public void HMI_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                PatientHandler.deleteMedicalRecordLock(currentPatient, currentRecord);
            }
        }

        private void home_Click(object sender, EventArgs e)
        {
            PatientHandler.deleteMedicalRecordLock(currentPatient, currentRecord);
            Hide();
            HRAS_Home_View view = new HRAS_Home_View();
            view.Closed += (s, args) => Close();
            view.Show();
        }

        private void checkOut_Click(object sender, EventArgs e)
        {
            PatientHandler.deleteMedicalRecordLock(currentPatient, currentRecord);
            //navigates to HRAS_Patient_Bill_View
           
            Hide();
            HRAS_Patient_Bill_View view = new HRAS_Patient_Bill_View( currentPatient, currentRecord);
            view.Closed += (s, args) => Close();
            view.Show();
        }
        
        private void logout_Click(object sender, EventArgs e)
        {
            PatientHandler.deleteMedicalRecordLock(currentPatient, currentRecord);
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View view = new HRAS_Login_View();
            view.Closed += (s, args) => Close();
            view.Show();
        }
        private void view_Click(object sender, EventArgs e)
        {
            PatientHandler.deleteMedicalRecordLock(currentPatient, currentRecord);
            Hide();
            HRAS_Medical_Record_View_View view = new HRAS_Medical_Record_View_View(currentRecord,currentPatient);
            view.Closed += (s, args) => Close();
            view.Show();
        }
        private void HomeLabel_Click(object sender, EventArgs e)
        {
            PatientHandler.deleteMedicalRecordLock(currentPatient, currentRecord);
            Hide();
            HRAS_Home_View view = new HRAS_Home_View();
            view.Closed += (s, args) => Close();
            view.Show();
        }

        private void lastName_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Patient_View_View_Load(object sender, EventArgs e)
        {
        }

        private void diagnosisLB_Click(object sender, EventArgs e)
        {
        }

        private void lastNameTitle_Click(object sender, EventArgs e)
        {

        }

        private void notes_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
