﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Inventory_Item_View_View : Form
    {
        private readonly InventoryItem currentItem;

        public HRAS_Inventory_Item_View_View(InventoryItem inItem)
        {
            InitializeComponent();

            if(!HRAS_Login_View.getUser().isInventoryAdministrator)
            {
                Edit.Enabled = false;
                adminMessage.Text = "You Are Not Inventory Administrator";
            }

            //remember current Item 
            currentItem = new InventoryItem();
            currentItem.deepCopy(inItem);
          
            displayInfoToFields();

            //all fields should be readonly
            setFieldsToReadOnly();
            
        }
        private void displayInfoToFields()
        {
            StockID.Text = currentItem.id;
            Size.Text = currentItem.size.ToString();
            Cost.Text = currentItem.cost.ToString();
            Descritption.Text = currentItem.description;

            if (currentItem.isVIRTL)
            {
                virtualItem.Text = "Y";
                Quantity.Text = "UNLIMITED";
            }
            else
            {
                virtualItem.Text = "N";
                Quantity.Text = currentItem.quantity.ToString();
            }
                 
        }
        private void setFieldsToReadOnly()
        {
            StockID.ReadOnly = true;
            Quantity.ReadOnly = true;
            Size.ReadOnly = true;
            Cost.ReadOnly = true;
            Descritption.ReadOnly = true;
            virtualItem.ReadOnly = true;
        }
        private void Edit_Click_1(object sender, EventArgs e)
        {
            //navigates to HRAS_Inventory_Item_Edit_View

            Hide();
            HRAS_Inventory_Item_Edit_View itemEditWindow = new HRAS_Inventory_Item_Edit_View(currentItem);
            itemEditWindow.Closed += (s, args) => Close();
            itemEditWindow.Show();
        }

        private void homeScreen_Click_1(object sender, EventArgs e)
        {

            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View loginWindow = new HRAS_Home_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void Logout_Click_1(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void hraslabel_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View harsMain = new HRAS_Home_View();
            harsMain.Closed += (s, args) => Close();
            harsMain.Show();
        }

        private void SockID_TextChanged(object sender, EventArgs e)
        {
        }

        private void Quantity_TextChanged(object sender, EventArgs e)
        {
        }

        private void Size_TextChanged(object sender, EventArgs e)
        {
        }

        private void Cost_TextChanged(object sender, EventArgs e)
        {
        }

        private void Descritption_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Inventory_Item_Record_Load(object sender, EventArgs e)
        {
        }

        private void label5_Click(object sender, EventArgs e)
        {
        }

       
       
    }
}