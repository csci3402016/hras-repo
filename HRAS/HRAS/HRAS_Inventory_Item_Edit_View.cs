﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Inventory_Item_Edit_View : Form
    {
        
        private InventoryItem currentItem;

        public HRAS_Inventory_Item_Edit_View(InventoryItem inItem)
        {
            InitializeComponent();

            view.Enabled = false;
            currentItem = new InventoryItem();
            currentItem.deepCopy(inItem);

            displayInfoToFields();
            
            DescriptionTextBox.ReadOnly = true;
            StockIDtextBox.ReadOnly = true;
            SizeTextBox.ReadOnly = true;
            virtualItem.ReadOnly = true;

        }
        private void displayInfoToFields()
        {
            StockIDtextBox.Text = currentItem.id;         
            SizeTextBox.Text = currentItem.size.ToString();
            CostTextBox.Text = currentItem.cost.ToString();
            DescriptionTextBox.Text = currentItem.description;

            if (currentItem.isVIRTL)
            {
                virtualItem.Text = "Y";
                QuantitytextBox.Text = "UNLIMITED";
                QuantitytextBox.ReadOnly = true;
            }
            else
            {
                virtualItem.Text = "N";
                QuantitytextBox.Text = currentItem.quantity.ToString();
            }
          
        }
        private void SaveButton_Click(object sender, EventArgs e)
        {
            message.Text = "";

            ItemHandler handler = new ItemHandler();

            //take values form textboxes(edted and unedited)            
            currentItem.size = Double.Parse(SizeTextBox.Text);
            currentItem.cost = Double.Parse(CostTextBox.Text);
            currentItem.description = DescriptionTextBox.Text;
            currentItem.id = StockIDtextBox.Text;
                   
            if (virtualItem.Text.ToUpper() == "Y")
            {
                currentItem.isVIRTL = true;               
            }
            else
            {
                currentItem.isVIRTL = false;
                currentItem.quantity = Int32.Parse(QuantitytextBox.Text);
            }
            try
            {
                handler.editItem(currentItem);

                view.Enabled = true;
                viewLB.Hide();
                message.Text = "Inventory Item Edited";
                message.ForeColor = System.Drawing.Color.ForestGreen;
                SaveButton.Enabled = false;

                QuantitytextBox.ReadOnly = true;
                CostTextBox.ReadOnly = true;
                virtualItem.ReadOnly = true;

            }
            catch (Exception ex)
            {
                message.Text = ex.Message;
                message.ForeColor = System.Drawing.Color.Red;
            }

        }
        private bool isValidInput()
        {
            bool isValid = true;
            //checking input validity textbox by texbox

            //Quantity
            int b = 0;
            int.TryParse(QuantitytextBox.Text, out b);

            if (QuantitytextBox.Text.Length == 0 && virtualItem.Text.ToUpper() != "Y")
            {
                quantityError.Text = "Required Field ( for non-virtual items )";
                isValid = false;
            }
            else if (b == 0)
            {
                quantityError.Text = "Must be a whole number";
                isValid = false;
            }
            else { quantityError.Text = ""; }

            //Cost
            double d = 0;
            double.TryParse(CostTextBox.Text, out d);

            if (CostTextBox.Text.Length == 0)
            {
                costError.Text = "Required Field";
                isValid = false;
            }
            else if (d == 0)
            {
                costError.Text = "Must be a real number";
                isValid = false;
            }
            else { costError.Text = ""; }

            //virtual
            if (virtualItem.Text.Length == 0)
            {
                virtualError.Text = "Required Field";
                isValid = false;
            }
            else if (!(virtualItem.Text.ToUpper() == "Y" || virtualItem.Text.ToUpper() == "N"))
            {
                virtualError.Text = "Must be Y/N";
                isValid = false;
            }
            else { virtualError.Text = ""; }

            if (!isValid)
            {
                message.Text = "";
                view.Enabled = false;
            }

            return isValid;
        }
        private void virtualItem_TextChanged(object sender, EventArgs e)
        {
            if (virtualItem.Text.ToUpper() == "Y")
            {
                QuantitytextBox.Text = "UNLIMITED";
                QuantitytextBox.ReadOnly = true;
            }
            else
            {
                QuantitytextBox.Text = "";
                QuantitytextBox.ReadOnly = false;
            }
        }

        private void HRAS_Label_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void home_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }
    
        private void view_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Inventory_Item_View_View

            Hide();
            HRAS_Inventory_Item_View_View home = new HRAS_Inventory_Item_View_View(currentItem);
            home.Closed += (s, args) => Close();
            home.Show();

        }
        private void StockIDtextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void QuantitytextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void SizeTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void CostTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void DescriptionTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Inventory_Item_Edit_View_Load(object sender, EventArgs e)
        {

        }
  
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void DescriptionTextBox_TextChanged_1(object sender, EventArgs e)
        {

        }

        
    }
}
