﻿using HRAS_Middleware;
using System.Collections.Generic;

namespace HRAS
{
    partial class HRAS_Medical_Record_Create_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MedicalRecordLB = new System.Windows.Forms.Label();
            this.logout = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.notesLB = new System.Windows.Forms.Label();
            this.diagnosisLB = new System.Windows.Forms.Label();
            this.symptomsLB = new System.Windows.Forms.Label();
            this.entryDateLB = new System.Windows.Forms.Label();
            this.exitDateLB = new System.Windows.Forms.Label();
            this.roomNumberLB = new System.Windows.Forms.Label();
            this.physicianLB = new System.Windows.Forms.Label();
            this.insurerLB = new System.Windows.Forms.Label();
            this.dnrLB = new System.Windows.Forms.Label();
            this.HRAS_HomeLabel = new System.Windows.Forms.Label();
            this.dnr = new System.Windows.Forms.TextBox();
            this.insurer = new System.Windows.Forms.TextBox();
            this.physician = new System.Windows.Forms.TextBox();
            this.roomNumber = new System.Windows.Forms.TextBox();
            this.exitDate = new System.Windows.Forms.TextBox();
            this.entryDate = new System.Windows.Forms.TextBox();
            this.firsNameTitle = new System.Windows.Forms.Label();
            this.lastNameTitle = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.Label();
            this.firstName = new System.Windows.Forms.Label();
            this.viewLB = new System.Windows.Forms.Label();
            this.view = new System.Windows.Forms.Button();
            this.combo = new System.Windows.Forms.ComboBox();
            this.checkedList = new System.Windows.Forms.CheckedListBox();
            this.remove = new System.Windows.Forms.Button();
            this.chosenSymptomsLB = new System.Windows.Forms.Label();
            this.exitDateError = new System.Windows.Forms.Label();
            this.entryDateError = new System.Windows.Forms.Label();
            this.dnrError = new System.Windows.Forms.Label();
            this.insurerError = new System.Windows.Forms.Label();
            this.physicianError = new System.Windows.Forms.Label();
            this.roomNumberError = new System.Windows.Forms.Label();
            this.diagnosisError = new System.Windows.Forms.Label();
            this.notesError = new System.Windows.Forms.Label();
            this.symptomsMessage = new System.Windows.Forms.Label();
            this.ssn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.notes = new System.Windows.Forms.RichTextBox();
            this.diagnosis = new System.Windows.Forms.RichTextBox();
            this.dateComparisonError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MedicalRecordLB
            // 
            this.MedicalRecordLB.AutoSize = true;
            this.MedicalRecordLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedicalRecordLB.Location = new System.Drawing.Point(7, 97);
            this.MedicalRecordLB.Name = "MedicalRecordLB";
            this.MedicalRecordLB.Size = new System.Drawing.Size(432, 38);
            this.MedicalRecordLB.TabIndex = 96;
            this.MedicalRecordLB.Text = "Medical Record Creation for:";
            this.MedicalRecordLB.Click += new System.EventHandler(this.MedicalRecordLB_Click);
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(896, 34);
            this.logout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(105, 35);
            this.logout.TabIndex = 95;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(1000, 668);
            this.Back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(120, 23);
            this.Back.TabIndex = 94;
            this.Back.Text = "Home Screen";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.back_Click);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(492, 669);
            this.save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(103, 23);
            this.save.TabIndex = 92;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // notesLB
            // 
            this.notesLB.AutoSize = true;
            this.notesLB.Location = new System.Drawing.Point(498, 493);
            this.notesLB.Name = "notesLB";
            this.notesLB.Size = new System.Drawing.Size(112, 17);
            this.notesLB.TabIndex = 90;
            this.notesLB.Text = "Notes (Optional)";
            // 
            // diagnosisLB
            // 
            this.diagnosisLB.AutoSize = true;
            this.diagnosisLB.Location = new System.Drawing.Point(498, 365);
            this.diagnosisLB.Name = "diagnosisLB";
            this.diagnosisLB.Size = new System.Drawing.Size(137, 17);
            this.diagnosisLB.TabIndex = 89;
            this.diagnosisLB.Text = "Diagnosis (Optional)";
            this.diagnosisLB.Click += new System.EventHandler(this.diagnosisLB_Click);
            // 
            // symptomsLB
            // 
            this.symptomsLB.AutoSize = true;
            this.symptomsLB.Location = new System.Drawing.Point(498, 156);
            this.symptomsLB.Name = "symptomsLB";
            this.symptomsLB.Size = new System.Drawing.Size(140, 17);
            this.symptomsLB.TabIndex = 88;
            this.symptomsLB.Text = "Symptoms (Optional)";
            // 
            // entryDateLB
            // 
            this.entryDateLB.AutoSize = true;
            this.entryDateLB.Location = new System.Drawing.Point(51, 322);
            this.entryDateLB.Name = "entryDateLB";
            this.entryDateLB.Size = new System.Drawing.Size(163, 17);
            this.entryDateLB.TabIndex = 85;
            this.entryDateLB.Text = "Entry Date (mm/dd/yyyy)";
            // 
            // exitDateLB
            // 
            this.exitDateLB.AutoSize = true;
            this.exitDateLB.Location = new System.Drawing.Point(51, 388);
            this.exitDateLB.Name = "exitDateLB";
            this.exitDateLB.Size = new System.Drawing.Size(152, 17);
            this.exitDateLB.TabIndex = 84;
            this.exitDateLB.Text = "Exit Date (mm/dd/yyyy)";
            // 
            // roomNumberLB
            // 
            this.roomNumberLB.AutoSize = true;
            this.roomNumberLB.Location = new System.Drawing.Point(51, 454);
            this.roomNumberLB.Name = "roomNumberLB";
            this.roomNumberLB.Size = new System.Drawing.Size(99, 17);
            this.roomNumberLB.TabIndex = 83;
            this.roomNumberLB.Text = "Room Number";
            // 
            // physicianLB
            // 
            this.physicianLB.AutoSize = true;
            this.physicianLB.Location = new System.Drawing.Point(51, 519);
            this.physicianLB.Name = "physicianLB";
            this.physicianLB.Size = new System.Drawing.Size(68, 17);
            this.physicianLB.TabIndex = 82;
            this.physicianLB.Text = "Physician";
            // 
            // insurerLB
            // 
            this.insurerLB.AutoSize = true;
            this.insurerLB.Location = new System.Drawing.Point(51, 582);
            this.insurerLB.Name = "insurerLB";
            this.insurerLB.Size = new System.Drawing.Size(119, 17);
            this.insurerLB.TabIndex = 81;
            this.insurerLB.Text = "(Optional) Insurer";
            // 
            // dnrLB
            // 
            this.dnrLB.AutoSize = true;
            this.dnrLB.Location = new System.Drawing.Point(51, 650);
            this.dnrLB.Name = "dnrLB";
            this.dnrLB.Size = new System.Drawing.Size(71, 17);
            this.dnrLB.TabIndex = 78;
            this.dnrLB.Text = "DNR(Y/N)";
            // 
            // HRAS_HomeLabel
            // 
            this.HRAS_HomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRAS_HomeLabel.Location = new System.Drawing.Point(397, 9);
            this.HRAS_HomeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HRAS_HomeLabel.Name = "HRAS_HomeLabel";
            this.HRAS_HomeLabel.Size = new System.Drawing.Size(261, 98);
            this.HRAS_HomeLabel.TabIndex = 68;
            this.HRAS_HomeLabel.Text = "HRAS";
            this.HRAS_HomeLabel.Click += new System.EventHandler(this.HRAS_HomeLabel_Click);
            // 
            // dnr
            // 
            this.dnr.Location = new System.Drawing.Point(52, 669);
            this.dnr.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dnr.Name = "dnr";
            this.dnr.Size = new System.Drawing.Size(45, 22);
            this.dnr.TabIndex = 66;
            // 
            // insurer
            // 
            this.insurer.Location = new System.Drawing.Point(52, 601);
            this.insurer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.insurer.Name = "insurer";
            this.insurer.Size = new System.Drawing.Size(161, 22);
            this.insurer.TabIndex = 61;
            // 
            // physician
            // 
            this.physician.Location = new System.Drawing.Point(52, 538);
            this.physician.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.physician.Name = "physician";
            this.physician.Size = new System.Drawing.Size(162, 22);
            this.physician.TabIndex = 60;
            // 
            // roomNumber
            // 
            this.roomNumber.Location = new System.Drawing.Point(52, 473);
            this.roomNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.roomNumber.Name = "roomNumber";
            this.roomNumber.Size = new System.Drawing.Size(162, 22);
            this.roomNumber.TabIndex = 59;
            // 
            // exitDate
            // 
            this.exitDate.Location = new System.Drawing.Point(53, 407);
            this.exitDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitDate.Name = "exitDate";
            this.exitDate.Size = new System.Drawing.Size(160, 22);
            this.exitDate.TabIndex = 58;
            // 
            // entryDate
            // 
            this.entryDate.Location = new System.Drawing.Point(53, 348);
            this.entryDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.entryDate.Name = "entryDate";
            this.entryDate.Size = new System.Drawing.Size(162, 22);
            this.entryDate.TabIndex = 57;
            // 
            // firsNameTitle
            // 
            this.firsNameTitle.AutoSize = true;
            this.firsNameTitle.Location = new System.Drawing.Point(34, 156);
            this.firsNameTitle.Name = "firsNameTitle";
            this.firsNameTitle.Size = new System.Drawing.Size(80, 17);
            this.firsNameTitle.TabIndex = 100;
            this.firsNameTitle.Text = "First Name:";
            // 
            // lastNameTitle
            // 
            this.lastNameTitle.AutoSize = true;
            this.lastNameTitle.Location = new System.Drawing.Point(34, 187);
            this.lastNameTitle.Name = "lastNameTitle";
            this.lastNameTitle.Size = new System.Drawing.Size(80, 17);
            this.lastNameTitle.TabIndex = 99;
            this.lastNameTitle.Text = "Last Name:";
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(148, 187);
            this.lastName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(124, 22);
            this.lastName.TabIndex = 98;
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(148, 156);
            this.firstName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(124, 22);
            this.firstName.TabIndex = 97;
            // 
            // viewLB
            // 
            this.viewLB.AutoSize = true;
            this.viewLB.Location = new System.Drawing.Point(625, 641);
            this.viewLB.Name = "viewLB";
            this.viewLB.Size = new System.Drawing.Size(207, 17);
            this.viewLB.TabIndex = 106;
            this.viewLB.Text = "Must Save Medical Record First";
            // 
            // view
            // 
            this.view.Location = new System.Drawing.Point(657, 669);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(114, 23);
            this.view.TabIndex = 105;
            this.view.Text = "View";
            this.view.UseVisualStyleBackColor = true;
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // combo
            // 
            this.combo.FormattingEnabled = true;
            this.combo.Location = new System.Drawing.Point(501, 180);
            this.combo.Name = "combo";
            this.combo.Size = new System.Drawing.Size(246, 24);
            this.combo.Sorted = true;
            this.combo.TabIndex = 107;
            this.combo.Text = "Select Symptoms";
            this.combo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // checkedList
            // 
            this.checkedList.FormattingEnabled = true;
            this.checkedList.Location = new System.Drawing.Point(807, 176);
            this.checkedList.Name = "checkedList";
            this.checkedList.Size = new System.Drawing.Size(242, 123);
            this.checkedList.Sorted = true;
            this.checkedList.TabIndex = 108;
            this.checkedList.SelectedIndexChanged += new System.EventHandler(this.checkedList_SelectedIndexChanged);
            // 
            // remove
            // 
            this.remove.Location = new System.Drawing.Point(807, 309);
            this.remove.Name = "remove";
            this.remove.Size = new System.Drawing.Size(75, 23);
            this.remove.TabIndex = 109;
            this.remove.Text = "Remove";
            this.remove.UseVisualStyleBackColor = true;
            this.remove.Click += new System.EventHandler(this.remove_Click);
            // 
            // chosenSymptomsLB
            // 
            this.chosenSymptomsLB.AutoSize = true;
            this.chosenSymptomsLB.Location = new System.Drawing.Point(862, 156);
            this.chosenSymptomsLB.Name = "chosenSymptomsLB";
            this.chosenSymptomsLB.Size = new System.Drawing.Size(125, 17);
            this.chosenSymptomsLB.TabIndex = 110;
            this.chosenSymptomsLB.Text = "Chosen Symptoms";
            // 
            // exitDateError
            // 
            this.exitDateError.AutoSize = true;
            this.exitDateError.ForeColor = System.Drawing.Color.Red;
            this.exitDateError.Location = new System.Drawing.Point(219, 388);
            this.exitDateError.Name = "exitDateError";
            this.exitDateError.Size = new System.Drawing.Size(0, 17);
            this.exitDateError.TabIndex = 111;
            // 
            // entryDateError
            // 
            this.entryDateError.AutoSize = true;
            this.entryDateError.ForeColor = System.Drawing.Color.Red;
            this.entryDateError.Location = new System.Drawing.Point(227, 322);
            this.entryDateError.Name = "entryDateError";
            this.entryDateError.Size = new System.Drawing.Size(0, 17);
            this.entryDateError.TabIndex = 112;
            // 
            // dnrError
            // 
            this.dnrError.AutoSize = true;
            this.dnrError.ForeColor = System.Drawing.Color.Red;
            this.dnrError.Location = new System.Drawing.Point(142, 650);
            this.dnrError.Name = "dnrError";
            this.dnrError.Size = new System.Drawing.Size(0, 17);
            this.dnrError.TabIndex = 113;
            // 
            // insurerError
            // 
            this.insurerError.AutoSize = true;
            this.insurerError.ForeColor = System.Drawing.Color.Red;
            this.insurerError.Location = new System.Drawing.Point(189, 582);
            this.insurerError.Name = "insurerError";
            this.insurerError.Size = new System.Drawing.Size(0, 17);
            this.insurerError.TabIndex = 114;
            // 
            // physicianError
            // 
            this.physicianError.AutoSize = true;
            this.physicianError.ForeColor = System.Drawing.Color.Red;
            this.physicianError.Location = new System.Drawing.Point(142, 519);
            this.physicianError.Name = "physicianError";
            this.physicianError.Size = new System.Drawing.Size(0, 17);
            this.physicianError.TabIndex = 115;
            // 
            // roomNumberError
            // 
            this.roomNumberError.AutoSize = true;
            this.roomNumberError.ForeColor = System.Drawing.Color.Red;
            this.roomNumberError.Location = new System.Drawing.Point(163, 454);
            this.roomNumberError.Name = "roomNumberError";
            this.roomNumberError.Size = new System.Drawing.Size(0, 17);
            this.roomNumberError.TabIndex = 116;
            // 
            // diagnosisError
            // 
            this.diagnosisError.AutoSize = true;
            this.diagnosisError.ForeColor = System.Drawing.Color.Red;
            this.diagnosisError.Location = new System.Drawing.Point(654, 365);
            this.diagnosisError.Name = "diagnosisError";
            this.diagnosisError.Size = new System.Drawing.Size(0, 17);
            this.diagnosisError.TabIndex = 117;
            // 
            // notesError
            // 
            this.notesError.AutoSize = true;
            this.notesError.ForeColor = System.Drawing.Color.Red;
            this.notesError.Location = new System.Drawing.Point(653, 493);
            this.notesError.Name = "notesError";
            this.notesError.Size = new System.Drawing.Size(0, 17);
            this.notesError.TabIndex = 118;
            // 
            // symptomsMessage
            // 
            this.symptomsMessage.AutoSize = true;
            this.symptomsMessage.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.symptomsMessage.Location = new System.Drawing.Point(505, 129);
            this.symptomsMessage.Name = "symptomsMessage";
            this.symptomsMessage.Size = new System.Drawing.Size(0, 17);
            this.symptomsMessage.TabIndex = 120;
            // 
            // ssn
            // 
            this.ssn.AutoSize = true;
            this.ssn.Location = new System.Drawing.Point(148, 222);
            this.ssn.Name = "ssn";
            this.ssn.Size = new System.Drawing.Size(0, 17);
            this.ssn.TabIndex = 121;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 122;
            this.label2.Text = "SSN:";
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F);
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(107, 273);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 26);
            this.message.TabIndex = 123;
            // 
            // notes
            // 
            this.notes.Location = new System.Drawing.Point(501, 514);
            this.notes.Name = "notes";
            this.notes.Size = new System.Drawing.Size(281, 109);
            this.notes.TabIndex = 124;
            this.notes.Text = "";
            // 
            // diagnosis
            // 
            this.diagnosis.Location = new System.Drawing.Point(501, 385);
            this.diagnosis.Name = "diagnosis";
            this.diagnosis.Size = new System.Drawing.Size(281, 86);
            this.diagnosis.TabIndex = 125;
            this.diagnosis.Text = "";
            // 
            // dateComparisonError
            // 
            this.dateComparisonError.AutoSize = true;
            this.dateComparisonError.ForeColor = System.Drawing.Color.Red;
            this.dateComparisonError.Location = new System.Drawing.Point(209, 388);
            this.dateComparisonError.Name = "dateComparisonError";
            this.dateComparisonError.Size = new System.Drawing.Size(0, 17);
            this.dateComparisonError.TabIndex = 126;
            // 
            // HRAS_Medical_Record_Create_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1181, 735);
            this.Controls.Add(this.dateComparisonError);
            this.Controls.Add(this.diagnosis);
            this.Controls.Add(this.notes);
            this.Controls.Add(this.message);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ssn);
            this.Controls.Add(this.symptomsMessage);
            this.Controls.Add(this.notesError);
            this.Controls.Add(this.diagnosisError);
            this.Controls.Add(this.roomNumberError);
            this.Controls.Add(this.physicianError);
            this.Controls.Add(this.insurerError);
            this.Controls.Add(this.dnrError);
            this.Controls.Add(this.entryDateError);
            this.Controls.Add(this.exitDateError);
            this.Controls.Add(this.chosenSymptomsLB);
            this.Controls.Add(this.remove);
            this.Controls.Add(this.checkedList);
            this.Controls.Add(this.combo);
            this.Controls.Add(this.viewLB);
            this.Controls.Add(this.view);
            this.Controls.Add(this.firsNameTitle);
            this.Controls.Add(this.lastNameTitle);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.MedicalRecordLB);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.save);
            this.Controls.Add(this.notesLB);
            this.Controls.Add(this.diagnosisLB);
            this.Controls.Add(this.symptomsLB);
            this.Controls.Add(this.entryDateLB);
            this.Controls.Add(this.exitDateLB);
            this.Controls.Add(this.roomNumberLB);
            this.Controls.Add(this.physicianLB);
            this.Controls.Add(this.insurerLB);
            this.Controls.Add(this.dnrLB);
            this.Controls.Add(this.HRAS_HomeLabel);
            this.Controls.Add(this.dnr);
            this.Controls.Add(this.insurer);
            this.Controls.Add(this.physician);
            this.Controls.Add(this.roomNumber);
            this.Controls.Add(this.exitDate);
            this.Controls.Add(this.entryDate);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "HRAS_Medical_Record_Create_View";
            this.Text = "Medical Record Creation";
            this.Load += new System.EventHandler(this.HRAS_Patient_View_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MedicalRecordLB;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label notesLB;
        private System.Windows.Forms.Label diagnosisLB;
        private System.Windows.Forms.Label symptomsLB;
        private System.Windows.Forms.Label entryDateLB;
        private System.Windows.Forms.Label exitDateLB;
        private System.Windows.Forms.Label roomNumberLB;
        private System.Windows.Forms.Label physicianLB;
        private System.Windows.Forms.Label insurerLB;
        private System.Windows.Forms.Label dnrLB;
        private System.Windows.Forms.Label HRAS_HomeLabel;
        private System.Windows.Forms.TextBox dnr;
        private System.Windows.Forms.TextBox insurer;
        private System.Windows.Forms.TextBox physician;
        private System.Windows.Forms.TextBox roomNumber;
        private System.Windows.Forms.TextBox exitDate;
        private System.Windows.Forms.TextBox entryDate;
        private System.Windows.Forms.Label firsNameTitle;
        private System.Windows.Forms.Label lastNameTitle;
        private System.Windows.Forms.Label lastName;
        private System.Windows.Forms.Label firstName;
        private System.Windows.Forms.Label viewLB;
        private System.Windows.Forms.Button view;
        private System.Windows.Forms.ComboBox combo;
        private System.Windows.Forms.CheckedListBox checkedList;
        private System.Windows.Forms.Button remove;
        private System.Windows.Forms.Label chosenSymptomsLB;
        private System.Windows.Forms.Label exitDateError;
        private System.Windows.Forms.Label entryDateError;
        private System.Windows.Forms.Label dnrError;
        private System.Windows.Forms.Label insurerError;
        private System.Windows.Forms.Label physicianError;
        private System.Windows.Forms.Label roomNumberError;
        private System.Windows.Forms.Label diagnosisError;
        private System.Windows.Forms.Label notesError;
        private System.Windows.Forms.Label symptomsMessage;
        private System.Windows.Forms.Label ssn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.RichTextBox notes;
        private System.Windows.Forms.RichTextBox diagnosis;
        private System.Windows.Forms.Label dateComparisonError;
    }
}