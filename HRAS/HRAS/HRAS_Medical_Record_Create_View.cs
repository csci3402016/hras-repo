﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;
using System.Globalization;

namespace HRAS
{
    public partial class HRAS_Medical_Record_Create_View : Form
    {
        private Patient currentPatient;
        private MedicalRecord newRecord;
        
        public HRAS_Medical_Record_Create_View(Patient inPatient)
        {
            InitializeComponent();
            view.Enabled = false;

            newRecord = new MedicalRecord();
            currentPatient = new Patient();

            //remember current patient
            currentPatient.deepCopy(inPatient);

            displaySymptomsFromRecords();
            displayBasicPatientInfo();
         
        }

        private void displaySymptomsFromRecords()
        {
            //display all symptoms in dropdown box
            List<string> symptomNames = new List<string>();

            foreach (Symptom s in PatientHandler.getSymptoms())
            {
                combo.Items.Add(s.name);
            }

            if (combo.Items.Count == 0)
            {
                symptomsMessage.Text = "There are no symptoms in our records";
            }
            
        }

        private void displayBasicPatientInfo()
        {
            firstName.Text = currentPatient.firstName;
            lastName.Text = currentPatient.lastName;
            ssn.Text = currentPatient.SSN;
        }

        private void save_Click(object sender, EventArgs e)
        {
            message.Text = "";
            roomNumberError.Text = "";

            PatientHandler handler = new PatientHandler();

            if (isValidInput())
            { 
                //take texbox entries
                newRecord.entryDate = DateTime.Parse(entryDate.Text);              
                if(exitDate.Text.Length > 0) newRecord.exitDate = DateTime.Parse(exitDate.Text); 
                newRecord.roomNumber = roomNumber.Text;
                newRecord.physician = physician.Text;
                newRecord.notes = notes.Text;
                newRecord.diagnosis = diagnosis.Text;
                newRecord.insurer = insurer.Text;

                if (dnr.Text.Equals("y") || dnr.Text.Equals("Y")) newRecord.doNotResuscitate = true;
                else newRecord.doNotResuscitate = false;

                //add all symptoms from the checkedList to new medical record
                foreach (string s in checkedList.Items.OfType<string>().ToList())
                {
                    newRecord.addSymptom(new Symptom(s));
                }

                try
                {
                    handler.createMedicalRecord(currentPatient, newRecord);

                    view.Enabled = true;
                    viewLB.Hide();
                    message.Text = "Medical Record Created";
                    message.ForeColor = System.Drawing.Color.ForestGreen;
                    save.Enabled = false;

                    setFieldsToReadOnly();
                    
                }
                catch (Exception ex)
                {
                    if (ex.Message.Equals("There is no such room in our records"))
                    {
                        roomNumberError.Text = ex.Message;
                    }
                    else
                    {
                        message.Text = ex.Message;
                        message.ForeColor = System.Drawing.Color.Red;
                    }

                    //clear the list so it doesnt duplicate 
                    newRecord.getSymptoms().Clear();
                }

            }
            
        }

        private bool isValidInput()
        {
            bool isValid = true;
            //checking input validity textbox by texbox

            //room number
            if (roomNumber.Text.Length == 0)
            {
                roomNumberError.Text = "Required Field";
                isValid = false;
            }
            else if (roomNumber.Text.Length > 9)
            {
                roomNumberError.Text = "Must be at most 9 chars";
                isValid = false;
            }
            else { roomNumberError.Text = ""; }

            //physician
            if (physician.Text.Length == 0)
            {
                physicianError.Text = "Required Field";
                isValid = false;
            }
            else if (physician.Text.Length > 100)
            {
                physicianError.Text = "Must be at most 100 chars";
                isValid = false;
            }
            else { physicianError.Text = ""; }


            //diagnosis           
            if (diagnosis.Text.Length > 200)
            {
                diagnosisError.Text = "Must be at most 200 chars";
                isValid = false;
            }
            else { diagnosisError.Text = ""; }

            //dnr
            if (dnr.Text.Length == 0)
            {
                dnrError.Text = "Required Field";
                isValid = false;
            }
            else if (!dnr.Text.Equals("Y") && !dnr.Text.Equals("y") && !dnr.Text.Equals("N") && !dnr.Text.Equals("n"))
            {
                dnrError.Text = "Must be Y or N";
                isValid = false;
            }
            else { dnrError.Text = ""; }

            //notes          
            if (notes.Text.Length > 8000)
            {
                notesError.Text = "Must be at most 8000 chars";
                isValid = false;
            }
            else { notesError.Text = ""; }

            //entryDate
            DateTime Test;
            if (entryDate.Text.Length == 0)
            {
                entryDateError.Text = "Required Field";
                isValid = false;
            }
            else if (!DateTime.TryParseExact(entryDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Test))
            {
                entryDateError.Text = "Must be in given format";
                isValid = false;
            }
            else if (Test.Year > 9999 || Test.Year < 1753)
            {
                entryDateError.Text = "Must be between 1753 and 9999";
                isValid = false;
            }
            else if (DateTime.Compare(currentPatient.birthDate, Test) > 0)
            {
                entryDateError.Text = "Must be later than patient's birthdate";
                isValid = false;
            }
            else { entryDateError.Text = ""; }

            //exitDate
            DateTime Test2;
            if (exitDate.Text.Length > 0)
            {
                exitDateError.Text = "Executed if statement > 0";
                if (!DateTime.TryParseExact(exitDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Test2))
                {
                    exitDateError.Text = "Must be in given format";
                    isValid = false;
                }
                else if (Test2.Year > 9999 || Test2.Year < 1753)
                {
                    exitDateError.Text = "Must be between 1753 and 9999";
                    isValid = false;
                }
                else { exitDateError.Text = ""; }
            }

            else { exitDateError.Text = ""; }

            DateTime entry;
            DateTime exit;

            //entrydate exitdate comparison
            if (exitDate.Text.Length > 0)
            {
                if (DateTime.TryParseExact(exitDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out exit) &&
                    DateTime.TryParseExact(entryDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out entry) &&
                     (exit.Year < 9999 && exit.Year > 1753) &&
                     (entry.Year < 9999 && entry.Year > 1753))
                {
                    if (DateTime.Compare(entry, exit) > 0)
                    {
                        dateComparisonError.Text = "Must be later than entry date";
                        isValid = false;
                    }
                    else { dateComparisonError.Text = ""; }
                }

                else { dateComparisonError.Text = ""; }
            }

            else { dateComparisonError.Text = ""; }

            //insurer

            if (insurer.Text.Length > 50)
            {
                insurerError.Text = "Must be at most 50 chars";
                isValid = false;
            }
            else { insurerError.Text = ""; }


            if (!isValid)
            {
                message.Text = "";
                view.Enabled = false;
            }
            return isValid;
        }
        private void setFieldsToReadOnly()
        {
            entryDate.ReadOnly = true;
            exitDate.ReadOnly = true;
            physician.ReadOnly = true;
            insurer.ReadOnly = true;
            notes.ReadOnly = true;
            roomNumber.ReadOnly = true;
            diagnosis.ReadOnly = true;
            dnr.ReadOnly = true;
            combo.Enabled = false;
            checkedList.Enabled = false;
            remove.Enabled = false;
        }

        private void remove_Click(object sender, EventArgs e)
        {
            //remove checked symptoms from checkedlist and add them to dropdown list          
            foreach (string symptomName in checkedList.CheckedItems.OfType<string>().ToList())
            {
                checkedList.Items.Remove(symptomName);
                combo.Items.Add(symptomName);
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //add selected symptom to checked list and remove it from dropdown list
            int selectedIndex = combo.SelectedIndex;
            Object selectedItem = combo.SelectedItem;
            string symptomName = selectedItem.ToString();
            checkedList.Items.Add(symptomName);
            combo.Items.Remove(symptomName);
        }

        private void back_Click(object sender, EventArgs e)
        {
           
            Hide();
            HRAS_Home_View view = new HRAS_Home_View();
            view.Closed += (s, args) => Close();
            view.Show();
        }

        private void logout_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View view = new HRAS_Login_View();
            view.Closed += (s, args) => Close();
            view.Show();
        }
        private void view_Click(object sender, EventArgs e)
        {
            Hide();
            HRAS_Medical_Record_View_View view = new HRAS_Medical_Record_View_View(newRecord, currentPatient);
            view.Closed += (s, args) => Close();
            view.Show();
        }
             
        private void HRAS_Patient_View_View_Load(object sender, EventArgs e)
        {
        }

        private void diagnosisLB_Click(object sender, EventArgs e)
        {
        }
    

        private void HRAS_HomeLabel_Click(object sender, EventArgs e)
        {

        }

        private void MedicalRecordLB_Click(object sender, EventArgs e)
        {

        }

        private void checkedList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
