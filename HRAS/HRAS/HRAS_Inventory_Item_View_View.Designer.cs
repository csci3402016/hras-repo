﻿﻿namespace HRAS
{
    partial class HRAS_Inventory_Item_View_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support  do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StockID = new System.Windows.Forms.TextBox();
            this.Quantity = new System.Windows.Forms.TextBox();
            this.Size = new System.Windows.Forms.TextBox();
            this.Cost = new System.Windows.Forms.TextBox();
            this.DescriptionLB = new System.Windows.Forms.Label();
            this.SizeLB = new System.Windows.Forms.Label();
            this.CostLB = new System.Windows.Forms.Label();
            this.QuantityLB = new System.Windows.Forms.Label();
            this.stockIFLB = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Descritption = new System.Windows.Forms.TextBox();
            this.Logout = new System.Windows.Forms.Button();
            this.homeScreen = new System.Windows.Forms.Button();
            this.Edit = new System.Windows.Forms.Button();
            this.adminMessage = new System.Windows.Forms.Label();
            this.virtualItem = new System.Windows.Forms.TextBox();
            this.virtualItemLB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StockID
            // 
            this.StockID.Location = new System.Drawing.Point(177, 137);
            this.StockID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StockID.Name = "StockID";
            this.StockID.Size = new System.Drawing.Size(295, 22);
            this.StockID.TabIndex = 2;
            this.StockID.TextChanged += new System.EventHandler(this.SockID_TextChanged);
            // 
            // Quantity
            // 
            this.Quantity.Location = new System.Drawing.Point(176, 334);
            this.Quantity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Quantity.Name = "Quantity";
            this.Quantity.Size = new System.Drawing.Size(295, 22);
            this.Quantity.TabIndex = 3;
            this.Quantity.TextChanged += new System.EventHandler(this.Quantity_TextChanged);
            // 
            // Size
            // 
            this.Size.Location = new System.Drawing.Point(177, 215);
            this.Size.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Size.Name = "Size";
            this.Size.Size = new System.Drawing.Size(295, 22);
            this.Size.TabIndex = 4;
            this.Size.TextChanged += new System.EventHandler(this.Size_TextChanged);
            // 
            // Cost
            // 
            this.Cost.Location = new System.Drawing.Point(176, 255);
            this.Cost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Cost.Name = "Cost";
            this.Cost.Size = new System.Drawing.Size(295, 22);
            this.Cost.TabIndex = 5;
            this.Cost.TextChanged += new System.EventHandler(this.Cost_TextChanged);
            // 
            // DescriptionLB
            // 
            this.DescriptionLB.AutoSize = true;
            this.DescriptionLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLB.Location = new System.Drawing.Point(56, 177);
            this.DescriptionLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DescriptionLB.Name = "DescriptionLB";
            this.DescriptionLB.Size = new System.Drawing.Size(95, 20);
            this.DescriptionLB.TabIndex = 6;
            this.DescriptionLB.Text = "Description";
            // 
            // SizeLB
            // 
            this.SizeLB.AutoSize = true;
            this.SizeLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SizeLB.Location = new System.Drawing.Point(109, 215);
            this.SizeLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SizeLB.Name = "SizeLB";
            this.SizeLB.Size = new System.Drawing.Size(42, 20);
            this.SizeLB.TabIndex = 7;
            this.SizeLB.Text = "Size";
            // 
            // CostLB
            // 
            this.CostLB.AutoSize = true;
            this.CostLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostLB.Location = new System.Drawing.Point(12, 257);
            this.CostLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.CostLB.Name = "CostLB";
            this.CostLB.Size = new System.Drawing.Size(137, 20);
            this.CostLB.TabIndex = 8;
            this.CostLB.Text = "Cost (To Patient)";
            // 
            // QuantityLB
            // 
            this.QuantityLB.AutoSize = true;
            this.QuantityLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityLB.Location = new System.Drawing.Point(79, 334);
            this.QuantityLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.QuantityLB.Name = "QuantityLB";
            this.QuantityLB.Size = new System.Drawing.Size(71, 20);
            this.QuantityLB.TabIndex = 9;
            this.QuantityLB.Text = "Quantity";
            // 
            // stockIFLB
            // 
            this.stockIFLB.AutoSize = true;
            this.stockIFLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockIFLB.Location = new System.Drawing.Point(77, 137);
            this.stockIFLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.stockIFLB.Name = "stockIFLB";
            this.stockIFLB.Size = new System.Drawing.Size(73, 20);
            this.stockIFLB.TabIndex = 10;
            this.stockIFLB.Text = "Stock ID";
            this.stockIFLB.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(160, -2);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(261, 91);
            this.label6.TabIndex = 11;
            this.label6.Text = "HRAS";
            this.label6.Click += new System.EventHandler(this.hraslabel_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 76);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(316, 39);
            this.label7.TabIndex = 12;
            this.label7.Text = "Inventory Item View";
            // 
            // Descritption
            // 
            this.Descritption.Location = new System.Drawing.Point(177, 175);
            this.Descritption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Descritption.Name = "Descritption";
            this.Descritption.Size = new System.Drawing.Size(295, 22);
            this.Descritption.TabIndex = 13;
            this.Descritption.TextChanged += new System.EventHandler(this.Descritption_TextChanged);
            // 
            // Logout
            // 
            this.Logout.Location = new System.Drawing.Point(505, 22);
            this.Logout.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(112, 44);
            this.Logout.TabIndex = 18;
            this.Logout.Text = "Log Out";
            this.Logout.UseVisualStyleBackColor = true;
            this.Logout.Click += new System.EventHandler(this.Logout_Click_1);
            // 
            // homeScreen
            // 
            this.homeScreen.Location = new System.Drawing.Point(531, 418);
            this.homeScreen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.homeScreen.Name = "homeScreen";
            this.homeScreen.Size = new System.Drawing.Size(112, 44);
            this.homeScreen.TabIndex = 19;
            this.homeScreen.Text = "Home Screen";
            this.homeScreen.UseVisualStyleBackColor = true;
            this.homeScreen.Click += new System.EventHandler(this.homeScreen_Click_1);
            // 
            // Edit
            // 
            this.Edit.Location = new System.Drawing.Point(219, 418);
            this.Edit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(112, 44);
            this.Edit.TabIndex = 20;
            this.Edit.Text = "Edit";
            this.Edit.UseVisualStyleBackColor = true;
            this.Edit.Click += new System.EventHandler(this.Edit_Click_1);
            // 
            // adminMessage
            // 
            this.adminMessage.AutoSize = true;
            this.adminMessage.Location = new System.Drawing.Point(165, 398);
            this.adminMessage.Name = "adminMessage";
            this.adminMessage.Size = new System.Drawing.Size(0, 17);
            this.adminMessage.TabIndex = 21;
            // 
            // virtualItem
            // 
            this.virtualItem.Location = new System.Drawing.Point(176, 295);
            this.virtualItem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.virtualItem.Name = "virtualItem";
            this.virtualItem.Size = new System.Drawing.Size(57, 22);
            this.virtualItem.TabIndex = 22;
            // 
            // virtualItemLB
            // 
            this.virtualItemLB.AutoSize = true;
            this.virtualItemLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.virtualItemLB.Location = new System.Drawing.Point(11, 297);
            this.virtualItemLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.virtualItemLB.Name = "virtualItemLB";
            this.virtualItemLB.Size = new System.Drawing.Size(138, 20);
            this.virtualItemLB.TabIndex = 23;
            this.virtualItemLB.Text = "Virtual Item (Y/N)";
            // 
            // HRAS_Inventory_Item_View_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 475);
            this.Controls.Add(this.virtualItemLB);
            this.Controls.Add(this.virtualItem);
            this.Controls.Add(this.adminMessage);
            this.Controls.Add(this.Edit);
            this.Controls.Add(this.homeScreen);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.Descritption);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.stockIFLB);
            this.Controls.Add(this.QuantityLB);
            this.Controls.Add(this.CostLB);
            this.Controls.Add(this.SizeLB);
            this.Controls.Add(this.DescriptionLB);
            this.Controls.Add(this.Cost);
            this.Controls.Add(this.Size);
            this.Controls.Add(this.Quantity);
            this.Controls.Add(this.StockID);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "HRAS_Inventory_Item_View_View";
            this.Text = "Item View";
            this.Load += new System.EventHandler(this.HRAS_Inventory_Item_Record_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox StockID;
        private System.Windows.Forms.TextBox Quantity;
        private System.Windows.Forms.TextBox Size;
        private System.Windows.Forms.TextBox Cost;
        private System.Windows.Forms.Label DescriptionLB;
        private System.Windows.Forms.Label SizeLB;
        private System.Windows.Forms.Label CostLB;
        private System.Windows.Forms.Label QuantityLB;
        private System.Windows.Forms.Label stockIFLB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Descritption;
        private System.Windows.Forms.Button Logout;
        private System.Windows.Forms.Button homeScreen;
        private System.Windows.Forms.Button Edit;
        private System.Windows.Forms.Label adminMessage;
        private System.Windows.Forms.TextBox virtualItem;
        private System.Windows.Forms.Label virtualItemLB;
    }
}