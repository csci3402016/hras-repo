﻿namespace HRAS
{
    partial class HRAS_Medical_Record_View_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MedicalRecordLB = new System.Windows.Forms.Label();
            this.logout = new System.Windows.Forms.Button();
            this.edit = new System.Windows.Forms.Button();
            this.checkOut = new System.Windows.Forms.Button();
            this.audit = new System.Windows.Forms.Button();
            this.notesLB = new System.Windows.Forms.Label();
            this.diagnosisLB = new System.Windows.Forms.Label();
            this.symptomsLB = new System.Windows.Forms.Label();
            this.entryDateLB = new System.Windows.Forms.Label();
            this.exitDateLB = new System.Windows.Forms.Label();
            this.roomNumberLB = new System.Windows.Forms.Label();
            this.physicianLB = new System.Windows.Forms.Label();
            this.insurerLB = new System.Windows.Forms.Label();
            this.dnrLB = new System.Windows.Forms.Label();
            this.notes = new System.Windows.Forms.RichTextBox();
            this.diagnosis = new System.Windows.Forms.RichTextBox();
            this.symptoms = new System.Windows.Forms.RichTextBox();
            this.HRAS_HomeLabel = new System.Windows.Forms.Label();
            this.organDonor = new System.Windows.Forms.Label();
            this.dnr = new System.Windows.Forms.TextBox();
            this.insurer = new System.Windows.Forms.TextBox();
            this.physician = new System.Windows.Forms.TextBox();
            this.roomNumber = new System.Windows.Forms.TextBox();
            this.exitDate = new System.Windows.Forms.TextBox();
            this.entryDate = new System.Windows.Forms.TextBox();
            this.back = new System.Windows.Forms.Button();
            this.firstNameTitle = new System.Windows.Forms.Label();
            this.lastnameTitle = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.Label();
            this.firstName = new System.Windows.Forms.Label();
            this.ssn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.isLockedLB = new System.Windows.Forms.Label();
            this.itemsToBill = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MedicalRecordLB
            // 
            this.MedicalRecordLB.AutoSize = true;
            this.MedicalRecordLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedicalRecordLB.Location = new System.Drawing.Point(9, 124);
            this.MedicalRecordLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MedicalRecordLB.Name = "MedicalRecordLB";
            this.MedicalRecordLB.Size = new System.Drawing.Size(276, 31);
            this.MedicalRecordLB.TabIndex = 96;
            this.MedicalRecordLB.Text = "Medical Record View:";
            this.MedicalRecordLB.Click += new System.EventHandler(this.MedicalRecordLB_Click);
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(452, 19);
            this.logout.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(79, 28);
            this.logout.TabIndex = 95;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // edit
            // 
            this.edit.Location = new System.Drawing.Point(59, 604);
            this.edit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(68, 19);
            this.edit.TabIndex = 94;
            this.edit.Text = "Edit";
            this.edit.UseVisualStyleBackColor = true;
            this.edit.Click += new System.EventHandler(this.edit_Click);
            // 
            // checkOut
            // 
            this.checkOut.Location = new System.Drawing.Point(378, 604);
            this.checkOut.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkOut.Name = "checkOut";
            this.checkOut.Size = new System.Drawing.Size(79, 19);
            this.checkOut.TabIndex = 93;
            this.checkOut.Text = "Check Out";
            this.checkOut.UseVisualStyleBackColor = true;
            this.checkOut.Click += new System.EventHandler(this.checkOut_Click);
            // 
            // audit
            // 
            this.audit.Location = new System.Drawing.Point(184, 604);
            this.audit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.audit.Name = "audit";
            this.audit.Size = new System.Drawing.Size(77, 19);
            this.audit.TabIndex = 92;
            this.audit.Text = "Audit History";
            this.audit.UseVisualStyleBackColor = true;
            this.audit.Click += new System.EventHandler(this.audit_Click);
            // 
            // notesLB
            // 
            this.notesLB.AutoSize = true;
            this.notesLB.Location = new System.Drawing.Point(346, 502);
            this.notesLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.notesLB.Name = "notesLB";
            this.notesLB.Size = new System.Drawing.Size(35, 13);
            this.notesLB.TabIndex = 90;
            this.notesLB.Text = "Notes";
            // 
            // diagnosisLB
            // 
            this.diagnosisLB.AutoSize = true;
            this.diagnosisLB.Location = new System.Drawing.Point(346, 401);
            this.diagnosisLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.diagnosisLB.Name = "diagnosisLB";
            this.diagnosisLB.Size = new System.Drawing.Size(53, 13);
            this.diagnosisLB.TabIndex = 89;
            this.diagnosisLB.Text = "Diagnosis";
            this.diagnosisLB.Click += new System.EventHandler(this.diagnosisLB_Click);
            // 
            // symptomsLB
            // 
            this.symptomsLB.AutoSize = true;
            this.symptomsLB.Location = new System.Drawing.Point(346, 291);
            this.symptomsLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.symptomsLB.Name = "symptomsLB";
            this.symptomsLB.Size = new System.Drawing.Size(55, 13);
            this.symptomsLB.TabIndex = 88;
            this.symptomsLB.Text = "Symptoms";
            // 
            // entryDateLB
            // 
            this.entryDateLB.AutoSize = true;
            this.entryDateLB.Location = new System.Drawing.Point(34, 291);
            this.entryDateLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.entryDateLB.Name = "entryDateLB";
            this.entryDateLB.Size = new System.Drawing.Size(57, 13);
            this.entryDateLB.TabIndex = 85;
            this.entryDateLB.Text = "Entry Date";
            // 
            // exitDateLB
            // 
            this.exitDateLB.AutoSize = true;
            this.exitDateLB.Location = new System.Drawing.Point(34, 348);
            this.exitDateLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.exitDateLB.Name = "exitDateLB";
            this.exitDateLB.Size = new System.Drawing.Size(50, 13);
            this.exitDateLB.TabIndex = 84;
            this.exitDateLB.Text = "Exit Date";
            // 
            // roomNumberLB
            // 
            this.roomNumberLB.AutoSize = true;
            this.roomNumberLB.Location = new System.Drawing.Point(34, 401);
            this.roomNumberLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.roomNumberLB.Name = "roomNumberLB";
            this.roomNumberLB.Size = new System.Drawing.Size(75, 13);
            this.roomNumberLB.TabIndex = 83;
            this.roomNumberLB.Text = "Room Number";
            // 
            // physicianLB
            // 
            this.physicianLB.AutoSize = true;
            this.physicianLB.Location = new System.Drawing.Point(34, 454);
            this.physicianLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.physicianLB.Name = "physicianLB";
            this.physicianLB.Size = new System.Drawing.Size(52, 13);
            this.physicianLB.TabIndex = 82;
            this.physicianLB.Text = "Physician";
            // 
            // insurerLB
            // 
            this.insurerLB.AutoSize = true;
            this.insurerLB.Location = new System.Drawing.Point(36, 502);
            this.insurerLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.insurerLB.Name = "insurerLB";
            this.insurerLB.Size = new System.Drawing.Size(39, 13);
            this.insurerLB.TabIndex = 81;
            this.insurerLB.Text = "Insurer";
            // 
            // dnrLB
            // 
            this.dnrLB.AutoSize = true;
            this.dnrLB.Location = new System.Drawing.Point(34, 545);
            this.dnrLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dnrLB.Name = "dnrLB";
            this.dnrLB.Size = new System.Drawing.Size(57, 13);
            this.dnrLB.TabIndex = 78;
            this.dnrLB.Text = "DNR(Y/N)";
            // 
            // notes
            // 
            this.notes.Location = new System.Drawing.Point(346, 518);
            this.notes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.notes.Name = "notes";
            this.notes.Size = new System.Drawing.Size(212, 70);
            this.notes.TabIndex = 71;
            this.notes.Text = "";
            // 
            // diagnosis
            // 
            this.diagnosis.Location = new System.Drawing.Point(346, 416);
            this.diagnosis.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.diagnosis.Name = "diagnosis";
            this.diagnosis.Size = new System.Drawing.Size(212, 79);
            this.diagnosis.TabIndex = 70;
            this.diagnosis.Text = "";
            // 
            // symptoms
            // 
            this.symptoms.Location = new System.Drawing.Point(346, 306);
            this.symptoms.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.symptoms.Name = "symptoms";
            this.symptoms.Size = new System.Drawing.Size(212, 79);
            this.symptoms.TabIndex = 69;
            this.symptoms.Text = "";
            // 
            // HRAS_HomeLabel
            // 
            this.HRAS_HomeLabel.AutoSize = true;
            this.HRAS_HomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRAS_HomeLabel.Location = new System.Drawing.Point(138, 28);
            this.HRAS_HomeLabel.Name = "HRAS_HomeLabel";
            this.HRAS_HomeLabel.Size = new System.Drawing.Size(210, 73);
            this.HRAS_HomeLabel.TabIndex = 68;
            this.HRAS_HomeLabel.Text = "HRAS";
            // 
            // organDonor
            // 
            this.organDonor.Location = new System.Drawing.Point(450, 472);
            this.organDonor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.organDonor.Name = "organDonor";
            this.organDonor.Size = new System.Drawing.Size(52, 18);
            this.organDonor.TabIndex = 67;
            // 
            // dnr
            // 
            this.dnr.Location = new System.Drawing.Point(36, 561);
            this.dnr.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dnr.Name = "dnr";
            this.dnr.Size = new System.Drawing.Size(45, 20);
            this.dnr.TabIndex = 66;
            this.dnr.Click += new System.EventHandler(this.dnr_Click);
            // 
            // insurer
            // 
            this.insurer.Location = new System.Drawing.Point(36, 518);
            this.insurer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.insurer.Name = "insurer";
            this.insurer.Size = new System.Drawing.Size(146, 20);
            this.insurer.TabIndex = 61;
            // 
            // physician
            // 
            this.physician.Location = new System.Drawing.Point(36, 470);
            this.physician.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.physician.Name = "physician";
            this.physician.Size = new System.Drawing.Size(146, 20);
            this.physician.TabIndex = 60;
            // 
            // roomNumber
            // 
            this.roomNumber.Location = new System.Drawing.Point(34, 416);
            this.roomNumber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.roomNumber.Name = "roomNumber";
            this.roomNumber.Size = new System.Drawing.Size(94, 20);
            this.roomNumber.TabIndex = 59;
            // 
            // exitDate
            // 
            this.exitDate.Location = new System.Drawing.Point(34, 363);
            this.exitDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.exitDate.Name = "exitDate";
            this.exitDate.Size = new System.Drawing.Size(94, 20);
            this.exitDate.TabIndex = 58;
            // 
            // entryDate
            // 
            this.entryDate.Location = new System.Drawing.Point(34, 306);
            this.entryDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.entryDate.Name = "entryDate";
            this.entryDate.Size = new System.Drawing.Size(94, 20);
            this.entryDate.TabIndex = 57;
            this.entryDate.Click += new System.EventHandler(this.entryDate_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(472, 604);
            this.back.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(86, 19);
            this.back.TabIndex = 99;
            this.back.Text = "Home Screen";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // firstNameTitle
            // 
            this.firstNameTitle.AutoSize = true;
            this.firstNameTitle.Location = new System.Drawing.Point(54, 181);
            this.firstNameTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.firstNameTitle.Name = "firstNameTitle";
            this.firstNameTitle.Size = new System.Drawing.Size(60, 13);
            this.firstNameTitle.TabIndex = 103;
            this.firstNameTitle.Text = "First Name:";
            // 
            // lastnameTitle
            // 
            this.lastnameTitle.AutoSize = true;
            this.lastnameTitle.Location = new System.Drawing.Point(55, 206);
            this.lastnameTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lastnameTitle.Name = "lastnameTitle";
            this.lastnameTitle.Size = new System.Drawing.Size(61, 13);
            this.lastnameTitle.TabIndex = 102;
            this.lastnameTitle.Text = "Last Name:";
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(119, 206);
            this.lastName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(93, 20);
            this.lastName.TabIndex = 101;
            this.lastName.Click += new System.EventHandler(this.lastName_Click);
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(119, 181);
            this.firstName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(93, 18);
            this.firstName.TabIndex = 100;
            // 
            // ssn
            // 
            this.ssn.AutoSize = true;
            this.ssn.Location = new System.Drawing.Point(119, 233);
            this.ssn.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ssn.Name = "ssn";
            this.ssn.Size = new System.Drawing.Size(0, 13);
            this.ssn.TabIndex = 104;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 233);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 105;
            this.label2.Text = "SSN:";
            // 
            // isLockedLB
            // 
            this.isLockedLB.AutoSize = true;
            this.isLockedLB.Location = new System.Drawing.Point(22, 588);
            this.isLockedLB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.isLockedLB.Name = "isLockedLB";
            this.isLockedLB.Size = new System.Drawing.Size(0, 13);
            this.isLockedLB.TabIndex = 106;
            // 
            // itemsToBill
            // 
            this.itemsToBill.Enabled = false;
            this.itemsToBill.Location = new System.Drawing.Point(278, 604);
            this.itemsToBill.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.itemsToBill.Name = "itemsToBill";
            this.itemsToBill.Size = new System.Drawing.Size(86, 19);
            this.itemsToBill.TabIndex = 107;
            this.itemsToBill.Text = "Add Item To Bill";
            this.itemsToBill.UseVisualStyleBackColor = true;
            // 
            // HRAS_Medical_Record_View_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 630);
            this.Controls.Add(this.itemsToBill);
            this.Controls.Add(this.isLockedLB);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ssn);
            this.Controls.Add(this.firstNameTitle);
            this.Controls.Add(this.lastnameTitle);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.back);
            this.Controls.Add(this.MedicalRecordLB);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.edit);
            this.Controls.Add(this.checkOut);
            this.Controls.Add(this.audit);
            this.Controls.Add(this.notesLB);
            this.Controls.Add(this.diagnosisLB);
            this.Controls.Add(this.symptomsLB);
            this.Controls.Add(this.entryDateLB);
            this.Controls.Add(this.exitDateLB);
            this.Controls.Add(this.roomNumberLB);
            this.Controls.Add(this.physicianLB);
            this.Controls.Add(this.insurerLB);
            this.Controls.Add(this.dnrLB);
            this.Controls.Add(this.notes);
            this.Controls.Add(this.diagnosis);
            this.Controls.Add(this.symptoms);
            this.Controls.Add(this.HRAS_HomeLabel);
            this.Controls.Add(this.organDonor);
            this.Controls.Add(this.dnr);
            this.Controls.Add(this.insurer);
            this.Controls.Add(this.physician);
            this.Controls.Add(this.roomNumber);
            this.Controls.Add(this.exitDate);
            this.Controls.Add(this.entryDate);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "HRAS_Medical_Record_View_View";
            this.Text = "Medical Record View";
            this.Load += new System.EventHandler(this.HRAS_Patient_View_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MedicalRecordLB;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Button edit;
        private System.Windows.Forms.Button checkOut;
        private System.Windows.Forms.Button audit;
        private System.Windows.Forms.Label notesLB;
        private System.Windows.Forms.Label diagnosisLB;
        private System.Windows.Forms.Label symptomsLB;
        private System.Windows.Forms.Label entryDateLB;
        private System.Windows.Forms.Label exitDateLB;
        private System.Windows.Forms.Label roomNumberLB;
        private System.Windows.Forms.Label physicianLB;
        private System.Windows.Forms.Label insurerLB;
        private System.Windows.Forms.Label dnrLB;
        private System.Windows.Forms.RichTextBox notes;
        private System.Windows.Forms.RichTextBox diagnosis;
        private System.Windows.Forms.RichTextBox symptoms;
        private System.Windows.Forms.Label HRAS_HomeLabel;
        private System.Windows.Forms.Label organDonor;
        private System.Windows.Forms.TextBox dnr;
        private System.Windows.Forms.TextBox insurer;
        private System.Windows.Forms.TextBox physician;
        private System.Windows.Forms.TextBox roomNumber;
        private System.Windows.Forms.TextBox exitDate;
        private System.Windows.Forms.TextBox entryDate;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label firstNameTitle;
        private System.Windows.Forms.Label lastnameTitle;
        private System.Windows.Forms.Label lastName;
        private System.Windows.Forms.Label firstName;
        private System.Windows.Forms.Label ssn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label isLockedLB;
        private System.Windows.Forms.Button itemsToBill;
    }
}