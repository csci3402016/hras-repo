﻿namespace HRAS
{
    partial class HRAS_Diagnosis_Wizard_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToHomePageLB = new System.Windows.Forms.Label();
            this.Logout_Button = new System.Windows.Forms.Button();
            this.Next_Button = new System.Windows.Forms.Button();
            this.Cancel_Button = new System.Windows.Forms.Button();
            this.DiagnosisWizardLB = new System.Windows.Forms.Label();
            this.YesRButton = new System.Windows.Forms.RadioButton();
            this.NoRButton = new System.Windows.Forms.RadioButton();
            this.symptomLabel = new System.Windows.Forms.Label();
            this.dBProblem = new System.Windows.Forms.Label();
            this.diagnosesListBox = new System.Windows.Forms.ListBox();
            this.noDataLabel = new System.Windows.Forms.Label();
            this.homeScreen = new System.Windows.Forms.Button();
            this.qustion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ToHomePageLB
            // 
            this.ToHomePageLB.AutoSize = true;
            this.ToHomePageLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToHomePageLB.Location = new System.Drawing.Point(250, 15);
            this.ToHomePageLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ToHomePageLB.Name = "ToHomePageLB";
            this.ToHomePageLB.Size = new System.Drawing.Size(261, 91);
            this.ToHomePageLB.TabIndex = 1;
            this.ToHomePageLB.Text = "HRAS";
            this.ToHomePageLB.Click += new System.EventHandler(this.ToHomePageLB_Click);
            // 
            // Logout_Button
            // 
            this.Logout_Button.Location = new System.Drawing.Point(619, 15);
            this.Logout_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Logout_Button.Name = "Logout_Button";
            this.Logout_Button.Size = new System.Drawing.Size(100, 28);
            this.Logout_Button.TabIndex = 2;
            this.Logout_Button.Text = "Logout";
            this.Logout_Button.UseVisualStyleBackColor = true;
            this.Logout_Button.Click += new System.EventHandler(this.Logout_Button_Click);
            // 
            // Next_Button
            // 
            this.Next_Button.Location = new System.Drawing.Point(511, 258);
            this.Next_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Next_Button.Name = "Next_Button";
            this.Next_Button.Size = new System.Drawing.Size(100, 28);
            this.Next_Button.TabIndex = 3;
            this.Next_Button.Text = "Next";
            this.Next_Button.UseVisualStyleBackColor = true;
            this.Next_Button.Click += new System.EventHandler(this.Next_Button_Click);
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.Location = new System.Drawing.Point(619, 258);
            this.Cancel_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(100, 28);
            this.Cancel_Button.TabIndex = 4;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = true;
            this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
            // 
            // DiagnosisWizardLB
            // 
            this.DiagnosisWizardLB.AutoSize = true;
            this.DiagnosisWizardLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiagnosisWizardLB.Location = new System.Drawing.Point(58, 117);
            this.DiagnosisWizardLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DiagnosisWizardLB.Name = "DiagnosisWizardLB";
            this.DiagnosisWizardLB.Size = new System.Drawing.Size(309, 42);
            this.DiagnosisWizardLB.TabIndex = 5;
            this.DiagnosisWizardLB.Text = "Diagnosis Wizard";
            // 
            // YesRButton
            // 
            this.YesRButton.AutoSize = true;
            this.YesRButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YesRButton.Location = new System.Drawing.Point(66, 258);
            this.YesRButton.Margin = new System.Windows.Forms.Padding(4);
            this.YesRButton.Name = "YesRButton";
            this.YesRButton.Size = new System.Drawing.Size(67, 29);
            this.YesRButton.TabIndex = 7;
            this.YesRButton.TabStop = true;
            this.YesRButton.Text = "Yes";
            this.YesRButton.UseVisualStyleBackColor = true;
            this.YesRButton.CheckedChanged += new System.EventHandler(this.YesRButton_CheckedChanged);
            // 
            // NoRButton
            // 
            this.NoRButton.AutoSize = true;
            this.NoRButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoRButton.Location = new System.Drawing.Point(202, 258);
            this.NoRButton.Margin = new System.Windows.Forms.Padding(4);
            this.NoRButton.Name = "NoRButton";
            this.NoRButton.Size = new System.Drawing.Size(58, 29);
            this.NoRButton.TabIndex = 8;
            this.NoRButton.TabStop = true;
            this.NoRButton.Text = "No";
            this.NoRButton.UseVisualStyleBackColor = true;
            this.NoRButton.CheckedChanged += new System.EventHandler(this.NoRButton_CheckedChanged);
            // 
            // symptomLabel
            // 
            this.symptomLabel.AutoSize = true;
            this.symptomLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.symptomLabel.Location = new System.Drawing.Point(60, 222);
            this.symptomLabel.Name = "symptomLabel";
            this.symptomLabel.Size = new System.Drawing.Size(0, 32);
            this.symptomLabel.TabIndex = 10;
            this.symptomLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // dBProblem
            // 
            this.dBProblem.AutoSize = true;
            this.dBProblem.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dBProblem.ForeColor = System.Drawing.Color.Red;
            this.dBProblem.Location = new System.Drawing.Point(90, 669);
            this.dBProblem.Name = "dBProblem";
            this.dBProblem.Size = new System.Drawing.Size(484, 32);
            this.dBProblem.TabIndex = 11;
            this.dBProblem.Text = "There is a problem with the Database";
            this.dBProblem.Visible = false;
            // 
            // diagnosesListBox
            // 
            this.diagnosesListBox.FormattingEnabled = true;
            this.diagnosesListBox.ItemHeight = 16;
            this.diagnosesListBox.Location = new System.Drawing.Point(65, 294);
            this.diagnosesListBox.Name = "diagnosesListBox";
            this.diagnosesListBox.Size = new System.Drawing.Size(654, 340);
            this.diagnosesListBox.TabIndex = 12;
            // 
            // noDataLabel
            // 
            this.noDataLabel.AutoSize = true;
            this.noDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noDataLabel.ForeColor = System.Drawing.Color.Red;
            this.noDataLabel.Location = new System.Drawing.Point(118, 656);
            this.noDataLabel.Name = "noDataLabel";
            this.noDataLabel.Size = new System.Drawing.Size(393, 64);
            this.noDataLabel.TabIndex = 13;
            this.noDataLabel.Text = "There is not enough data in \r\nour records to continue further";
            this.noDataLabel.Visible = false;
            // 
            // homeScreen
            // 
            this.homeScreen.Location = new System.Drawing.Point(592, 673);
            this.homeScreen.Margin = new System.Windows.Forms.Padding(4);
            this.homeScreen.Name = "homeScreen";
            this.homeScreen.Size = new System.Drawing.Size(127, 28);
            this.homeScreen.TabIndex = 14;
            this.homeScreen.Text = "Home Screen";
            this.homeScreen.UseVisualStyleBackColor = true;
            this.homeScreen.Click += new System.EventHandler(this.homeScreen_Click);
            // 
            // qustion
            // 
            this.qustion.AutoSize = true;
            this.qustion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F);
            this.qustion.Location = new System.Drawing.Point(59, 177);
            this.qustion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.qustion.Name = "qustion";
            this.qustion.Size = new System.Drawing.Size(303, 29);
            this.qustion.TabIndex = 15;
            this.qustion.Text = "Do you have this symptom?";
            // 
            // HRAS_Diagnosis_Wizard_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 729);
            this.Controls.Add(this.qustion);
            this.Controls.Add(this.homeScreen);
            this.Controls.Add(this.noDataLabel);
            this.Controls.Add(this.diagnosesListBox);
            this.Controls.Add(this.dBProblem);
            this.Controls.Add(this.symptomLabel);
            this.Controls.Add(this.NoRButton);
            this.Controls.Add(this.YesRButton);
            this.Controls.Add(this.DiagnosisWizardLB);
            this.Controls.Add(this.Cancel_Button);
            this.Controls.Add(this.Next_Button);
            this.Controls.Add(this.Logout_Button);
            this.Controls.Add(this.ToHomePageLB);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HRAS_Diagnosis_Wizard_View";
            this.Text = "HRAS Diagnosis Wizard";
            this.Load += new System.EventHandler(this.HRAS_Diagnosis_Wizard_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ToHomePageLB;
        private System.Windows.Forms.Button Logout_Button;
        private System.Windows.Forms.Button Next_Button;
        private System.Windows.Forms.Button Cancel_Button;
        private System.Windows.Forms.Label DiagnosisWizardLB;
        private System.Windows.Forms.RadioButton YesRButton;
        private System.Windows.Forms.RadioButton NoRButton;
        private System.Windows.Forms.Label symptomLabel;
        private System.Windows.Forms.Label dBProblem;
        private System.Windows.Forms.ListBox diagnosesListBox;
        private System.Windows.Forms.Label noDataLabel;
        private System.Windows.Forms.Button homeScreen;
        private System.Windows.Forms.Label qustion;
    }
}