﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Inventory_Item_Search_Results_View : Form
    {
        private readonly List<InventoryItem> itemList;

        public HRAS_Inventory_Item_Search_Results_View(List<InventoryItem> items, InventoryItem inItem)
        {
            InitializeComponent();
            itemList = items;
            FillData(items);

            description.Text = inItem.description;
        }

        private void logout_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void back_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void ItemSearchResults_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == ItemSearchResults.Columns["View"].Index)
            {
                Hide();
                HRAS_Inventory_Item_View_View itemView =
                    new HRAS_Inventory_Item_View_View(itemList.ElementAt(e.RowIndex));
                itemView.Closed += (s, args) => Close();
                itemView.Show();
            }
            //else if (e.ColumnIndex == ItemSearchResults.Columns["Edit"].Index)
            //{
            //    Hide();
            //    HRAS_Inventory_Item_Edit_View editView = new HRAS_Inventory_Item_Edit_View(itemList.ElementAt(e.RowIndex));
            //    editView.Closed += (s, args) => Close();
            //    editView.Show();
            //}
        }

        private void FillData(List<InventoryItem> list)
        {
            ItemSearchResults.DataSource = list;

            // add view button
            DataGridViewButtonColumn viewButtonColumn = new DataGridViewButtonColumn();
            viewButtonColumn.Name = "View";
            viewButtonColumn.Text = "View";
            viewButtonColumn.UseColumnTextForButtonValue = true;
            int columnIndex = ItemSearchResults.DisplayedColumnCount(false) + 1;

            if (ItemSearchResults.Columns["View"] == null)
            {
                ItemSearchResults.Columns.Insert(columnIndex, viewButtonColumn);
            }

            // add edit button
            //DataGridViewButtonColumn editButtonColumn = new DataGridViewButtonColumn();
            //editButtonColumn.Name = "Edit";
            //editButtonColumn.Text = "Edit";
            //editButtonColumn.UseColumnTextForButtonValue = true;
            //int columnIndexEdit = ItemSearchResults.DisplayedColumnCount(false) + 2;

            //if (ItemSearchResults.Columns["Edit"] == null)
            //{
            //    ItemSearchResults.Columns.Insert(columnIndexEdit, editButtonColumn);
            //}

            ItemSearchResults.AutoGenerateColumns = false;
            ItemSearchResults.Columns["id"].DisplayIndex = 0;           
            ItemSearchResults.Columns["Description"].DisplayIndex = 1;
            ItemSearchResults.Columns["Quantity"].DisplayIndex = 2;
            ItemSearchResults.Columns["Size"].DisplayIndex = 3;
            ItemSearchResults.Columns["Cost"].DisplayIndex = 4;

            //hide these columns
            ItemSearchResults.Columns["isVIRTL"].Visible = false;          
        }
        
        private void HRAS_Inventory_Item_Search_Results_View_Load(object sender, EventArgs e)
        {
        }
    }
}