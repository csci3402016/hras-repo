﻿namespace HRAS
{
    partial class HRAS_Medical_Record_Audit_History_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HRASLB = new System.Windows.Forms.Label();
            this.LogoutButton = new System.Windows.Forms.Button();
            this.medicalRecorAuditHistoryTitle = new System.Windows.Forms.Label();
            this.auditHistoryGrid = new System.Windows.Forms.DataGridView();
            this.back = new System.Windows.Forms.Button();
            this.firstName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.Label();
            this.ssnTitle = new System.Windows.Forms.Label();
            this.ssn = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.entryDateTitle = new System.Windows.Forms.Label();
            this.entryDate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.auditHistoryGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // HRASLB
            // 
            this.HRASLB.AutoSize = true;
            this.HRASLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRASLB.Location = new System.Drawing.Point(339, 9);
            this.HRASLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HRASLB.Name = "HRASLB";
            this.HRASLB.Size = new System.Drawing.Size(261, 91);
            this.HRASLB.TabIndex = 2;
            this.HRASLB.Text = "HRAS";
            this.HRASLB.Click += new System.EventHandler(this.HRASLB_Click);
            // 
            // LogoutButton
            // 
            this.LogoutButton.Location = new System.Drawing.Point(827, 22);
            this.LogoutButton.Margin = new System.Windows.Forms.Padding(4);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(112, 44);
            this.LogoutButton.TabIndex = 9;
            this.LogoutButton.Text = "Log Out";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // medicalRecorAuditHistoryTitle
            // 
            this.medicalRecorAuditHistoryTitle.AutoSize = true;
            this.medicalRecorAuditHistoryTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.medicalRecorAuditHistoryTitle.Location = new System.Drawing.Point(60, 115);
            this.medicalRecorAuditHistoryTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.medicalRecorAuditHistoryTitle.Name = "medicalRecorAuditHistoryTitle";
            this.medicalRecorAuditHistoryTitle.Size = new System.Drawing.Size(402, 36);
            this.medicalRecorAuditHistoryTitle.TabIndex = 10;
            this.medicalRecorAuditHistoryTitle.Text = "Medical Record Audit History";
            // 
            // auditHistoryGrid
            // 
            this.auditHistoryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.auditHistoryGrid.Location = new System.Drawing.Point(66, 312);
            this.auditHistoryGrid.Margin = new System.Windows.Forms.Padding(4);
            this.auditHistoryGrid.Name = "auditHistoryGrid";
            this.auditHistoryGrid.Size = new System.Drawing.Size(873, 309);
            this.auditHistoryGrid.TabIndex = 11;
            this.auditHistoryGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.auditHistoryGrid_CellContentClick);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(827, 643);
            this.back.Margin = new System.Windows.Forms.Padding(4);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(112, 44);
            this.back.TabIndex = 12;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // firstName
            // 
            this.firstName.AutoSize = true;
            this.firstName.Location = new System.Drawing.Point(186, 168);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(0, 17);
            this.firstName.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 199);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 23;
            this.label2.Text = "Last Name:";
            // 
            // lastName
            // 
            this.lastName.AutoSize = true;
            this.lastName.Location = new System.Drawing.Point(186, 199);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(0, 17);
            this.lastName.TabIndex = 22;
            // 
            // ssnTitle
            // 
            this.ssnTitle.AutoSize = true;
            this.ssnTitle.Location = new System.Drawing.Point(102, 233);
            this.ssnTitle.Name = "ssnTitle";
            this.ssnTitle.Size = new System.Drawing.Size(40, 17);
            this.ssnTitle.TabIndex = 21;
            this.ssnTitle.Text = "SSN:";
            // 
            // ssn
            // 
            this.ssn.AutoSize = true;
            this.ssn.Location = new System.Drawing.Point(186, 233);
            this.ssn.Name = "ssn";
            this.ssn.Size = new System.Drawing.Size(0, 17);
            this.ssn.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 19;
            this.label4.Text = "First Name:";
            // 
            // entryDateTitle
            // 
            this.entryDateTitle.AutoSize = true;
            this.entryDateTitle.Location = new System.Drawing.Point(63, 263);
            this.entryDateTitle.Name = "entryDateTitle";
            this.entryDateTitle.Size = new System.Drawing.Size(79, 17);
            this.entryDateTitle.TabIndex = 25;
            this.entryDateTitle.Text = "Entry Date:";
            // 
            // entryDate
            // 
            this.entryDate.AutoSize = true;
            this.entryDate.Location = new System.Drawing.Point(186, 263);
            this.entryDate.Name = "entryDate";
            this.entryDate.Size = new System.Drawing.Size(0, 17);
            this.entryDate.TabIndex = 26;
            // 
            // HRAS_Medical_Record_Audit_History_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 699);
            this.Controls.Add(this.entryDate);
            this.Controls.Add(this.entryDateTitle);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.ssnTitle);
            this.Controls.Add(this.ssn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.back);
            this.Controls.Add(this.auditHistoryGrid);
            this.Controls.Add(this.medicalRecorAuditHistoryTitle);
            this.Controls.Add(this.LogoutButton);
            this.Controls.Add(this.HRASLB);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HRAS_Medical_Record_Audit_History_View";
            this.Text = "Medical Record Audit History";
            this.Load += new System.EventHandler(this.HRAS_Medical_Record_Audit_History_View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.auditHistoryGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HRASLB;
        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.Label medicalRecorAuditHistoryTitle;
        private System.Windows.Forms.DataGridView auditHistoryGrid;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label firstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lastName;
        private System.Windows.Forms.Label ssnTitle;
        private System.Windows.Forms.Label ssn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label entryDateTitle;
        private System.Windows.Forms.Label entryDate;
    }
}