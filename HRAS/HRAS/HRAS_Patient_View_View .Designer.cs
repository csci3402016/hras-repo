﻿namespace HRAS
{
    partial class HRAS_Patient_View_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logout = new System.Windows.Forms.Button();
            this.patientMedicalRecordLB = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.genderLB = new System.Windows.Forms.Label();
            this.stateLB = new System.Windows.Forms.Label();
            this.zipLB = new System.Windows.Forms.Label();
            this.cityLB = new System.Windows.Forms.Label();
            this.organDonorLB = new System.Windows.Forms.Label();
            this.birthDateLB = new System.Windows.Forms.Label();
            this.ssnLB = new System.Windows.Forms.Label();
            this.mInitialLB = new System.Windows.Forms.Label();
            this.lastNameLB = new System.Windows.Forms.Label();
            this.firstNameLB = new System.Windows.Forms.Label();
            this.HRAS_HomeLabel = new System.Windows.Forms.Label();
            this.organDonor = new System.Windows.Forms.TextBox();
            this.zip = new System.Windows.Forms.TextBox();
            this.city = new System.Windows.Forms.TextBox();
            this.state = new System.Windows.Forms.TextBox();
            this.birthDate = new System.Windows.Forms.TextBox();
            this.ssn = new System.Windows.Forms.TextBox();
            this.middleInitial = new System.Windows.Forms.TextBox();
            this.gender = new System.Windows.Forms.TextBox();
            this.firstName = new System.Windows.Forms.TextBox();
            this.lastName = new System.Windows.Forms.TextBox();
            this.addMedRec = new System.Windows.Forms.Button();
            this.edit = new System.Windows.Forms.Button();
            this.viewMedicalRecords = new System.Windows.Forms.Button();
            this.AddressLB = new System.Windows.Forms.Label();
            this.Address = new System.Windows.Forms.TextBox();
            this.address2LB = new System.Windows.Forms.Label();
            this.address2 = new System.Windows.Forms.TextBox();
            this.viewMedicalRecordsMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(924, -23);
            this.logout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(105, 23);
            this.logout.TabIndex = 95;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            // 
            // patientMedicalRecordLB
            // 
            this.patientMedicalRecordLB.AutoSize = true;
            this.patientMedicalRecordLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientMedicalRecordLB.Location = new System.Drawing.Point(34, 130);
            this.patientMedicalRecordLB.Name = "patientMedicalRecordLB";
            this.patientMedicalRecordLB.Size = new System.Drawing.Size(198, 38);
            this.patientMedicalRecordLB.TabIndex = 141;
            this.patientMedicalRecordLB.Text = "Patient View";
            this.patientMedicalRecordLB.Click += new System.EventHandler(this.patientMedicalRecordLB_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(509, 11);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 39);
            this.button1.TabIndex = 120;
            this.button1.Text = "Logout";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.logout_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(523, 670);
            this.back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(129, 23);
            this.back.TabIndex = 118;
            this.back.Text = "Home Screen";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.home_Click);
            // 
            // genderLB
            // 
            this.genderLB.AutoSize = true;
            this.genderLB.Location = new System.Drawing.Point(55, 513);
            this.genderLB.Name = "genderLB";
            this.genderLB.Size = new System.Drawing.Size(56, 17);
            this.genderLB.TabIndex = 132;
            this.genderLB.Text = "Gender";
            // 
            // stateLB
            // 
            this.stateLB.AutoSize = true;
            this.stateLB.Location = new System.Drawing.Point(369, 286);
            this.stateLB.Name = "stateLB";
            this.stateLB.Size = new System.Drawing.Size(41, 17);
            this.stateLB.TabIndex = 131;
            this.stateLB.Text = "State";
            // 
            // zipLB
            // 
            this.zipLB.AutoSize = true;
            this.zipLB.Location = new System.Drawing.Point(369, 513);
            this.zipLB.Name = "zipLB";
            this.zipLB.Size = new System.Drawing.Size(28, 17);
            this.zipLB.TabIndex = 125;
            this.zipLB.Text = "Zip";
            // 
            // cityLB
            // 
            this.cityLB.AutoSize = true;
            this.cityLB.Location = new System.Drawing.Point(369, 198);
            this.cityLB.Name = "cityLB";
            this.cityLB.Size = new System.Drawing.Size(31, 17);
            this.cityLB.TabIndex = 124;
            this.cityLB.Text = "City";
            this.cityLB.Click += new System.EventHandler(this.cityLB_Click);
            // 
            // organDonorLB
            // 
            this.organDonorLB.AutoSize = true;
            this.organDonorLB.Location = new System.Drawing.Point(369, 577);
            this.organDonorLB.Name = "organDonorLB";
            this.organDonorLB.Size = new System.Drawing.Size(91, 17);
            this.organDonorLB.TabIndex = 122;
            this.organDonorLB.Text = "Organ Donor";
            // 
            // birthDateLB
            // 
            this.birthDateLB.AutoSize = true;
            this.birthDateLB.Location = new System.Drawing.Point(55, 577);
            this.birthDateLB.Name = "birthDateLB";
            this.birthDateLB.Size = new System.Drawing.Size(71, 17);
            this.birthDateLB.TabIndex = 121;
            this.birthDateLB.Text = "Birth Date";
            // 
            // ssnLB
            // 
            this.ssnLB.AutoSize = true;
            this.ssnLB.Location = new System.Drawing.Point(55, 443);
            this.ssnLB.Name = "ssnLB";
            this.ssnLB.Size = new System.Drawing.Size(36, 17);
            this.ssnLB.TabIndex = 120;
            this.ssnLB.Text = "SSN";
            // 
            // mInitialLB
            // 
            this.mInitialLB.AutoSize = true;
            this.mInitialLB.Location = new System.Drawing.Point(55, 363);
            this.mInitialLB.Name = "mInitialLB";
            this.mInitialLB.Size = new System.Drawing.Size(59, 17);
            this.mInitialLB.TabIndex = 119;
            this.mInitialLB.Text = "M. Initial";
            // 
            // lastNameLB
            // 
            this.lastNameLB.AutoSize = true;
            this.lastNameLB.Location = new System.Drawing.Point(55, 198);
            this.lastNameLB.Name = "lastNameLB";
            this.lastNameLB.Size = new System.Drawing.Size(76, 17);
            this.lastNameLB.TabIndex = 118;
            this.lastNameLB.Text = "Last Name";
            // 
            // firstNameLB
            // 
            this.firstNameLB.AutoSize = true;
            this.firstNameLB.Location = new System.Drawing.Point(55, 286);
            this.firstNameLB.Name = "firstNameLB";
            this.firstNameLB.Size = new System.Drawing.Size(76, 17);
            this.firstNameLB.TabIndex = 117;
            this.firstNameLB.Text = "First Name";
            // 
            // HRAS_HomeLabel
            // 
            this.HRAS_HomeLabel.AutoSize = true;
            this.HRAS_HomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRAS_HomeLabel.Location = new System.Drawing.Point(188, 20);
            this.HRAS_HomeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HRAS_HomeLabel.Name = "HRAS_HomeLabel";
            this.HRAS_HomeLabel.Size = new System.Drawing.Size(261, 91);
            this.HRAS_HomeLabel.TabIndex = 113;
            this.HRAS_HomeLabel.Text = "HRAS";
            this.HRAS_HomeLabel.Click += new System.EventHandler(this.HRAS_HomeLabel_Click);
            // 
            // organDonor
            // 
            this.organDonor.Location = new System.Drawing.Point(369, 596);
            this.organDonor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.organDonor.Name = "organDonor";
            this.organDonor.Size = new System.Drawing.Size(69, 22);
            this.organDonor.TabIndex = 112;
            // 
            // zip
            // 
            this.zip.Location = new System.Drawing.Point(369, 530);
            this.zip.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.zip.Name = "zip";
            this.zip.Size = new System.Drawing.Size(119, 22);
            this.zip.TabIndex = 110;
            // 
            // city
            // 
            this.city.Location = new System.Drawing.Point(369, 217);
            this.city.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(119, 22);
            this.city.TabIndex = 108;
            // 
            // state
            // 
            this.state.Location = new System.Drawing.Point(369, 305);
            this.state.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(55, 22);
            this.state.TabIndex = 109;
            // 
            // birthDate
            // 
            this.birthDate.Location = new System.Drawing.Point(57, 596);
            this.birthDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.birthDate.Name = "birthDate";
            this.birthDate.Size = new System.Drawing.Size(124, 22);
            this.birthDate.TabIndex = 101;
            // 
            // ssn
            // 
            this.ssn.Location = new System.Drawing.Point(55, 462);
            this.ssn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ssn.Name = "ssn";
            this.ssn.Size = new System.Drawing.Size(157, 22);
            this.ssn.TabIndex = 100;
            // 
            // middleInitial
            // 
            this.middleInitial.Location = new System.Drawing.Point(55, 382);
            this.middleInitial.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.middleInitial.Name = "middleInitial";
            this.middleInitial.Size = new System.Drawing.Size(49, 22);
            this.middleInitial.TabIndex = 98;
            // 
            // gender
            // 
            this.gender.Location = new System.Drawing.Point(57, 530);
            this.gender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gender.Name = "gender";
            this.gender.Size = new System.Drawing.Size(57, 22);
            this.gender.TabIndex = 99;
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(55, 305);
            this.firstName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(163, 22);
            this.firstName.TabIndex = 97;
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(55, 217);
            this.lastName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(163, 22);
            this.lastName.TabIndex = 96;
            this.lastName.TextChanged += new System.EventHandler(this.lastName_TextChanged);
            // 
            // addMedRec
            // 
            this.addMedRec.Location = new System.Drawing.Point(162, 670);
            this.addMedRec.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.addMedRec.Name = "addMedRec";
            this.addMedRec.Size = new System.Drawing.Size(163, 23);
            this.addMedRec.TabIndex = 142;
            this.addMedRec.Text = "Add Medical Record";
            this.addMedRec.UseVisualStyleBackColor = true;
            this.addMedRec.Click += new System.EventHandler(this.addMedRec_Click);
            // 
            // edit
            // 
            this.edit.Location = new System.Drawing.Point(41, 670);
            this.edit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.edit.Name = "edit";
            this.edit.Size = new System.Drawing.Size(103, 23);
            this.edit.TabIndex = 143;
            this.edit.Text = "Edit";
            this.edit.UseVisualStyleBackColor = true;
            this.edit.Click += new System.EventHandler(this.edit_Click);
            // 
            // viewMedicalRecords
            // 
            this.viewMedicalRecords.Location = new System.Drawing.Point(342, 670);
            this.viewMedicalRecords.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.viewMedicalRecords.Name = "viewMedicalRecords";
            this.viewMedicalRecords.Size = new System.Drawing.Size(163, 23);
            this.viewMedicalRecords.TabIndex = 144;
            this.viewMedicalRecords.Text = "View Medical Records";
            this.viewMedicalRecords.UseVisualStyleBackColor = true;
            this.viewMedicalRecords.Click += new System.EventHandler(this.viewMedicalRecords_Click);
            // 
            // AddressLB
            // 
            this.AddressLB.AutoSize = true;
            this.AddressLB.Location = new System.Drawing.Point(366, 363);
            this.AddressLB.Name = "AddressLB";
            this.AddressLB.Size = new System.Drawing.Size(103, 17);
            this.AddressLB.TabIndex = 146;
            this.AddressLB.Text = "Address Line 1";
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(369, 382);
            this.Address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(119, 22);
            this.Address.TabIndex = 145;
            // 
            // address2LB
            // 
            this.address2LB.AutoSize = true;
            this.address2LB.Location = new System.Drawing.Point(369, 443);
            this.address2LB.Name = "address2LB";
            this.address2LB.Size = new System.Drawing.Size(103, 17);
            this.address2LB.TabIndex = 147;
            this.address2LB.Text = "Address Line 2";
            // 
            // address2
            // 
            this.address2.Location = new System.Drawing.Point(372, 462);
            this.address2.Name = "address2";
            this.address2.Size = new System.Drawing.Size(116, 22);
            this.address2.TabIndex = 148;
            // 
            // viewMedicalRecordsMessage
            // 
            this.viewMedicalRecordsMessage.AutoSize = true;
            this.viewMedicalRecordsMessage.Location = new System.Drawing.Point(330, 642);
            this.viewMedicalRecordsMessage.Name = "viewMedicalRecordsMessage";
            this.viewMedicalRecordsMessage.Size = new System.Drawing.Size(190, 17);
            this.viewMedicalRecordsMessage.TabIndex = 149;
            this.viewMedicalRecordsMessage.Text = "No medical records available";
            // 
            // HRAS_Patient_View_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 741);
            this.Controls.Add(this.viewMedicalRecordsMessage);
            this.Controls.Add(this.address2);
            this.Controls.Add(this.address2LB);
            this.Controls.Add(this.AddressLB);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.viewMedicalRecords);
            this.Controls.Add(this.edit);
            this.Controls.Add(this.addMedRec);
            this.Controls.Add(this.patientMedicalRecordLB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.back);
            this.Controls.Add(this.genderLB);
            this.Controls.Add(this.stateLB);
            this.Controls.Add(this.zipLB);
            this.Controls.Add(this.cityLB);
            this.Controls.Add(this.organDonorLB);
            this.Controls.Add(this.birthDateLB);
            this.Controls.Add(this.ssnLB);
            this.Controls.Add(this.mInitialLB);
            this.Controls.Add(this.lastNameLB);
            this.Controls.Add(this.firstNameLB);
            this.Controls.Add(this.HRAS_HomeLabel);
            this.Controls.Add(this.organDonor);
            this.Controls.Add(this.zip);
            this.Controls.Add(this.city);
            this.Controls.Add(this.state);
            this.Controls.Add(this.birthDate);
            this.Controls.Add(this.ssn);
            this.Controls.Add(this.middleInitial);
            this.Controls.Add(this.gender);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.logout);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "HRAS_Patient_View_View";
            this.Text = "Patient View";
            this.Load += new System.EventHandler(this.HRAS_Patient_Create_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Label patientMedicalRecordLB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label genderLB;
        private System.Windows.Forms.Label stateLB;
        private System.Windows.Forms.Label zipLB;
        private System.Windows.Forms.Label cityLB;
        private System.Windows.Forms.Label organDonorLB;
        private System.Windows.Forms.Label birthDateLB;
        private System.Windows.Forms.Label ssnLB;
        private System.Windows.Forms.Label mInitialLB;
        private System.Windows.Forms.Label lastNameLB;
        private System.Windows.Forms.Label firstNameLB;
        private System.Windows.Forms.Label HRAS_HomeLabel;
        private System.Windows.Forms.TextBox organDonor;
        private System.Windows.Forms.TextBox zip;
        private System.Windows.Forms.TextBox city;
        private System.Windows.Forms.TextBox state;
        private System.Windows.Forms.TextBox birthDate;
        private System.Windows.Forms.TextBox ssn;
        private System.Windows.Forms.TextBox middleInitial;
        private System.Windows.Forms.TextBox gender;
        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.Button addMedRec;
        private System.Windows.Forms.Button edit;
        private System.Windows.Forms.Button viewMedicalRecords;
        private System.Windows.Forms.Label AddressLB;
        private System.Windows.Forms.TextBox Address;
        private System.Windows.Forms.Label address2LB;
        private System.Windows.Forms.TextBox address2;
        private System.Windows.Forms.Label viewMedicalRecordsMessage;
    }
}