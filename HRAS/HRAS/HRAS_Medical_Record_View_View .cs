﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Medical_Record_View_View : Form
    {
        private MedicalRecord currentRecord;
        private Patient currentPatient;

        public HRAS_Medical_Record_View_View(MedicalRecord inRecord,Patient inPatient)
        {
            InitializeComponent();
          
            currentPatient = new Patient();
            currentPatient.deepCopy(inPatient);

            currentRecord = new MedicalRecord();
            currentRecord.deepCopy(inRecord);

            displayInfoToFields();
            setFieldsToReadOnly();
                    
            //if med rec is locked( its being edite by another user, disable edit button
            if(PatientHandler.isMedicalRecordLocked(currentPatient,currentRecord))
            {
                isLockedLB.Text = "Medical Record is being edited";
                edit.Enabled = false;
            }

        }

        private void displayInfoToFields()
        {

            //display patient info first
            firstName.Text = currentPatient.firstName;
            lastName.Text = currentPatient.lastName;
            ssn.Text = currentPatient.SSN;

            //display medical record
            insurer.Text = currentRecord.insurer;
            physician.Text = currentRecord.physician;
            diagnosis.Text = currentRecord.diagnosis;
            notes.Text = currentRecord.notes;
            roomNumber.Text = currentRecord.roomNumber;         
            if (currentRecord.doNotResuscitate == true) { dnr.Text = "Y"; }
            else { dnr.Text = "N"; };

            string month;
            string day;

            //if month or day is single digit ,concatinate '0' to it,
            //entrydate
            if (Convert.ToString(currentRecord.entryDate.Month).Length == 1) { month = "0" + currentRecord.entryDate.Month; }
            else { month = Convert.ToString(currentRecord.entryDate.Month); } 

            if (Convert.ToString(currentRecord.entryDate.Day).Length == 1) { day = "0" + currentRecord.entryDate.Day; }           
            else { day = Convert.ToString(currentRecord.entryDate.Day); }
            entryDate.Text = Convert.ToString(month + "/" + day + "/" + currentRecord.entryDate.Year);

            if (Convert.ToString(currentRecord.exitDate).Length > 1)
            {
                //exitdate
                if (Convert.ToString(currentRecord.exitDate.Value.Month).Length == 1) { month = "0" + currentRecord.exitDate.Value.Month; }
                else { month = Convert.ToString(currentRecord.exitDate.Value.Month); }

                if (Convert.ToString(currentRecord.exitDate.Value.Day).Length == 1) { day = "0" + currentRecord.exitDate.Value.Day; }
                else { day = Convert.ToString(currentRecord.exitDate.Value.Day); }
                exitDate.Text = Convert.ToString(month + "/" + day + "/" + currentRecord.exitDate.Value.Year);            
            }

            //display all symptoms           
            List<Symptom> s = new List<Symptom>();
            s = currentRecord.getSymptoms();

            foreach (Symptom symptom in s) 
            {
                if (symptoms.Text.Length == 0) { symptoms.Text = symptom.name; }
                else { symptoms.Text = symptoms.Text + ",\n" + symptom.name; }             
            }
        }
        private void setFieldsToReadOnly()
        {
            exitDate.ReadOnly = true;
            physician.ReadOnly = true;
            insurer.ReadOnly = true;
            notes.ReadOnly = true;
            roomNumber.ReadOnly = true;
            diagnosis.ReadOnly = true;
            dnr.ReadOnly = true;
            symptoms.ReadOnly = true;
            entryDate.ReadOnly = true;
        }
       
        private void audit_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Patient_Record_Audit_History_View
         
            Hide();
            HRAS_Medical_Record_Audit_History_View view = new HRAS_Medical_Record_Audit_History_View(currentPatient,currentRecord);
            view.Closed += (s, args) => Close();
            view.Show();            
        }

        private void edit_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Patient_Edit_View

            Hide();
            HRAS_Medical_Record_Edit_View view = new HRAS_Medical_Record_Edit_View(currentRecord,currentPatient);
            view.Closed += (s, args) => Close();
            view.Show();
        }

        private void checkOut_Click(object sender, EventArgs e)
        {

            //note: we can only check out if this is current med_Rec of particular Patient
            //navigates to HRAS_Patient_Bill_View

            //will have to send ssn and entry to edit page to know which doc it is
            Hide();
            HRAS_Patient_Bill_View view = new HRAS_Patient_Bill_View( currentPatient, currentRecord);
            view.Closed += (s, args) => Close();
            view.Show();
        }
        private void logout_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View view = new HRAS_Login_View();
            view.Closed += (s, args) => Close();
            view.Show();
        }
        private void back_Click(object sender, EventArgs e)
        {
            Hide();
            HRAS_Home_View view = new HRAS_Home_View();
            view.Closed += (s, args) => Close();
            view.Show();
        }

        private void lastName_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Patient_View_View_Load(object sender, EventArgs e)
        {
        }

        private void diagnosisLB_Click(object sender, EventArgs e)
        {
        }

        private void dnr_Click(object sender, EventArgs e)
        {

        }
        private void entryDate_Click(object sender, EventArgs e)
        {

        }

        private void MedicalRecordLB_Click(object sender, EventArgs e)
        {

        }
        private void lastName_Click(object sender, EventArgs e)
        {

        }
    }
}
