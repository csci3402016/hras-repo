﻿namespace HRAS
{
    partial class HRAS_Patient_Bill_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoutButton = new System.Windows.Forms.Button();
            this.ToHomePageLB = new System.Windows.Forms.Label();
            this.PatientBillLB = new System.Windows.Forms.Label();
            this.NameLB = new System.Windows.Forms.Label();
            this.AddressLB = new System.Windows.Forms.Label();
            this.CityLB = new System.Windows.Forms.Label();
            this.DatesOfStayLB = new System.Windows.Forms.Label();
            this.TotalHoursLB = new System.Windows.Forms.Label();
            this.HourlyRateLB = new System.Windows.Forms.Label();
            this.ItemsServicesUsedLB = new System.Windows.Forms.Label();
            this.TotalLB = new System.Windows.Forms.Label();
            this.NameTB = new System.Windows.Forms.Label();
            this.AddressTB = new System.Windows.Forms.Label();
            this.CityTB = new System.Windows.Forms.Label();
            this.DatesOfStayTB = new System.Windows.Forms.Label();
            this.TotalHoursTB = new System.Windows.Forms.Label();
            this.HourlyRateTB = new System.Windows.Forms.Label();
            this.BillTotalTB = new System.Windows.Forms.Label();
            this.StateLB = new System.Windows.Forms.Label();
            this.ZipLB = new System.Windows.Forms.Label();
            this.StateTB = new System.Windows.Forms.Label();
            this.ZipCodeTB = new System.Windows.Forms.Label();
            this.InventoryUsageInfo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.InventoryUsageInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoutButton
            // 
            this.LogoutButton.Location = new System.Drawing.Point(882, 12);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(64, 29);
            this.LogoutButton.TabIndex = 0;
            this.LogoutButton.Text = "Logout";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // ToHomePageLB
            // 
            this.ToHomePageLB.AutoSize = true;
            this.ToHomePageLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToHomePageLB.Location = new System.Drawing.Point(373, 25);
            this.ToHomePageLB.Name = "ToHomePageLB";
            this.ToHomePageLB.Size = new System.Drawing.Size(210, 73);
            this.ToHomePageLB.TabIndex = 1;
            this.ToHomePageLB.Text = "HRAS";
            this.ToHomePageLB.Click += new System.EventHandler(this.ToHomePageLB_Click);
            // 
            // PatientBillLB
            // 
            this.PatientBillLB.AutoSize = true;
            this.PatientBillLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatientBillLB.Location = new System.Drawing.Point(97, 131);
            this.PatientBillLB.Name = "PatientBillLB";
            this.PatientBillLB.Size = new System.Drawing.Size(153, 33);
            this.PatientBillLB.TabIndex = 2;
            this.PatientBillLB.Text = "Patient Bill";
            // 
            // NameLB
            // 
            this.NameLB.AutoSize = true;
            this.NameLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLB.Location = new System.Drawing.Point(102, 190);
            this.NameLB.Name = "NameLB";
            this.NameLB.Size = new System.Drawing.Size(45, 16);
            this.NameLB.TabIndex = 3;
            this.NameLB.Text = "Name";
            // 
            // AddressLB
            // 
            this.AddressLB.AutoSize = true;
            this.AddressLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddressLB.Location = new System.Drawing.Point(100, 218);
            this.AddressLB.Name = "AddressLB";
            this.AddressLB.Size = new System.Drawing.Size(59, 16);
            this.AddressLB.TabIndex = 4;
            this.AddressLB.Text = "Address";
            // 
            // CityLB
            // 
            this.CityLB.AutoSize = true;
            this.CityLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CityLB.Location = new System.Drawing.Point(100, 246);
            this.CityLB.Name = "CityLB";
            this.CityLB.Size = new System.Drawing.Size(30, 16);
            this.CityLB.TabIndex = 5;
            this.CityLB.Text = "City";
            // 
            // DatesOfStayLB
            // 
            this.DatesOfStayLB.AutoSize = true;
            this.DatesOfStayLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatesOfStayLB.Location = new System.Drawing.Point(102, 289);
            this.DatesOfStayLB.Name = "DatesOfStayLB";
            this.DatesOfStayLB.Size = new System.Drawing.Size(88, 16);
            this.DatesOfStayLB.TabIndex = 6;
            this.DatesOfStayLB.Text = "Dates of Stay";
            // 
            // TotalHoursLB
            // 
            this.TotalHoursLB.AutoSize = true;
            this.TotalHoursLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalHoursLB.Location = new System.Drawing.Point(100, 315);
            this.TotalHoursLB.Name = "TotalHoursLB";
            this.TotalHoursLB.Size = new System.Drawing.Size(78, 16);
            this.TotalHoursLB.TabIndex = 7;
            this.TotalHoursLB.Text = "Total Hours";
            // 
            // HourlyRateLB
            // 
            this.HourlyRateLB.AutoSize = true;
            this.HourlyRateLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HourlyRateLB.Location = new System.Drawing.Point(100, 338);
            this.HourlyRateLB.Name = "HourlyRateLB";
            this.HourlyRateLB.Size = new System.Drawing.Size(89, 16);
            this.HourlyRateLB.TabIndex = 8;
            this.HourlyRateLB.Text = "Hourly Rate $";
            // 
            // ItemsServicesUsedLB
            // 
            this.ItemsServicesUsedLB.AutoSize = true;
            this.ItemsServicesUsedLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemsServicesUsedLB.Location = new System.Drawing.Point(100, 376);
            this.ItemsServicesUsedLB.Name = "ItemsServicesUsedLB";
            this.ItemsServicesUsedLB.Size = new System.Drawing.Size(234, 29);
            this.ItemsServicesUsedLB.TabIndex = 9;
            this.ItemsServicesUsedLB.Text = "Items/Services Used";
            // 
            // TotalLB
            // 
            this.TotalLB.AutoSize = true;
            this.TotalLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TotalLB.Location = new System.Drawing.Point(100, 538);
            this.TotalLB.Name = "TotalLB";
            this.TotalLB.Size = new System.Drawing.Size(87, 29);
            this.TotalLB.TabIndex = 10;
            this.TotalLB.Text = "Total $";
            // 
            // NameTB
            // 
            this.NameTB.Location = new System.Drawing.Point(234, 186);
            this.NameTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.NameTB.Name = "NameTB";
            this.NameTB.Size = new System.Drawing.Size(380, 18);
            this.NameTB.TabIndex = 11;
            this.NameTB.TextChanged += new System.EventHandler(this.NameTB_TextChanged);
            // 
            // AddressTB
            // 
            this.AddressTB.Location = new System.Drawing.Point(234, 214);
            this.AddressTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.AddressTB.Name = "AddressTB";
            this.AddressTB.Size = new System.Drawing.Size(380, 18);
            this.AddressTB.TabIndex = 12;
            this.AddressTB.Click += new System.EventHandler(this.AddressTB_Click);
            // 
            // CityTB
            // 
            this.CityTB.Location = new System.Drawing.Point(234, 242);
            this.CityTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.CityTB.Name = "CityTB";
            this.CityTB.Size = new System.Drawing.Size(142, 18);
            this.CityTB.TabIndex = 13;
            // 
            // DatesOfStayTB
            // 
            this.DatesOfStayTB.Location = new System.Drawing.Point(291, 285);
            this.DatesOfStayTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.DatesOfStayTB.Name = "DatesOfStayTB";
            this.DatesOfStayTB.Size = new System.Drawing.Size(188, 18);
            this.DatesOfStayTB.TabIndex = 14;
            // 
            // TotalHoursTB
            // 
            this.TotalHoursTB.Location = new System.Drawing.Point(364, 311);
            this.TotalHoursTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.TotalHoursTB.Name = "TotalHoursTB";
            this.TotalHoursTB.Size = new System.Drawing.Size(115, 18);
            this.TotalHoursTB.TabIndex = 15;
            // 
            // HourlyRateTB
            // 
            this.HourlyRateTB.Location = new System.Drawing.Point(364, 337);
            this.HourlyRateTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.HourlyRateTB.Name = "HourlyRateTB";
            this.HourlyRateTB.Size = new System.Drawing.Size(115, 18);
            this.HourlyRateTB.TabIndex = 16;
            // 
            // BillTotalTB
            // 
            this.BillTotalTB.Location = new System.Drawing.Point(216, 538);
            this.BillTotalTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.BillTotalTB.Name = "BillTotalTB";
            this.BillTotalTB.Size = new System.Drawing.Size(117, 18);
            this.BillTotalTB.TabIndex = 17;
            // 
            // StateLB
            // 
            this.StateLB.AutoSize = true;
            this.StateLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StateLB.Location = new System.Drawing.Point(383, 243);
            this.StateLB.Name = "StateLB";
            this.StateLB.Size = new System.Drawing.Size(39, 16);
            this.StateLB.TabIndex = 18;
            this.StateLB.Text = "State";
            // 
            // ZipLB
            // 
            this.ZipLB.AutoSize = true;
            this.ZipLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZipLB.Location = new System.Drawing.Point(480, 243);
            this.ZipLB.Name = "ZipLB";
            this.ZipLB.Size = new System.Drawing.Size(63, 16);
            this.ZipLB.TabIndex = 19;
            this.ZipLB.Text = "Zip Code";
            // 
            // StateTB
            // 
            this.StateTB.Location = new System.Drawing.Point(431, 243);
            this.StateTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.StateTB.Name = "StateTB";
            this.StateTB.Size = new System.Drawing.Size(42, 18);
            this.StateTB.TabIndex = 20;
            // 
            // ZipCodeTB
            // 
            this.ZipCodeTB.Location = new System.Drawing.Point(549, 242);
            this.ZipCodeTB.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.ZipCodeTB.Name = "ZipCodeTB";
            this.ZipCodeTB.Size = new System.Drawing.Size(65, 18);
            this.ZipCodeTB.TabIndex = 21;
            this.ZipCodeTB.Click += new System.EventHandler(this.zipCode_Click);
            // 
            // dataGridView1
            // 
            this.InventoryUsageInfo.AllowUserToAddRows = false;
            this.InventoryUsageInfo.AllowUserToDeleteRows = false;
            this.InventoryUsageInfo.AllowUserToResizeColumns = false;
            this.InventoryUsageInfo.AllowUserToResizeRows = false;
            this.InventoryUsageInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.InventoryUsageInfo.Location = new System.Drawing.Point(107, 408);
            this.InventoryUsageInfo.Name = "dataGridView1";
            this.InventoryUsageInfo.ReadOnly = true;
            this.InventoryUsageInfo.Size = new System.Drawing.Size(543, 113);
            this.InventoryUsageInfo.TabIndex = 22;
            // 
            // HRAS_Patient_Bill_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 593);
            this.Controls.Add(this.InventoryUsageInfo);
            this.Controls.Add(this.ZipCodeTB);
            this.Controls.Add(this.StateTB);
            this.Controls.Add(this.ZipLB);
            this.Controls.Add(this.StateLB);
            this.Controls.Add(this.BillTotalTB);
            this.Controls.Add(this.HourlyRateTB);
            this.Controls.Add(this.TotalHoursTB);
            this.Controls.Add(this.DatesOfStayTB);
            this.Controls.Add(this.CityTB);
            this.Controls.Add(this.AddressTB);
            this.Controls.Add(this.NameTB);
            this.Controls.Add(this.TotalLB);
            this.Controls.Add(this.ItemsServicesUsedLB);
            this.Controls.Add(this.HourlyRateLB);
            this.Controls.Add(this.TotalHoursLB);
            this.Controls.Add(this.DatesOfStayLB);
            this.Controls.Add(this.CityLB);
            this.Controls.Add(this.AddressLB);
            this.Controls.Add(this.NameLB);
            this.Controls.Add(this.PatientBillLB);
            this.Controls.Add(this.ToHomePageLB);
            this.Controls.Add(this.LogoutButton);
            this.Name = "HRAS_Patient_Bill_View";
            this.Text = "Patient Bill";
            this.Load += new System.EventHandler(this.HRAS_Patient_Bill_View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.InventoryUsageInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.Label ToHomePageLB;
        private System.Windows.Forms.Label PatientBillLB;
        private System.Windows.Forms.Label NameLB;
        private System.Windows.Forms.Label AddressLB;
        private System.Windows.Forms.Label CityLB;
        private System.Windows.Forms.Label DatesOfStayLB;
        private System.Windows.Forms.Label TotalHoursLB;
        private System.Windows.Forms.Label HourlyRateLB;
        private System.Windows.Forms.Label ItemsServicesUsedLB;
        private System.Windows.Forms.Label TotalLB;
        private System.Windows.Forms.Label NameTB;
        private System.Windows.Forms.Label AddressTB;
        private System.Windows.Forms.Label CityTB;
        private System.Windows.Forms.Label DatesOfStayTB;
        private System.Windows.Forms.Label TotalHoursTB;
        private System.Windows.Forms.Label HourlyRateTB;
        private System.Windows.Forms.Label BillTotalTB;
        private System.Windows.Forms.Label StateLB;
        private System.Windows.Forms.Label ZipLB;
        private System.Windows.Forms.Label StateTB;
        private System.Windows.Forms.Label ZipCodeTB;
        private System.Windows.Forms.DataGridView InventoryUsageInfo;
    }
}