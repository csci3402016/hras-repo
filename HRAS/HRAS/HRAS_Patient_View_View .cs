﻿using System;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Patient_View_View : Form
    {
        private readonly Patient currentPatient;

        public HRAS_Patient_View_View(Patient inPatient)
        {
            InitializeComponent();

            currentPatient = new Patient();
            currentPatient.deepCopy(inPatient);

            displayInfoToFields();
            setFieldsToReadOnly();
            

            viewMedicalRecordsMessage.Hide();
            //check if patient has medical records(for disabling button "view medrecs")
            if(!PatientHandler.hasRecord(currentPatient))
            {
                viewMedicalRecords.Enabled = false;
                viewMedicalRecordsMessage.Show();
            }
        }
        private void displayInfoToFields()
        {
            //display values of patient to view in the textboxes
            firstName.Text = currentPatient.firstName;
            lastName.Text = currentPatient.lastName;
            ssn.Text = currentPatient.SSN;
            state.Text = currentPatient.state;
            city.Text = currentPatient.city;
            zip.Text = currentPatient.zipCode;
            middleInitial.Text = Convert.ToString(currentPatient.middleInitial);
            Address.Text = currentPatient.address;
            address2.Text = currentPatient.address2;
            gender.Text = Convert.ToString(currentPatient.gender);
            organDonor.Text = Convert.ToString(currentPatient.organDonor);

            //if birthdate day or month is a single digit concatinate '0' to it
            //otherwise it cant be parsed in edit window
            string month;
            string day;
            if (Convert.ToString(currentPatient.birthDate.Month).Length == 1) { month = "0" + currentPatient.birthDate.Month; }
            else { month = Convert.ToString(currentPatient.birthDate.Month); }
           
            if (Convert.ToString(currentPatient.birthDate.Day).Length == 1) { day = "0" + currentPatient.birthDate.Day; }
            else { day = Convert.ToString(currentPatient.birthDate.Day); }
          
            birthDate.Text = Convert.ToString(month + "/" + day + "/" + currentPatient.birthDate.Year);
                    
           
        }
        private void setFieldsToReadOnly()
        {
            firstName.ReadOnly = true;
            lastName.ReadOnly = true;
            ssn.ReadOnly = true;
            birthDate.ReadOnly = true;
            state.ReadOnly = true;
            city.ReadOnly = true;
            zip.ReadOnly = true;
            middleInitial.ReadOnly = true;
            Address.ReadOnly = true;
            address2.ReadOnly = true;
            gender.ReadOnly = true;
            organDonor.ReadOnly = true;
        }

        private void home_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void HRAS_HomeLabel_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Home_View

            Hide();
            HRAS_Home_View homeView = new HRAS_Home_View();
            homeView.Closed += (s, args) => Close();
            homeView.Show();
        }

        private void checkOut_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Patient_Bill_View

            Hide();
            HRAS_Patient_Bill_View bill = new HRAS_Patient_Bill_View(currentPatient);
            bill.Closed += (s, args) => Close();
            bill.Show();
        }

        private void logout_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }
        private void edit_Click(object sender, EventArgs e)
        {
            Hide();
            HRAS_Patient_Edit_View home = new HRAS_Patient_Edit_View(currentPatient);
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void viewMedicalRecords_Click(object sender, EventArgs e)
        {
            Hide();
            HRAS_Medical_Record_Search_Results_View home =
                new HRAS_Medical_Record_Search_Results_View(currentPatient.medicalRecords, currentPatient);
            home.Closed += (s, args) => Close();
            home.Show();
        }
        private void addMedRec_Click(object sender, EventArgs e)
        {
            Hide();
            HRAS_Medical_Record_Create_View view = new HRAS_Medical_Record_Create_View(currentPatient);
            view.Closed += (s, args) => Close();
            view.Show();
        }

        private void lastName_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Patient_Create_View_Load(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }
     
        private void patientMedicalRecordLB_Click(object sender, EventArgs e)
        {
        }

        private void cityLB_Click(object sender, EventArgs e)
        {
        }
    }
}