﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;
using System.Globalization;

namespace HRAS
{
    public partial class HRAS_Patient_Create_View : Form
    {
       
        private Patient currentPatient; 

        public HRAS_Patient_Create_View()
        {
            
            InitializeComponent();
            view.Enabled = false;
            addMedRec.Enabled = false;
           
            currentPatient = new Patient();
            
        }

        private void save_Click(object sender, EventArgs e)
        {
            PatientHandler handler = new PatientHandler();


            if (isValidInput())
            { 
                //take values from textboxes and store them into currentPatient
                currentPatient.firstName = firstName.Text;
                currentPatient.lastName = lastName.Text;
                currentPatient.middleInitial = Char.Parse(middleInitial.Text);
                currentPatient.SSN = ssn.Text;
                currentPatient.birthDate = DateTime.Parse(birthDate.Text);
                currentPatient.address = Address.Text;
                currentPatient.address2 = Address2.Text;               
                currentPatient.city = city.Text;
                currentPatient.state = state.Text;
                currentPatient.zipCode = zip.Text;
                currentPatient.organDonor = Convert.ToChar(organDonor.Text);
                currentPatient.gender = Convert.ToChar(gender.Text);

                try
                { 
                    handler.createPatient(currentPatient);
                    view.Enabled = true;
                    addMedRec.Enabled = true;
                    viewLB.Hide();
                    addMedRecLB.Hide();
                 
                    message.Text = "Patient Created"; 
                    message.ForeColor = System.Drawing.Color.ForestGreen;
                    save.Hide();

                    setFieldsToReadOnly();
                    
                }
                catch(Exception ex)
                {                   
                    message.Text = ex.Message;
                    message.ForeColor = System.Drawing.Color.Red;
                }

            }

        }
        private bool isValidInput()
        {
            bool isValid = true;
            //checking input validity textbox by texbox

            //firstname
            if (firstName.Text.Length == 0)
            {
                firstNameError.Text = "Required Field";
                isValid = false;
            }
            else if (firstName.Text.Length > 50)
            {
                firstNameError.Text = "Must be at most 50 chars";
                isValid = false;
            }
            else { firstNameError.Text = ""; }

            //last name
            if (lastName.Text.Length == 0)
            {
                lastNameError.Text = "Required Field";
                isValid = false;
            }
            else if (lastName.Text.Length > 25)
            {
                lastNameError.Text = "Must be at most 25 chars";
                isValid = false;
            }
            else { lastNameError.Text = ""; }

            //ssn
            int b = 0;
            int.TryParse(ssn.Text, out b);

            if (ssn.Text.Length == 0)
            {
                ssnError.Text = "Required Field";
                isValid = false;
            }
            else if (b == 0)
            {
                ssnError.Text = "Must be a digit";
                isValid = false;
            }
            else if (ssn.Text.Length != 9)
            {
                ssnError.Text = "Must be 9 digits long";
                isValid = false;
            }
            else { ssnError.Text = ""; }

            //middle initial
            if (middleInitial.Text.Length == 0)
            {
                middleInitialError.Text = "Required Field";
                isValid = false;
            }
            else if (middleInitial.Text.Length != 1)
            {
                middleInitialError.Text = "Must be exactly one char long";
                isValid = false;
            }
            else { middleInitialError.Text = ""; }

            //gender
            if (gender.Text.Length == 0)
            {
                genderError.Text = "Required Field";
                isValid = false;
            }
            else if (!gender.Text.Equals("M") && !gender.Text.Equals("m") && !gender.Text.Equals("F") && !gender.Text.Equals("f"))
            {
                genderError.Text = "Must be M or F";
                isValid = false;
            }
            else { genderError.Text = ""; }

            //organ donor
            if (organDonor.Text.Length == 0)
            {
                organDonorError.Text = "Required Field";
                isValid = false;
            }
            else if (!organDonor.Text.Equals("Y") && !organDonor.Text.Equals("y") && !organDonor.Text.Equals("N") && !organDonor.Text.Equals("n"))
            {
                organDonorError.Text = "Must be Y or N";
                isValid = false;
            }
            else { organDonorError.Text = ""; }

            //birthdate
            DateTime Test;
            if (birthDate.Text.Length == 0)
            {
                birthDateError.Text = "Required Field";
                isValid = false;
            }
            else if (!DateTime.TryParseExact(birthDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Test))
            {
                birthDateError.Text = "Must be in given format";
                isValid = false;
            }
            else if (Test.Year > 9999 || Test.Year < 1753)
            {
                birthDateError.Text = "Must be between 1753 and 9999";
                isValid = false;
            }
            else { birthDateError.Text = ""; }

            //address 1
            if (Address.Text.Length == 0)
            {
                addressError.Text = "Required Field";
                isValid = false;
            }
            else if (Address.Text.Length > 35)
            {
                addressError.Text = "Must be at most 35 chars";
                isValid = false;
            }
            else { addressError.Text = ""; }

            //address 2
            if (Address2.Text.Length > 35)
            {
                address2Error.Text = "Must be at most 35 chars";
                isValid = false;
            }
            else { address2Error.Text = ""; }

            //city
            if (city.Text.Length == 0)
            {
                cityError.Text = "Required Field";
                isValid = false;
            }
            else if (city.Text.Length > 25)
            {
                cityError.Text = "Must be at most 25 chars";
                isValid = false;
            }
            else { cityError.Text = ""; }

            //state
            if (state.Text.Length == 0)
            {
                stateError.Text = "Required Field";
                isValid = false;
            }
            else if (state.Text.Length != 2)
            {
                stateError.Text = "Must be exactly 2 chars long";
                isValid = false;
            }
            else { stateError.Text = ""; }

            //zip
            int s = 0;
            int.TryParse(zip.Text, out s);

            if (zip.Text.Length == 0)
            {
                zipError.Text = "Required Field";
                isValid = false;
            }
            else if (s == 0)
            {
                zipError.Text = "Must be a digit";
                isValid = false;
            }
            else if (zip.Text.Length != 5)
            {
                zipError.Text = "Must be exactly 5 chars long";
                isValid = false;
            }
            else { zipError.Text = ""; }

            if (!isValid)
            {
                message.Text = "";
                view.Enabled = false;
            }
            return isValid;
        }
        private void setFieldsToReadOnly()
        {
            firstName.ReadOnly = true;
            lastName.ReadOnly = true;
            ssn.ReadOnly = true;
            birthDate.ReadOnly = true;
            state.ReadOnly = true;
            city.ReadOnly = true;
            zip.ReadOnly = true;
            middleInitial.ReadOnly = true;
            Address.ReadOnly = true;
            Address2.ReadOnly = true;
            gender.ReadOnly = true;
            organDonor.ReadOnly = true;
        }

        private void home_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void HRAS_HomeLabel_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Home_View

            Hide();
            HRAS_Home_View homeView = new HRAS_Home_View();
            homeView.Closed += (s, args) => Close();
            homeView.Show();
        }

        private void checkOut_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Patient_Bill_View

            Hide();
            HRAS_Patient_Bill_View bill = new HRAS_Patient_Bill_View(currentPatient);
            bill.Closed += (s, args) => Close();
            bill.Show();
        }

        private void logout_Click(object sender, EventArgs e)
        {
            //naviates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

       
        private void addMedRec_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Medical_Record_Create_View loginWindow = new HRAS_Medical_Record_Create_View(currentPatient);
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void view_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Patient_View_View

            Hide();
            HRAS_Patient_View_View loginWindow = new HRAS_Patient_View_View(currentPatient);
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }
             
        private void lastName_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Patient_Create_View_Load(object sender, EventArgs e)
        {
        }

        private void adrdess_TextChanged(object sender, EventArgs e)
        {

        }

        private void descriptionLB_Click(object sender, EventArgs e)
        {

        }

        private void message_Click(object sender, EventArgs e)
        {

        }

        private void genderLB_Click(object sender, EventArgs e)
        {

        }

        private void zipLB_Click(object sender, EventArgs e)
        {

        }
    }
}
