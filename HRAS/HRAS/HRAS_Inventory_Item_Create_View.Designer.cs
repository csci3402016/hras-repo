﻿namespace HRAS
{
    partial class HRAS_Inventory_Item_Create_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.StockIDLB = new System.Windows.Forms.Label();
            this.QuantityLB = new System.Windows.Forms.Label();
            this.SizeLB = new System.Windows.Forms.Label();
            this.CostLB = new System.Windows.Forms.Label();
            this.DescriptionLB = new System.Windows.Forms.Label();
            this.LogoutButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.StockIDtextBox = new System.Windows.Forms.TextBox();
            this.CostTextBox = new System.Windows.Forms.TextBox();
            this.SizeTextBox = new System.Windows.Forms.TextBox();
            this.QuantitytextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.HRASLB = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.costError = new System.Windows.Forms.Label();
            this.quantityError = new System.Windows.Forms.Label();
            this.sizeError = new System.Windows.Forms.Label();
            this.descriptionError = new System.Windows.Forms.Label();
            this.stockIDError = new System.Windows.Forms.Label();
            this.view = new System.Windows.Forms.Button();
            this.viewLB = new System.Windows.Forms.Label();
            this.virtualItem = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.virtualError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 98);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(344, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "New Inventory Item Record";
            // 
            // StockIDLB
            // 
            this.StockIDLB.AutoSize = true;
            this.StockIDLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StockIDLB.Location = new System.Drawing.Point(109, 169);
            this.StockIDLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StockIDLB.Name = "StockIDLB";
            this.StockIDLB.Size = new System.Drawing.Size(86, 25);
            this.StockIDLB.TabIndex = 2;
            this.StockIDLB.Text = "Stock ID";
            // 
            // QuantityLB
            // 
            this.QuantityLB.AutoSize = true;
            this.QuantityLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuantityLB.Location = new System.Drawing.Point(110, 384);
            this.QuantityLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.QuantityLB.Name = "QuantityLB";
            this.QuantityLB.Size = new System.Drawing.Size(85, 25);
            this.QuantityLB.TabIndex = 3;
            this.QuantityLB.Text = "Quantity";
            // 
            // SizeLB
            // 
            this.SizeLB.AutoSize = true;
            this.SizeLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SizeLB.Location = new System.Drawing.Point(132, 258);
            this.SizeLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SizeLB.Name = "SizeLB";
            this.SizeLB.Size = new System.Drawing.Size(51, 25);
            this.SizeLB.TabIndex = 4;
            this.SizeLB.Text = "Size";
            // 
            // CostLB
            // 
            this.CostLB.AutoSize = true;
            this.CostLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CostLB.Location = new System.Drawing.Point(44, 299);
            this.CostLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.CostLB.Name = "CostLB";
            this.CostLB.Size = new System.Drawing.Size(151, 25);
            this.CostLB.TabIndex = 5;
            this.CostLB.Text = "Cost (to patient)";
            // 
            // DescriptionLB
            // 
            this.DescriptionLB.AutoSize = true;
            this.DescriptionLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionLB.Location = new System.Drawing.Point(87, 209);
            this.DescriptionLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DescriptionLB.Name = "DescriptionLB";
            this.DescriptionLB.Size = new System.Drawing.Size(109, 25);
            this.DescriptionLB.TabIndex = 6;
            this.DescriptionLB.Text = "Description";
            // 
            // LogoutButton
            // 
            this.LogoutButton.Location = new System.Drawing.Point(535, 14);
            this.LogoutButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(112, 44);
            this.LogoutButton.TabIndex = 7;
            this.LogoutButton.Text = "Log Out";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(661, 470);
            this.CancelButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(112, 44);
            this.CancelButton.TabIndex = 8;
            this.CancelButton.Text = "Home Screen";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.homeScreen_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(179, 470);
            this.SaveButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(112, 44);
            this.SaveButton.TabIndex = 9;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // StockIDtextBox
            // 
            this.StockIDtextBox.Location = new System.Drawing.Point(220, 172);
            this.StockIDtextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.StockIDtextBox.Name = "StockIDtextBox";
            this.StockIDtextBox.Size = new System.Drawing.Size(312, 22);
            this.StockIDtextBox.TabIndex = 10;
            this.StockIDtextBox.TextChanged += new System.EventHandler(this.StockIDtextBox_TextChanged);
            // 
            // CostTextBox
            // 
            this.CostTextBox.Location = new System.Drawing.Point(220, 303);
            this.CostTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CostTextBox.Name = "CostTextBox";
            this.CostTextBox.Size = new System.Drawing.Size(312, 22);
            this.CostTextBox.TabIndex = 12;
            this.CostTextBox.TextChanged += new System.EventHandler(this.CostTextBox_TextChanged);
            // 
            // SizeTextBox
            // 
            this.SizeTextBox.Location = new System.Drawing.Point(220, 258);
            this.SizeTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SizeTextBox.Name = "SizeTextBox";
            this.SizeTextBox.Size = new System.Drawing.Size(312, 22);
            this.SizeTextBox.TabIndex = 13;
            this.SizeTextBox.TextChanged += new System.EventHandler(this.SizeTextBox_TextChanged);
            // 
            // QuantitytextBox
            // 
            this.QuantitytextBox.Location = new System.Drawing.Point(220, 387);
            this.QuantitytextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.QuantitytextBox.Name = "QuantitytextBox";
            this.QuantitytextBox.Size = new System.Drawing.Size(312, 22);
            this.QuantitytextBox.TabIndex = 14;
            this.QuantitytextBox.TextChanged += new System.EventHandler(this.QuantitytextBox_TextChanged);
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Location = new System.Drawing.Point(220, 213);
            this.descriptionTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(312, 22);
            this.descriptionTextBox.TabIndex = 16;
            // 
            // HRASLB
            // 
            this.HRASLB.AutoSize = true;
            this.HRASLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRASLB.Location = new System.Drawing.Point(239, 9);
            this.HRASLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HRASLB.Name = "HRASLB";
            this.HRASLB.Size = new System.Drawing.Size(261, 91);
            this.HRASLB.TabIndex = 132;
            this.HRASLB.Text = "HRAS";
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.8F);
            this.message.Location = new System.Drawing.Point(317, 129);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 25);
            this.message.TabIndex = 133;
            // 
            // costError
            // 
            this.costError.AutoSize = true;
            this.costError.ForeColor = System.Drawing.Color.Red;
            this.costError.Location = new System.Drawing.Point(567, 308);
            this.costError.Name = "costError";
            this.costError.Size = new System.Drawing.Size(0, 17);
            this.costError.TabIndex = 134;
            // 
            // quantityError
            // 
            this.quantityError.AutoSize = true;
            this.quantityError.ForeColor = System.Drawing.Color.Red;
            this.quantityError.Location = new System.Drawing.Point(567, 390);
            this.quantityError.Name = "quantityError";
            this.quantityError.Size = new System.Drawing.Size(0, 17);
            this.quantityError.TabIndex = 135;
            // 
            // sizeError
            // 
            this.sizeError.AutoSize = true;
            this.sizeError.ForeColor = System.Drawing.Color.Red;
            this.sizeError.Location = new System.Drawing.Point(567, 258);
            this.sizeError.Name = "sizeError";
            this.sizeError.Size = new System.Drawing.Size(0, 17);
            this.sizeError.TabIndex = 136;
            // 
            // descriptionError
            // 
            this.descriptionError.AutoSize = true;
            this.descriptionError.ForeColor = System.Drawing.Color.Red;
            this.descriptionError.Location = new System.Drawing.Point(567, 209);
            this.descriptionError.Name = "descriptionError";
            this.descriptionError.Size = new System.Drawing.Size(0, 17);
            this.descriptionError.TabIndex = 137;
            // 
            // stockIDError
            // 
            this.stockIDError.AutoSize = true;
            this.stockIDError.ForeColor = System.Drawing.Color.Red;
            this.stockIDError.Location = new System.Drawing.Point(567, 172);
            this.stockIDError.Name = "stockIDError";
            this.stockIDError.Size = new System.Drawing.Size(0, 17);
            this.stockIDError.TabIndex = 138;
            // 
            // view
            // 
            this.view.Location = new System.Drawing.Point(347, 470);
            this.view.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(112, 44);
            this.view.TabIndex = 139;
            this.view.Text = "View";
            this.view.UseVisualStyleBackColor = true;
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // viewLB
            // 
            this.viewLB.AutoSize = true;
            this.viewLB.Location = new System.Drawing.Point(333, 449);
            this.viewLB.Name = "viewLB";
            this.viewLB.Size = new System.Drawing.Size(135, 17);
            this.viewLB.TabIndex = 140;
            this.viewLB.Text = "Must Save Item First";
            // 
            // virtualItem
            // 
            this.virtualItem.Location = new System.Drawing.Point(222, 345);
            this.virtualItem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.virtualItem.Name = "virtualItem";
            this.virtualItem.Size = new System.Drawing.Size(69, 22);
            this.virtualItem.TabIndex = 141;
            this.virtualItem.TextChanged += new System.EventHandler(this.virtual_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 345);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 25);
            this.label1.TabIndex = 142;
            this.label1.Text = "Virtual Item (Y/N)";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // virtualError
            // 
            this.virtualError.AutoSize = true;
            this.virtualError.ForeColor = System.Drawing.Color.Red;
            this.virtualError.Location = new System.Drawing.Point(312, 345);
            this.virtualError.Name = "virtualError";
            this.virtualError.Size = new System.Drawing.Size(0, 17);
            this.virtualError.TabIndex = 143;
            // 
            // HRAS_Inventory_Item_Create_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 545);
            this.Controls.Add(this.virtualError);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.virtualItem);
            this.Controls.Add(this.viewLB);
            this.Controls.Add(this.view);
            this.Controls.Add(this.stockIDError);
            this.Controls.Add(this.descriptionError);
            this.Controls.Add(this.sizeError);
            this.Controls.Add(this.quantityError);
            this.Controls.Add(this.costError);
            this.Controls.Add(this.message);
            this.Controls.Add(this.HRASLB);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(this.QuantitytextBox);
            this.Controls.Add(this.SizeTextBox);
            this.Controls.Add(this.CostTextBox);
            this.Controls.Add(this.StockIDtextBox);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.LogoutButton);
            this.Controls.Add(this.DescriptionLB);
            this.Controls.Add(this.CostLB);
            this.Controls.Add(this.SizeLB);
            this.Controls.Add(this.QuantityLB);
            this.Controls.Add(this.StockIDLB);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "HRAS_Inventory_Item_Create_View";
            this.Text = "Item Create";
            this.Load += new System.EventHandler(this.HRAS_Inventory_Item_Create_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label StockIDLB;
        private System.Windows.Forms.Label QuantityLB;
        private System.Windows.Forms.Label SizeLB;
        private System.Windows.Forms.Label CostLB;
        private System.Windows.Forms.Label DescriptionLB;
        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.TextBox StockIDtextBox;
        private System.Windows.Forms.TextBox CostTextBox;
        private System.Windows.Forms.TextBox SizeTextBox;
        private System.Windows.Forms.TextBox QuantitytextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label HRASLB;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label costError;
        private System.Windows.Forms.Label quantityError;
        private System.Windows.Forms.Label sizeError;
        private System.Windows.Forms.Label descriptionError;
        private System.Windows.Forms.Label stockIDError;
        private System.Windows.Forms.Button view;
        private System.Windows.Forms.Label viewLB;
        private System.Windows.Forms.TextBox virtualItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label virtualError;
    }
}