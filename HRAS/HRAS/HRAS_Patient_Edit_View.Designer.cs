﻿namespace HRAS
{
    partial class HRAS_Patient_Edit_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logout = new System.Windows.Forms.Button();
            this.patientMedicalRecordLB = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.genderLB = new System.Windows.Forms.Label();
            this.stateLB = new System.Windows.Forms.Label();
            this.zipLB = new System.Windows.Forms.Label();
            this.organDonorLB = new System.Windows.Forms.Label();
            this.birthDateLB = new System.Windows.Forms.Label();
            this.ssnLB = new System.Windows.Forms.Label();
            this.mInitialLB = new System.Windows.Forms.Label();
            this.lastNameLB = new System.Windows.Forms.Label();
            this.firstNameLB = new System.Windows.Forms.Label();
            this.HRAS_HomeLabel = new System.Windows.Forms.Label();
            this.organDonor = new System.Windows.Forms.TextBox();
            this.zip = new System.Windows.Forms.TextBox();
            this.city = new System.Windows.Forms.TextBox();
            this.state = new System.Windows.Forms.TextBox();
            this.birthDate = new System.Windows.Forms.TextBox();
            this.ssn = new System.Windows.Forms.TextBox();
            this.middleInitial = new System.Windows.Forms.TextBox();
            this.gender = new System.Windows.Forms.TextBox();
            this.firstName = new System.Windows.Forms.TextBox();
            this.lastName = new System.Windows.Forms.TextBox();
            this.cityLB = new System.Windows.Forms.Label();
            this.AddressLB = new System.Windows.Forms.Label();
            this.Address = new System.Windows.Forms.TextBox();
            this.view = new System.Windows.Forms.Button();
            this.viewLB = new System.Windows.Forms.Label();
            this.genderError = new System.Windows.Forms.Label();
            this.zipError = new System.Windows.Forms.Label();
            this.addressError = new System.Windows.Forms.Label();
            this.cityError = new System.Windows.Forms.Label();
            this.birthDateError = new System.Windows.Forms.Label();
            this.stateError = new System.Windows.Forms.Label();
            this.middleInitialError = new System.Windows.Forms.Label();
            this.firstNameError = new System.Windows.Forms.Label();
            this.lastNameError = new System.Windows.Forms.Label();
            this.organDonorError = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.address2LB = new System.Windows.Forms.Label();
            this.address2 = new System.Windows.Forms.TextBox();
            this.address2Error = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(924, -23);
            this.logout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(105, 23);
            this.logout.TabIndex = 95;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            // 
            // patientMedicalRecordLB
            // 
            this.patientMedicalRecordLB.AutoSize = true;
            this.patientMedicalRecordLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientMedicalRecordLB.Location = new System.Drawing.Point(63, 138);
            this.patientMedicalRecordLB.Name = "patientMedicalRecordLB";
            this.patientMedicalRecordLB.Size = new System.Drawing.Size(183, 38);
            this.patientMedicalRecordLB.TabIndex = 141;
            this.patientMedicalRecordLB.Text = "Patient Edit";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(646, 28);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 39);
            this.button1.TabIndex = 120;
            this.button1.Text = "Logout";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.logout_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(624, 623);
            this.back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(127, 23);
            this.back.TabIndex = 118;
            this.back.Text = "Home Screen";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.home_Click);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(104, 623);
            this.save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(103, 23);
            this.save.TabIndex = 117;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // genderLB
            // 
            this.genderLB.AutoSize = true;
            this.genderLB.Location = new System.Drawing.Point(80, 453);
            this.genderLB.Name = "genderLB";
            this.genderLB.Size = new System.Drawing.Size(56, 17);
            this.genderLB.TabIndex = 132;
            this.genderLB.Text = "Gender";
            // 
            // stateLB
            // 
            this.stateLB.AutoSize = true;
            this.stateLB.Location = new System.Drawing.Point(445, 259);
            this.stateLB.Name = "stateLB";
            this.stateLB.Size = new System.Drawing.Size(41, 17);
            this.stateLB.TabIndex = 131;
            this.stateLB.Text = "State";
            // 
            // zipLB
            // 
            this.zipLB.AutoSize = true;
            this.zipLB.Location = new System.Drawing.Point(445, 453);
            this.zipLB.Name = "zipLB";
            this.zipLB.Size = new System.Drawing.Size(28, 17);
            this.zipLB.TabIndex = 125;
            this.zipLB.Text = "Zip";
            // 
            // organDonorLB
            // 
            this.organDonorLB.AutoSize = true;
            this.organDonorLB.Location = new System.Drawing.Point(443, 519);
            this.organDonorLB.Name = "organDonorLB";
            this.organDonorLB.Size = new System.Drawing.Size(124, 17);
            this.organDonorLB.TabIndex = 122;
            this.organDonorLB.Text = "Organ Donor(Y/N)";
            // 
            // birthDateLB
            // 
            this.birthDateLB.AutoSize = true;
            this.birthDateLB.Location = new System.Drawing.Point(81, 519);
            this.birthDateLB.Name = "birthDateLB";
            this.birthDateLB.Size = new System.Drawing.Size(155, 17);
            this.birthDateLB.TabIndex = 121;
            this.birthDateLB.Text = "Birth Date(mm/dd/yyyy)";
            // 
            // ssnLB
            // 
            this.ssnLB.AutoSize = true;
            this.ssnLB.Location = new System.Drawing.Point(80, 193);
            this.ssnLB.Name = "ssnLB";
            this.ssnLB.Size = new System.Drawing.Size(36, 17);
            this.ssnLB.TabIndex = 120;
            this.ssnLB.Text = "SSN";
            // 
            // mInitialLB
            // 
            this.mInitialLB.AutoSize = true;
            this.mInitialLB.Location = new System.Drawing.Point(81, 393);
            this.mInitialLB.Name = "mInitialLB";
            this.mInitialLB.Size = new System.Drawing.Size(59, 17);
            this.mInitialLB.TabIndex = 119;
            this.mInitialLB.Text = "M. Initial";
            // 
            // lastNameLB
            // 
            this.lastNameLB.AutoSize = true;
            this.lastNameLB.Location = new System.Drawing.Point(80, 259);
            this.lastNameLB.Name = "lastNameLB";
            this.lastNameLB.Size = new System.Drawing.Size(76, 17);
            this.lastNameLB.TabIndex = 118;
            this.lastNameLB.Text = "Last Name";
            // 
            // firstNameLB
            // 
            this.firstNameLB.AutoSize = true;
            this.firstNameLB.Location = new System.Drawing.Point(81, 327);
            this.firstNameLB.Name = "firstNameLB";
            this.firstNameLB.Size = new System.Drawing.Size(76, 17);
            this.firstNameLB.TabIndex = 117;
            this.firstNameLB.Text = "First Name";
            // 
            // HRAS_HomeLabel
            // 
            this.HRAS_HomeLabel.AutoSize = true;
            this.HRAS_HomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRAS_HomeLabel.Location = new System.Drawing.Point(225, 9);
            this.HRAS_HomeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HRAS_HomeLabel.Name = "HRAS_HomeLabel";
            this.HRAS_HomeLabel.Size = new System.Drawing.Size(261, 91);
            this.HRAS_HomeLabel.TabIndex = 113;
            this.HRAS_HomeLabel.Text = "HRAS";
            this.HRAS_HomeLabel.Click += new System.EventHandler(this.HRAS_HomeLabel_Click);
            // 
            // organDonor
            // 
            this.organDonor.Location = new System.Drawing.Point(446, 538);
            this.organDonor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.organDonor.Name = "organDonor";
            this.organDonor.Size = new System.Drawing.Size(69, 22);
            this.organDonor.TabIndex = 112;
            // 
            // zip
            // 
            this.zip.Location = new System.Drawing.Point(448, 474);
            this.zip.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.zip.Name = "zip";
            this.zip.Size = new System.Drawing.Size(119, 22);
            this.zip.TabIndex = 110;
            // 
            // city
            // 
            this.city.Location = new System.Drawing.Point(448, 212);
            this.city.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(119, 22);
            this.city.TabIndex = 108;
            // 
            // state
            // 
            this.state.Location = new System.Drawing.Point(448, 278);
            this.state.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(55, 22);
            this.state.TabIndex = 109;
            // 
            // birthDate
            // 
            this.birthDate.Location = new System.Drawing.Point(83, 538);
            this.birthDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.birthDate.Name = "birthDate";
            this.birthDate.Size = new System.Drawing.Size(124, 22);
            this.birthDate.TabIndex = 101;
            // 
            // ssn
            // 
            this.ssn.Location = new System.Drawing.Point(80, 212);
            this.ssn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ssn.Name = "ssn";
            this.ssn.Size = new System.Drawing.Size(157, 22);
            this.ssn.TabIndex = 100;
            // 
            // middleInitial
            // 
            this.middleInitial.Location = new System.Drawing.Point(84, 412);
            this.middleInitial.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.middleInitial.Name = "middleInitial";
            this.middleInitial.Size = new System.Drawing.Size(49, 22);
            this.middleInitial.TabIndex = 98;
            // 
            // gender
            // 
            this.gender.Location = new System.Drawing.Point(83, 474);
            this.gender.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gender.Name = "gender";
            this.gender.Size = new System.Drawing.Size(57, 22);
            this.gender.TabIndex = 99;
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(83, 347);
            this.firstName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(163, 22);
            this.firstName.TabIndex = 97;
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(83, 278);
            this.lastName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(163, 22);
            this.lastName.TabIndex = 96;
            this.lastName.TextChanged += new System.EventHandler(this.lastName_TextChanged);
            // 
            // cityLB
            // 
            this.cityLB.AutoSize = true;
            this.cityLB.Location = new System.Drawing.Point(445, 193);
            this.cityLB.Name = "cityLB";
            this.cityLB.Size = new System.Drawing.Size(31, 17);
            this.cityLB.TabIndex = 124;
            this.cityLB.Text = "City";
            // 
            // AddressLB
            // 
            this.AddressLB.AutoSize = true;
            this.AddressLB.Location = new System.Drawing.Point(445, 327);
            this.AddressLB.Name = "AddressLB";
            this.AddressLB.Size = new System.Drawing.Size(103, 17);
            this.AddressLB.TabIndex = 146;
            this.AddressLB.Text = "Address Line 1";
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(448, 350);
            this.Address.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(119, 22);
            this.Address.TabIndex = 145;
            // 
            // view
            // 
            this.view.Location = new System.Drawing.Point(257, 623);
            this.view.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(103, 23);
            this.view.TabIndex = 147;
            this.view.Text = "View";
            this.view.UseVisualStyleBackColor = true;
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // viewLB
            // 
            this.viewLB.AutoSize = true;
            this.viewLB.Location = new System.Drawing.Point(238, 604);
            this.viewLB.Name = "viewLB";
            this.viewLB.Size = new System.Drawing.Size(153, 17);
            this.viewLB.TabIndex = 148;
            this.viewLB.Text = "Must Save Patient First";
            // 
            // genderError
            // 
            this.genderError.AutoSize = true;
            this.genderError.ForeColor = System.Drawing.Color.Red;
            this.genderError.Location = new System.Drawing.Point(146, 453);
            this.genderError.Name = "genderError";
            this.genderError.Size = new System.Drawing.Size(0, 17);
            this.genderError.TabIndex = 149;
            // 
            // zipError
            // 
            this.zipError.AutoSize = true;
            this.zipError.ForeColor = System.Drawing.Color.Red;
            this.zipError.Location = new System.Drawing.Point(479, 453);
            this.zipError.Name = "zipError";
            this.zipError.Size = new System.Drawing.Size(0, 17);
            this.zipError.TabIndex = 150;
            // 
            // addressError
            // 
            this.addressError.AutoSize = true;
            this.addressError.ForeColor = System.Drawing.Color.Red;
            this.addressError.Location = new System.Drawing.Point(554, 327);
            this.addressError.Name = "addressError";
            this.addressError.Size = new System.Drawing.Size(0, 17);
            this.addressError.TabIndex = 151;
            // 
            // cityError
            // 
            this.cityError.AutoSize = true;
            this.cityError.ForeColor = System.Drawing.Color.Red;
            this.cityError.Location = new System.Drawing.Point(485, 193);
            this.cityError.Name = "cityError";
            this.cityError.Size = new System.Drawing.Size(0, 17);
            this.cityError.TabIndex = 152;
            // 
            // birthDateError
            // 
            this.birthDateError.AutoSize = true;
            this.birthDateError.ForeColor = System.Drawing.Color.Red;
            this.birthDateError.Location = new System.Drawing.Point(242, 519);
            this.birthDateError.Name = "birthDateError";
            this.birthDateError.Size = new System.Drawing.Size(0, 17);
            this.birthDateError.TabIndex = 153;
            // 
            // stateError
            // 
            this.stateError.AutoSize = true;
            this.stateError.ForeColor = System.Drawing.Color.Red;
            this.stateError.Location = new System.Drawing.Point(492, 259);
            this.stateError.Name = "stateError";
            this.stateError.Size = new System.Drawing.Size(0, 17);
            this.stateError.TabIndex = 154;
            // 
            // middleInitialError
            // 
            this.middleInitialError.AutoSize = true;
            this.middleInitialError.ForeColor = System.Drawing.Color.Red;
            this.middleInitialError.Location = new System.Drawing.Point(146, 393);
            this.middleInitialError.Name = "middleInitialError";
            this.middleInitialError.Size = new System.Drawing.Size(0, 17);
            this.middleInitialError.TabIndex = 155;
            // 
            // firstNameError
            // 
            this.firstNameError.AutoSize = true;
            this.firstNameError.ForeColor = System.Drawing.Color.Red;
            this.firstNameError.Location = new System.Drawing.Point(175, 327);
            this.firstNameError.Name = "firstNameError";
            this.firstNameError.Size = new System.Drawing.Size(0, 17);
            this.firstNameError.TabIndex = 156;
            // 
            // lastNameError
            // 
            this.lastNameError.AutoSize = true;
            this.lastNameError.ForeColor = System.Drawing.Color.Red;
            this.lastNameError.Location = new System.Drawing.Point(175, 259);
            this.lastNameError.Name = "lastNameError";
            this.lastNameError.Size = new System.Drawing.Size(0, 17);
            this.lastNameError.TabIndex = 157;
            // 
            // organDonorError
            // 
            this.organDonorError.AutoSize = true;
            this.organDonorError.ForeColor = System.Drawing.Color.Red;
            this.organDonorError.Location = new System.Drawing.Point(574, 519);
            this.organDonorError.Name = "organDonorError";
            this.organDonorError.Size = new System.Drawing.Size(0, 17);
            this.organDonorError.TabIndex = 158;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.8F);
            this.message.ForeColor = System.Drawing.Color.ForestGreen;
            this.message.Location = new System.Drawing.Point(268, 144);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 32);
            this.message.TabIndex = 159;
            // 
            // address2LB
            // 
            this.address2LB.AutoSize = true;
            this.address2LB.Location = new System.Drawing.Point(445, 393);
            this.address2LB.Name = "address2LB";
            this.address2LB.Size = new System.Drawing.Size(170, 17);
            this.address2LB.TabIndex = 160;
            this.address2LB.Text = "(Optional) Address Line 2";
            // 
            // address2
            // 
            this.address2.Location = new System.Drawing.Point(448, 412);
            this.address2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.address2.Name = "address2";
            this.address2.Size = new System.Drawing.Size(119, 22);
            this.address2.TabIndex = 161;
            // 
            // address2Error
            // 
            this.address2Error.AutoSize = true;
            this.address2Error.ForeColor = System.Drawing.Color.Red;
            this.address2Error.Location = new System.Drawing.Point(621, 393);
            this.address2Error.Name = "address2Error";
            this.address2Error.Size = new System.Drawing.Size(0, 17);
            this.address2Error.TabIndex = 162;
            // 
            // HRAS_Patient_Edit_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 741);
            this.Controls.Add(this.address2Error);
            this.Controls.Add(this.address2);
            this.Controls.Add(this.address2LB);
            this.Controls.Add(this.message);
            this.Controls.Add(this.organDonorError);
            this.Controls.Add(this.lastNameError);
            this.Controls.Add(this.firstNameError);
            this.Controls.Add(this.middleInitialError);
            this.Controls.Add(this.stateError);
            this.Controls.Add(this.birthDateError);
            this.Controls.Add(this.cityError);
            this.Controls.Add(this.addressError);
            this.Controls.Add(this.zipError);
            this.Controls.Add(this.genderError);
            this.Controls.Add(this.viewLB);
            this.Controls.Add(this.view);
            this.Controls.Add(this.AddressLB);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.patientMedicalRecordLB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.back);
            this.Controls.Add(this.save);
            this.Controls.Add(this.genderLB);
            this.Controls.Add(this.stateLB);
            this.Controls.Add(this.zipLB);
            this.Controls.Add(this.cityLB);
            this.Controls.Add(this.organDonorLB);
            this.Controls.Add(this.birthDateLB);
            this.Controls.Add(this.ssnLB);
            this.Controls.Add(this.mInitialLB);
            this.Controls.Add(this.lastNameLB);
            this.Controls.Add(this.firstNameLB);
            this.Controls.Add(this.HRAS_HomeLabel);
            this.Controls.Add(this.organDonor);
            this.Controls.Add(this.zip);
            this.Controls.Add(this.city);
            this.Controls.Add(this.state);
            this.Controls.Add(this.birthDate);
            this.Controls.Add(this.ssn);
            this.Controls.Add(this.middleInitial);
            this.Controls.Add(this.gender);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.logout);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "HRAS_Patient_Edit_View";
            this.Text = "Patient Edit";
            this.Load += new System.EventHandler(this.HRAS_Patient_Create_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Label patientMedicalRecordLB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label genderLB;
        private System.Windows.Forms.Label stateLB;
        private System.Windows.Forms.Label zipLB;
        private System.Windows.Forms.Label organDonorLB;
        private System.Windows.Forms.Label birthDateLB;
        private System.Windows.Forms.Label ssnLB;
        private System.Windows.Forms.Label mInitialLB;
        private System.Windows.Forms.Label lastNameLB;
        private System.Windows.Forms.Label firstNameLB;
        private System.Windows.Forms.Label HRAS_HomeLabel;
        private System.Windows.Forms.TextBox organDonor;
        private System.Windows.Forms.TextBox zip;
        private System.Windows.Forms.TextBox city;
        private System.Windows.Forms.TextBox state;
        private System.Windows.Forms.TextBox birthDate;
        private System.Windows.Forms.TextBox ssn;
        private System.Windows.Forms.TextBox middleInitial;
        private System.Windows.Forms.TextBox gender;
        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.Label cityLB;
        private System.Windows.Forms.Label AddressLB;
        private System.Windows.Forms.TextBox Address;
        private System.Windows.Forms.Button view;
        private System.Windows.Forms.Label viewLB;
        private System.Windows.Forms.Label genderError;
        private System.Windows.Forms.Label zipError;
        private System.Windows.Forms.Label addressError;
        private System.Windows.Forms.Label cityError;
        private System.Windows.Forms.Label birthDateError;
        private System.Windows.Forms.Label stateError;
        private System.Windows.Forms.Label middleInitialError;
        private System.Windows.Forms.Label firstNameError;
        private System.Windows.Forms.Label lastNameError;
        private System.Windows.Forms.Label organDonorError;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label address2LB;
        private System.Windows.Forms.TextBox address2;
        private System.Windows.Forms.Label address2Error;
    }
}