﻿namespace HRAS
{
    partial class HRAS_Inventory_Item_Search_Results_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logout = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.back = new System.Windows.Forms.Button();
            this.ItemSearchResults = new System.Windows.Forms.DataGridView();
            this.HRAS_label = new System.Windows.Forms.Label();
            this.stockIFLB = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ItemSearchResults)).BeginInit();
            this.SuspendLayout();
            // 
            // logout
            // 
            this.logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.logout.Location = new System.Drawing.Point(821, 13);
            this.logout.Margin = new System.Windows.Forms.Padding(4);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(115, 48);
            this.logout.TabIndex = 1;
            this.logout.Text = "Log Out";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.75F);
            this.label2.Location = new System.Drawing.Point(47, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(316, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "Item Search Results for:";
            // 
            // back
            // 
            this.back.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.back.Location = new System.Drawing.Point(831, 443);
            this.back.Margin = new System.Windows.Forms.Padding(4);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(115, 48);
            this.back.TabIndex = 6;
            this.back.Text = "Home Screen";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // ItemSearchResults
            // 
            this.ItemSearchResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ItemSearchResults.Location = new System.Drawing.Point(53, 250);
            this.ItemSearchResults.Margin = new System.Windows.Forms.Padding(4);
            this.ItemSearchResults.Name = "ItemSearchResults";
            this.ItemSearchResults.ReadOnly = true;
            this.ItemSearchResults.Size = new System.Drawing.Size(893, 185);
            this.ItemSearchResults.TabIndex = 7;
            this.ItemSearchResults.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ItemSearchResults_CellContentClick);
            // 
            // HRAS_label
            // 
            this.HRAS_label.AutoSize = true;
            this.HRAS_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRAS_label.Location = new System.Drawing.Point(294, 13);
            this.HRAS_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HRAS_label.Name = "HRAS_label";
            this.HRAS_label.Size = new System.Drawing.Size(261, 91);
            this.HRAS_label.TabIndex = 12;
            this.HRAS_label.Text = "HRAS";
            // 
            // stockIFLB
            // 
            this.stockIFLB.AutoSize = true;
            this.stockIFLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockIFLB.Location = new System.Drawing.Point(52, 193);
            this.stockIFLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.stockIFLB.Name = "stockIFLB";
            this.stockIFLB.Size = new System.Drawing.Size(100, 20);
            this.stockIFLB.TabIndex = 13;
            this.stockIFLB.Text = "Description:";
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.description.Location = new System.Drawing.Point(171, 193);
            this.description.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(0, 20);
            this.description.TabIndex = 14;
            // 
            // HRAS_Inventory_Item_Search_Results_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 527);
            this.Controls.Add(this.description);
            this.Controls.Add(this.stockIFLB);
            this.Controls.Add(this.HRAS_label);
            this.Controls.Add(this.ItemSearchResults);
            this.Controls.Add(this.back);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.logout);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HRAS_Inventory_Item_Search_Results_View";
            this.Text = "Item Search Results";
            this.Load += new System.EventHandler(this.HRAS_Inventory_Item_Search_Results_View_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ItemSearchResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.DataGridView ItemSearchResults;
        private System.Windows.Forms.Label HRAS_label;
        private System.Windows.Forms.Label stockIFLB;
        private System.Windows.Forms.Label description;
    }
}