﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HRAS_Middleware;

namespace HRAS
{
    public partial class HRAS_Inventory_Item_Create_View : Form
    {

        private InventoryItem newItem;

        public HRAS_Inventory_Item_Create_View()
        {
            
            InitializeComponent();
            newItem = new InventoryItem();
            view.Enabled = false;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {

            message.Text = "";
            if (isValidInput())
            {
                //middleware connection object
                ItemHandler handler = new ItemHandler();

                //take textbox values
                newItem.id = StockIDtextBox.Text;                            
                newItem.size = Double.Parse(SizeTextBox.Text);
                newItem.cost = Double.Parse(CostTextBox.Text);
                newItem.description = descriptionTextBox.Text;

                if (virtualItem.Text.ToUpper() == "Y")
                {
                    newItem.isVIRTL = true;                  
                }            
                else
                {
                    newItem.isVIRTL = false;
                    newItem.quantity = Int32.Parse(QuantitytextBox.Text);
                }
                                             
                //create new item and set buttons and messages states appropriately
                  try
                    {
                        handler.createItem(newItem);

                        view.Enabled = true;
                        viewLB.Hide();
                        message.Text = "Inventory Item Created";
                        message.ForeColor = System.Drawing.Color.ForestGreen;
                        SaveButton.Enabled = false;

                        setFieldsToReadOnly();

                    }
                    catch (Exception ex)
                    {
                        message.Text = ex.Message;
                        message.ForeColor = System.Drawing.Color.Red;
                    }
            }

        }
        private bool isValidInput()
        {
            bool isValid = true;
            //checking input validity textbox by textbox

            //Stock ID
            if (StockIDtextBox.Text.Length == 0)
            {
                stockIDError.Text = "Required Field";
                isValid = false;
            }
            else if (StockIDtextBox.Text.Length > 5)
            {
                stockIDError.Text = "Must be at most 5 chars";
                isValid = false;
            }
            else { stockIDError.Text = ""; }


            //Quantity
            int b = 0;
            int.TryParse(QuantitytextBox.Text, out b);

            if (QuantitytextBox.Text.Length == 0 && virtualItem.Text.ToUpper() != "Y") // If virtual item, quantity box can be empty. 
            {
                quantityError.Text = "Required Field ( for non-virtual items )";
                isValid = false;
            }
            else if (b == 0 && !QuantitytextBox.Text.Equals("UNLIMITED"))
            {
                quantityError.Text = "Must be a whole number";
                isValid = false;
            }
            else { quantityError.Text = ""; }

            //Size
            double c = 0;
            double.TryParse(SizeTextBox.Text, out c);

            if (SizeTextBox.Text.Length == 0)
            {
                sizeError.Text = "Required Field";
                isValid = false;
            }
            else if (c == 0)
            {
                sizeError.Text = "Must be a real number";
                isValid = false;
            }
            else { sizeError.Text = ""; }

            //Cost
            double d = 0;
            double.TryParse(CostTextBox.Text, out d);

            if (CostTextBox.Text.Length == 0)
            {
                costError.Text = "Required Field";
                isValid = false;
            }
            else if (d == 0)
            {
                costError.Text = "Must be a real number";
                isValid = false;
            }
            else { costError.Text = ""; }

            //Description
            if (descriptionTextBox.Text.Length == 0)
            {
                descriptionError.Text = "Required Field";
                isValid = false;
            }
            else if (descriptionTextBox.Text.Length > 100)
            {
                descriptionError.Text = "Must be at most 100 chars";
                isValid = false;
            }
            else { descriptionError.Text = ""; }

            //virtual
            if(virtualItem.Text.Length == 0)
            {
                virtualError.Text = "Required Field";
                isValid = false;
            }
            else if(!(virtualItem.Text.ToUpper() == "Y" || virtualItem.Text.ToUpper() == "N"))
            {
                virtualError.Text = "Must be Y/N";
                isValid = false;
            }
            else { virtualError.Text = ""; }
          
            if (!isValid)
            {
                message.Text = "";
                view.Enabled = false;
            }
            return isValid;
        }
        private void setFieldsToReadOnly()
        {
            StockIDtextBox.ReadOnly = true;
            QuantitytextBox.ReadOnly = true;
            SizeTextBox.ReadOnly = true;
            CostTextBox.ReadOnly = true;
            descriptionTextBox.ReadOnly = true;
            virtualItem.ReadOnly = true;
        }
        private void virtual_TextChanged(object sender, EventArgs e)
        {
            if (virtualItem.Text.ToUpper() == "Y")
            {
                QuantitytextBox.Text = "UNLIMITED";
                QuantitytextBox.ReadOnly = true;
            }
            else
            {
                QuantitytextBox.Text = "";
                QuantitytextBox.ReadOnly = false;
            }
        }

        private void homeScreen_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

       
        private void view_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Inventory_Item_View

            Hide();
            HRAS_Inventory_Item_View_View loginWindow = new HRAS_Inventory_Item_View_View(newItem);
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();

        }
        private void HRAS_Label_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Home_View

            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void StockIDtextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void QuantitytextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void SizeTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void CostTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void DescriptionTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Inventory_Item_Create_View_Load(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        
    }
}
