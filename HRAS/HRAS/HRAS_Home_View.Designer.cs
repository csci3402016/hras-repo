﻿namespace HRAS
{
    partial class HRAS_Home_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HRAS_HomeLabel = new System.Windows.Forms.Label();
            this.patientsRB = new System.Windows.Forms.RadioButton();
            this.inventoryItemRB = new System.Windows.Forms.RadioButton();
            this.lastNameTB = new System.Windows.Forms.TextBox();
            this.ssnTB = new System.Windows.Forms.TextBox();
            this.roomNumberTB = new System.Windows.Forms.TextBox();
            this.inventoryItemNameTB = new System.Windows.Forms.TextBox();
            this.newPatient = new System.Windows.Forms.Button();
            this.diagnosisWizard = new System.Windows.Forms.Button();
            this.newInventoryItem = new System.Windows.Forms.Button();
            this.search = new System.Windows.Forms.Button();
            this.roomNumberLabel = new System.Windows.Forms.Label();
            this.ssnLabel = new System.Windows.Forms.Label();
            this.patientNameLabel = new System.Windows.Forms.Label();
            this.inventoryItemNameLabel = new System.Windows.Forms.Label();
            this.logout = new System.Windows.Forms.Button();
            this.firstNameTB = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.adminMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // HRAS_HomeLabel
            // 
            this.HRAS_HomeLabel.AutoSize = true;
            this.HRAS_HomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HRAS_HomeLabel.Location = new System.Drawing.Point(415, 11);
            this.HRAS_HomeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HRAS_HomeLabel.Name = "HRAS_HomeLabel";
            this.HRAS_HomeLabel.Size = new System.Drawing.Size(261, 91);
            this.HRAS_HomeLabel.TabIndex = 14;
            this.HRAS_HomeLabel.Text = "HRAS";
            this.HRAS_HomeLabel.Click += new System.EventHandler(this.HRAS_HomeLabel_Click);
            // 
            // patientsRB
            // 
            this.patientsRB.AutoSize = true;
            this.patientsRB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patientsRB.Location = new System.Drawing.Point(202, 124);
            this.patientsRB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.patientsRB.Name = "patientsRB";
            this.patientsRB.Size = new System.Drawing.Size(120, 33);
            this.patientsRB.TabIndex = 0;
            this.patientsRB.TabStop = true;
            this.patientsRB.Text = "Patients";
            this.patientsRB.UseVisualStyleBackColor = true;
            this.patientsRB.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // inventoryItemRB
            // 
            this.inventoryItemRB.AutoSize = true;
            this.inventoryItemRB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventoryItemRB.Location = new System.Drawing.Point(691, 124);
            this.inventoryItemRB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inventoryItemRB.Name = "inventoryItemRB";
            this.inventoryItemRB.Size = new System.Drawing.Size(194, 33);
            this.inventoryItemRB.TabIndex = 5;
            this.inventoryItemRB.TabStop = true;
            this.inventoryItemRB.Text = "Inventory Items";
            this.inventoryItemRB.UseVisualStyleBackColor = true;
            this.inventoryItemRB.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // lastNameTB
            // 
            this.lastNameTB.Location = new System.Drawing.Point(167, 206);
            this.lastNameTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lastNameTB.Name = "lastNameTB";
            this.lastNameTB.Size = new System.Drawing.Size(249, 22);
            this.lastNameTB.TabIndex = 2;
            // 
            // ssnTB
            // 
            this.ssnTB.Location = new System.Drawing.Point(167, 251);
            this.ssnTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ssnTB.Name = "ssnTB";
            this.ssnTB.Size = new System.Drawing.Size(249, 22);
            this.ssnTB.TabIndex = 3;
            this.ssnTB.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // roomNumberTB
            // 
            this.roomNumberTB.Location = new System.Drawing.Point(167, 296);
            this.roomNumberTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.roomNumberTB.Name = "roomNumberTB";
            this.roomNumberTB.Size = new System.Drawing.Size(249, 22);
            this.roomNumberTB.TabIndex = 4;
            // 
            // inventoryItemNameTB
            // 
            this.inventoryItemNameTB.Location = new System.Drawing.Point(671, 163);
            this.inventoryItemNameTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.inventoryItemNameTB.Name = "inventoryItemNameTB";
            this.inventoryItemNameTB.Size = new System.Drawing.Size(243, 22);
            this.inventoryItemNameTB.TabIndex = 6;
            // 
            // newPatient
            // 
            this.newPatient.Location = new System.Drawing.Point(66, 435);
            this.newPatient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.newPatient.Name = "newPatient";
            this.newPatient.Size = new System.Drawing.Size(120, 47);
            this.newPatient.TabIndex = 7;
            this.newPatient.Text = "New Patient";
            this.newPatient.UseVisualStyleBackColor = true;
            this.newPatient.Click += new System.EventHandler(this.newPatient_Click);
            // 
            // diagnosisWizard
            // 
            this.diagnosisWizard.Location = new System.Drawing.Point(254, 435);
            this.diagnosisWizard.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.diagnosisWizard.Name = "diagnosisWizard";
            this.diagnosisWizard.Size = new System.Drawing.Size(127, 47);
            this.diagnosisWizard.TabIndex = 8;
            this.diagnosisWizard.Text = "Diagnosis Wizard";
            this.diagnosisWizard.UseVisualStyleBackColor = true;
            this.diagnosisWizard.Click += new System.EventHandler(this.diagnosisWizard_Click);
            // 
            // newInventoryItem
            // 
            this.newInventoryItem.Location = new System.Drawing.Point(444, 435);
            this.newInventoryItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.newInventoryItem.Name = "newInventoryItem";
            this.newInventoryItem.Size = new System.Drawing.Size(139, 47);
            this.newInventoryItem.TabIndex = 9;
            this.newInventoryItem.Text = "*(New Inventory Item)";
            this.newInventoryItem.UseVisualStyleBackColor = true;
            this.newInventoryItem.Click += new System.EventHandler(this.newInventoryItem_Click);
            // 
            // search
            // 
            this.search.Location = new System.Drawing.Point(444, 344);
            this.search.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(152, 49);
            this.search.TabIndex = 7;
            this.search.Text = "Search";
            this.search.UseVisualStyleBackColor = true;
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // roomNumberLabel
            // 
            this.roomNumberLabel.AutoSize = true;
            this.roomNumberLabel.Location = new System.Drawing.Point(62, 300);
            this.roomNumberLabel.Name = "roomNumberLabel";
            this.roomNumberLabel.Size = new System.Drawing.Size(99, 17);
            this.roomNumberLabel.TabIndex = 12;
            this.roomNumberLabel.Text = "Room Number";
            // 
            // ssnLabel
            // 
            this.ssnLabel.AutoSize = true;
            this.ssnLabel.Location = new System.Drawing.Point(62, 254);
            this.ssnLabel.Name = "ssnLabel";
            this.ssnLabel.Size = new System.Drawing.Size(36, 17);
            this.ssnLabel.TabIndex = 11;
            this.ssnLabel.Text = "SSN";
            this.ssnLabel.Click += new System.EventHandler(this.label2_Click);
            // 
            // patientNameLabel
            // 
            this.patientNameLabel.AutoSize = true;
            this.patientNameLabel.Location = new System.Drawing.Point(62, 167);
            this.patientNameLabel.Name = "patientNameLabel";
            this.patientNameLabel.Size = new System.Drawing.Size(76, 17);
            this.patientNameLabel.TabIndex = 9;
            this.patientNameLabel.Text = "First Name";
            this.patientNameLabel.Click += new System.EventHandler(this.patientNameLabel_Click);
            // 
            // inventoryItemNameLabel
            // 
            this.inventoryItemNameLabel.AutoSize = true;
            this.inventoryItemNameLabel.Location = new System.Drawing.Point(606, 167);
            this.inventoryItemNameLabel.Name = "inventoryItemNameLabel";
            this.inventoryItemNameLabel.Size = new System.Drawing.Size(45, 17);
            this.inventoryItemNameLabel.TabIndex = 13;
            this.inventoryItemNameLabel.Text = "Name";
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(815, 11);
            this.logout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(99, 43);
            this.logout.TabIndex = 10;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // firstNameTB
            // 
            this.firstNameTB.Location = new System.Drawing.Point(167, 163);
            this.firstNameTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.firstNameTB.Name = "firstNameTB";
            this.firstNameTB.Size = new System.Drawing.Size(249, 22);
            this.firstNameTB.TabIndex = 1;
            this.firstNameTB.TextChanged += new System.EventHandler(this.firstNameTB_TextChanged);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(62, 210);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(76, 17);
            this.label.TabIndex = 10;
            this.label.Text = "Last Name";
            // 
            // adminMessage
            // 
            this.adminMessage.AutoSize = true;
            this.adminMessage.Location = new System.Drawing.Point(408, 416);
            this.adminMessage.Name = "adminMessage";
            this.adminMessage.Size = new System.Drawing.Size(0, 17);
            this.adminMessage.TabIndex = 15;
            // 
            // HRAS_Home_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 535);
            this.Controls.Add(this.adminMessage);
            this.Controls.Add(this.label);
            this.Controls.Add(this.firstNameTB);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.inventoryItemNameLabel);
            this.Controls.Add(this.patientNameLabel);
            this.Controls.Add(this.ssnLabel);
            this.Controls.Add(this.roomNumberLabel);
            this.Controls.Add(this.search);
            this.Controls.Add(this.newInventoryItem);
            this.Controls.Add(this.diagnosisWizard);
            this.Controls.Add(this.newPatient);
            this.Controls.Add(this.inventoryItemNameTB);
            this.Controls.Add(this.roomNumberTB);
            this.Controls.Add(this.ssnTB);
            this.Controls.Add(this.lastNameTB);
            this.Controls.Add(this.inventoryItemRB);
            this.Controls.Add(this.patientsRB);
            this.Controls.Add(this.HRAS_HomeLabel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HRAS_Home_View";
            this.Text = "HRAS";
            this.Load += new System.EventHandler(this.HRAS_Home_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HRAS_HomeLabel;
        private System.Windows.Forms.RadioButton patientsRB;
        private System.Windows.Forms.RadioButton inventoryItemRB;
        private System.Windows.Forms.TextBox lastNameTB;
        private System.Windows.Forms.TextBox ssnTB;
        private System.Windows.Forms.TextBox roomNumberTB;
        private System.Windows.Forms.TextBox inventoryItemNameTB;
        private System.Windows.Forms.Button newPatient;
        private System.Windows.Forms.Button diagnosisWizard;
        private System.Windows.Forms.Button newInventoryItem;
        private System.Windows.Forms.Button search;
        private System.Windows.Forms.Label roomNumberLabel;
        private System.Windows.Forms.Label ssnLabel;
        private System.Windows.Forms.Label patientNameLabel;
        private System.Windows.Forms.Label inventoryItemNameLabel;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.TextBox firstNameTB;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label adminMessage;
    }
}