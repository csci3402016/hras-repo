﻿namespace HRAS
{
    partial class HRAS_Login_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancel_Button = new System.Windows.Forms.Button();
            this.Submit_Button = new System.Windows.Forms.Button();
            this.Username_textbox = new System.Windows.Forms.TextBox();
            this.Password_textbox = new System.Windows.Forms.TextBox();
            this.UsernameLB = new System.Windows.Forms.Label();
            this.PasswordLB = new System.Windows.Forms.Label();
            this.LoginErrorLB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.Location = new System.Drawing.Point(307, 105);
            this.Cancel_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(100, 28);
            this.Cancel_Button.TabIndex = 3;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.UseVisualStyleBackColor = true;
            this.Cancel_Button.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Submit_Button
            // 
            this.Submit_Button.Location = new System.Drawing.Point(177, 105);
            this.Submit_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Submit_Button.Name = "Submit_Button";
            this.Submit_Button.Size = new System.Drawing.Size(100, 28);
            this.Submit_Button.TabIndex = 2;
            this.Submit_Button.Text = "Submit";
            this.Submit_Button.UseVisualStyleBackColor = true;
            this.Submit_Button.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // Username_textbox
            // 
            this.Username_textbox.Location = new System.Drawing.Point(160, 41);
            this.Username_textbox.Margin = new System.Windows.Forms.Padding(4);
            this.Username_textbox.Name = "Username_textbox";
            this.Username_textbox.Size = new System.Drawing.Size(259, 22);
            this.Username_textbox.TabIndex = 0;
            this.Username_textbox.TextChanged += new System.EventHandler(this.Username_textbox_TextChanged);
            // 
            // Password_textbox
            // 
            this.Password_textbox.Location = new System.Drawing.Point(160, 73);
            this.Password_textbox.Margin = new System.Windows.Forms.Padding(4);
            this.Password_textbox.Name = "Password_textbox";
            this.Password_textbox.Size = new System.Drawing.Size(259, 22);
            this.Password_textbox.TabIndex = 1;
            this.Password_textbox.TextChanged += new System.EventHandler(this.Password_textbox_TextChanged);
            // 
            // UsernameLB
            // 
            this.UsernameLB.AutoSize = true;
            this.UsernameLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameLB.Location = new System.Drawing.Point(29, 41);
            this.UsernameLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.UsernameLB.Name = "UsernameLB";
            this.UsernameLB.Size = new System.Drawing.Size(102, 25);
            this.UsernameLB.TabIndex = 4;
            this.UsernameLB.Text = "Username";
            // 
            // PasswordLB
            // 
            this.PasswordLB.AutoSize = true;
            this.PasswordLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordLB.Location = new System.Drawing.Point(32, 76);
            this.PasswordLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PasswordLB.Name = "PasswordLB";
            this.PasswordLB.Size = new System.Drawing.Size(98, 25);
            this.PasswordLB.TabIndex = 5;
            this.PasswordLB.Text = "Password";
            // 
            // LoginErrorLB
            // 
            this.LoginErrorLB.AutoSize = true;
            this.LoginErrorLB.ForeColor = System.Drawing.Color.Maroon;
            this.LoginErrorLB.Location = new System.Drawing.Point(129, 172);
            this.LoginErrorLB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LoginErrorLB.Name = "LoginErrorLB";
            this.LoginErrorLB.Size = new System.Drawing.Size(214, 17);
            this.LoginErrorLB.TabIndex = 6;
            this.LoginErrorLB.Text = "Username or Password Incorrect";
            this.LoginErrorLB.Visible = false;
            this.LoginErrorLB.Click += new System.EventHandler(this.LoginErrorLB_Click);
            // 
            // HRAS_Login_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 236);
            this.Controls.Add(this.LoginErrorLB);
            this.Controls.Add(this.PasswordLB);
            this.Controls.Add(this.UsernameLB);
            this.Controls.Add(this.Password_textbox);
            this.Controls.Add(this.Username_textbox);
            this.Controls.Add(this.Submit_Button);
            this.Controls.Add(this.Cancel_Button);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HRAS_Login_View";
            this.Text = "HRAS Login";
            this.Load += new System.EventHandler(this.HRAS_Login_View_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cancel_Button;
        private System.Windows.Forms.Button Submit_Button;
        private System.Windows.Forms.TextBox Username_textbox;
        private System.Windows.Forms.TextBox Password_textbox;
        private System.Windows.Forms.Label UsernameLB;
        private System.Windows.Forms.Label PasswordLB;


        //Use these two declarations in conjuction with middleware communication to display the "Incorrect Username or Password"
        //error when a the username/password combination is not found within the database 
        private bool correctEntry = true;
        private System.Windows.Forms.Label LoginErrorLB;
    }
}

