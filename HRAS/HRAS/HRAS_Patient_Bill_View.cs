﻿using System;
using System.Windows.Forms;
using HRAS_Middleware;
using System.Collections.Generic;

namespace HRAS
{
    public partial class HRAS_Patient_Bill_View : Form
    {

        private MedicalRecord currentRecord;
        private Patient currentPatient;
        private DateTime timeIn;
        private DateTime timeOut;
        private int stayTime;


        public HRAS_Patient_Bill_View(Patient inPatient) //Overloaded constructor in the case that patient has no medical record (as in case Patient Create)
        {
            InitializeComponent();

            currentPatient = new Patient();
            currentPatient.deepCopy(inPatient);

            displayPersonInfoOnly();
        }

        public HRAS_Patient_Bill_View(Patient inPatient, MedicalRecord inRecord)
        {
            InitializeComponent();

            currentPatient = new Patient();
            currentPatient.deepCopy(inPatient);

            currentRecord = new MedicalRecord();
            currentRecord.deepCopy(inRecord);

            displayInfoToFields();
        }


        private void displayPersonInfoOnly()
        {
            NameTB.Text = currentPatient.firstName + " " + currentPatient.lastName;
            AddressTB.Text = currentPatient.address + " " + currentPatient.address2;
            CityTB.Text = currentPatient.city;
            StateTB.Text = currentPatient.state;
            ZipCodeTB.Text = currentPatient.zipCode;
        }

        private void displayInfoToFields()
        {
            double billTotal = 0;

            ItemHandler connection = new ItemHandler();
            List<InventoryItem> usedOnPatient = connection.getItemsUsedByStay(currentRecord, currentPatient);

            InventoryUsageInfo.DataSource = 


            InventoryUsageInfo.AutoGenerateColumns = false;

            InventoryUsageInfo.Columns["id"].DisplayIndex = 0;
            InventoryUsageInfo.Columns["id"].HeaderText = "Stock ID";
                      
            InventoryUsageInfo.Columns["quantity"].DisplayIndex = 1;
            InventoryUsageInfo.Columns["quantity"].HeaderText = "Quantity";

            InventoryUsageInfo.Columns["cost"].DisplayIndex = 3;
            InventoryUsageInfo.Columns["cost"].HeaderText = "Cost";

            InventoryUsageInfo.Columns["size"].Visible = false;
            InventoryUsageInfo.Columns["description"].Visible = false;
            InventoryUsageInfo.Columns["isVIRTL"].Visible = false;

            foreach (InventoryItem toAdd in currentRecord.inventoryItems)
            {
                billTotal += (toAdd.cost * (double)toAdd.quantity);
            }

            BillTotalTB.Text = "" + billTotal;
            NameTB.Text = currentPatient.firstName + " " + currentPatient.lastName;
            AddressTB.Text = currentPatient.address + " " + currentPatient.address2;
            CityTB.Text = currentPatient.city;
            StateTB.Text = currentPatient.state;
            ZipCodeTB.Text = currentPatient.zipCode;

            DatesOfStayTB.Text = findDatesInOut();

            stayTime = findStayTime();
            TotalHoursTB.Text =  stayTime + "  Hours";


        }

        private String findDatesInOut()
        {
            String dateString = "";

            //To be re-evaluated when I discover how we store default empty values in the entryDate exitDate   -jjl
            //For more on the matter of date times visit https://msdn.microsoft.com/en-us/library/system.datetime%28v=vs.110%29.aspx
            

            if (currentRecord.entryDate != null)
            {
                timeIn = currentRecord.entryDate;
            }

            else
            {
                timeIn = DateTime.Today;
            }

            if(currentRecord.exitDate != null)
            {
                timeOut = (DateTime)currentRecord.exitDate;
            }

            else
            {
                timeOut = DateTime.Today;            
            }

            dateString = timeIn.ToShortDateString() + " -- " + timeOut.ToShortDateString();

            return dateString;
        }


        private int findStayTime()  //This way we only need to change this method if we change teh time spans we charge for
        {
            int dayCount = 0;
            TimeSpan stayDuration;
            //For more info on timeSpans visit https://msdn.microsoft.com/en-us/library/system.timespan%28v=vs.110%29.aspx

            stayDuration = timeOut.Subtract(timeIn);

            dayCount = stayDuration.Days;

            if(stayDuration.Hours > 0 || stayDuration.Minutes > 0 )
            {
                dayCount++;
            }

            return dayCount;
        }


        private void LogoutButton_Click(object sender, EventArgs e)
        {
            //navigates to HRAS_Login_View

            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void NameTB_TextChanged(object sender, EventArgs e)
        {
        }

        private void HRAS_Patient_Bill_View_Load(object sender, EventArgs e)
        {
        }

        private void AddressTB_Click(object sender, EventArgs e)
        {
        }

        private void ToHomePageLB_Click(object sender, EventArgs e)
        {
        }

        private void zipCode_Click(object sender, EventArgs e)
        {

        }
    }
}