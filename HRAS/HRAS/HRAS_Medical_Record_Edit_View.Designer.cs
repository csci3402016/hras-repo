﻿namespace HRAS
{
    partial class HRAS_Medical_Record_Edit_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MedicalRecordEditLB = new System.Windows.Forms.Label();
            this.logout = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.notesLB = new System.Windows.Forms.Label();
            this.diagnosisLB = new System.Windows.Forms.Label();
            this.entryDateLB = new System.Windows.Forms.Label();
            this.exitDateLB = new System.Windows.Forms.Label();
            this.roomNumberLB = new System.Windows.Forms.Label();
            this.physicianLB = new System.Windows.Forms.Label();
            this.insurerLB = new System.Windows.Forms.Label();
            this.dnrLB = new System.Windows.Forms.Label();
            this.dnr = new System.Windows.Forms.TextBox();
            this.insurer = new System.Windows.Forms.TextBox();
            this.physician = new System.Windows.Forms.TextBox();
            this.roomNumber = new System.Windows.Forms.TextBox();
            this.exitDate = new System.Windows.Forms.TextBox();
            this.entryDate = new System.Windows.Forms.TextBox();
            this.firstNameTitle = new System.Windows.Forms.Label();
            this.lastNameTitle = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.Label();
            this.firstName = new System.Windows.Forms.Label();
            this.view = new System.Windows.Forms.Button();
            this.viewLB = new System.Windows.Forms.Label();
            this.chosenSymptomsLB = new System.Windows.Forms.Label();
            this.remove = new System.Windows.Forms.Button();
            this.checkedList = new System.Windows.Forms.CheckedListBox();
            this.combo = new System.Windows.Forms.ComboBox();
            this.symptomsLB = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ssn = new System.Windows.Forms.Label();
            this.diagnosis = new System.Windows.Forms.RichTextBox();
            this.notes = new System.Windows.Forms.RichTextBox();
            this.exitDateError = new System.Windows.Forms.Label();
            this.dnrError = new System.Windows.Forms.Label();
            this.insurerError = new System.Windows.Forms.Label();
            this.physicianError = new System.Windows.Forms.Label();
            this.roomNumberError = new System.Windows.Forms.Label();
            this.diagnosisError = new System.Windows.Forms.Label();
            this.notesError = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.symptomsMessage = new System.Windows.Forms.Label();
            this.dateComparisonError = new System.Windows.Forms.Label();
            this.HomeLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MedicalRecordEditLB
            // 
            this.MedicalRecordEditLB.AutoSize = true;
            this.MedicalRecordEditLB.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MedicalRecordEditLB.Location = new System.Drawing.Point(12, 90);
            this.MedicalRecordEditLB.Name = "MedicalRecordEditLB";
            this.MedicalRecordEditLB.Size = new System.Drawing.Size(318, 38);
            this.MedicalRecordEditLB.TabIndex = 96;
            this.MedicalRecordEditLB.Text = "Medical Record Edit:";
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(896, 34);
            this.logout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(105, 35);
            this.logout.TabIndex = 95;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(933, 674);
            this.Back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(126, 23);
            this.Back.TabIndex = 94;
            this.Back.Text = "Home Screen";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.home_Click);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(463, 674);
            this.save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(103, 23);
            this.save.TabIndex = 92;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // notesLB
            // 
            this.notesLB.AutoSize = true;
            this.notesLB.Location = new System.Drawing.Point(493, 484);
            this.notesLB.Name = "notesLB";
            this.notesLB.Size = new System.Drawing.Size(112, 17);
            this.notesLB.TabIndex = 90;
            this.notesLB.Text = "Notes (Optional)";
            // 
            // diagnosisLB
            // 
            this.diagnosisLB.AutoSize = true;
            this.diagnosisLB.Location = new System.Drawing.Point(493, 329);
            this.diagnosisLB.Name = "diagnosisLB";
            this.diagnosisLB.Size = new System.Drawing.Size(137, 17);
            this.diagnosisLB.TabIndex = 89;
            this.diagnosisLB.Text = "Diagnosis (Optional)";
            this.diagnosisLB.Click += new System.EventHandler(this.diagnosisLB_Click);
            // 
            // entryDateLB
            // 
            this.entryDateLB.AutoSize = true;
            this.entryDateLB.Location = new System.Drawing.Point(56, 306);
            this.entryDateLB.Name = "entryDateLB";
            this.entryDateLB.Size = new System.Drawing.Size(75, 17);
            this.entryDateLB.TabIndex = 85;
            this.entryDateLB.Text = "Entry Date";
            // 
            // exitDateLB
            // 
            this.exitDateLB.AutoSize = true;
            this.exitDateLB.Location = new System.Drawing.Point(56, 374);
            this.exitDateLB.Name = "exitDateLB";
            this.exitDateLB.Size = new System.Drawing.Size(148, 17);
            this.exitDateLB.TabIndex = 84;
            this.exitDateLB.Text = "Exit Date(mm/dd/yyyy)";
            // 
            // roomNumberLB
            // 
            this.roomNumberLB.AutoSize = true;
            this.roomNumberLB.Location = new System.Drawing.Point(53, 433);
            this.roomNumberLB.Name = "roomNumberLB";
            this.roomNumberLB.Size = new System.Drawing.Size(99, 17);
            this.roomNumberLB.TabIndex = 83;
            this.roomNumberLB.Text = "Room Number";
            // 
            // physicianLB
            // 
            this.physicianLB.AutoSize = true;
            this.physicianLB.Location = new System.Drawing.Point(53, 493);
            this.physicianLB.Name = "physicianLB";
            this.physicianLB.Size = new System.Drawing.Size(68, 17);
            this.physicianLB.TabIndex = 82;
            this.physicianLB.Text = "Physician";
            // 
            // insurerLB
            // 
            this.insurerLB.AutoSize = true;
            this.insurerLB.Location = new System.Drawing.Point(56, 559);
            this.insurerLB.Name = "insurerLB";
            this.insurerLB.Size = new System.Drawing.Size(119, 17);
            this.insurerLB.TabIndex = 81;
            this.insurerLB.Text = "(Optional) Insurer";
            // 
            // dnrLB
            // 
            this.dnrLB.AutoSize = true;
            this.dnrLB.Location = new System.Drawing.Point(53, 624);
            this.dnrLB.Name = "dnrLB";
            this.dnrLB.Size = new System.Drawing.Size(71, 17);
            this.dnrLB.TabIndex = 78;
            this.dnrLB.Text = "DNR(Y/N)";
            // 
            // dnr
            // 
            this.dnr.Location = new System.Drawing.Point(55, 643);
            this.dnr.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dnr.Name = "dnr";
            this.dnr.Size = new System.Drawing.Size(59, 22);
            this.dnr.TabIndex = 66;
            // 
            // insurer
            // 
            this.insurer.Location = new System.Drawing.Point(55, 578);
            this.insurer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.insurer.Name = "insurer";
            this.insurer.Size = new System.Drawing.Size(149, 22);
            this.insurer.TabIndex = 61;
            // 
            // physician
            // 
            this.physician.Location = new System.Drawing.Point(55, 512);
            this.physician.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.physician.Name = "physician";
            this.physician.Size = new System.Drawing.Size(149, 22);
            this.physician.TabIndex = 60;
            // 
            // roomNumber
            // 
            this.roomNumber.Location = new System.Drawing.Point(56, 452);
            this.roomNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.roomNumber.Name = "roomNumber";
            this.roomNumber.Size = new System.Drawing.Size(148, 22);
            this.roomNumber.TabIndex = 59;
            // 
            // exitDate
            // 
            this.exitDate.Location = new System.Drawing.Point(56, 393);
            this.exitDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exitDate.Name = "exitDate";
            this.exitDate.Size = new System.Drawing.Size(148, 22);
            this.exitDate.TabIndex = 58;
            // 
            // entryDate
            // 
            this.entryDate.Location = new System.Drawing.Point(56, 329);
            this.entryDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.entryDate.Name = "entryDate";
            this.entryDate.Size = new System.Drawing.Size(124, 22);
            this.entryDate.TabIndex = 57;
            // 
            // firstNameTitle
            // 
            this.firstNameTitle.AutoSize = true;
            this.firstNameTitle.Location = new System.Drawing.Point(65, 150);
            this.firstNameTitle.Name = "firstNameTitle";
            this.firstNameTitle.Size = new System.Drawing.Size(80, 17);
            this.firstNameTitle.TabIndex = 102;
            this.firstNameTitle.Text = "First Name:";
            // 
            // lastNameTitle
            // 
            this.lastNameTitle.AutoSize = true;
            this.lastNameTitle.Location = new System.Drawing.Point(65, 184);
            this.lastNameTitle.Name = "lastNameTitle";
            this.lastNameTitle.Size = new System.Drawing.Size(80, 17);
            this.lastNameTitle.TabIndex = 101;
            this.lastNameTitle.Text = "Last Name:";
            this.lastNameTitle.Click += new System.EventHandler(this.lastNameTitle_Click);
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(178, 184);
            this.lastName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(124, 22);
            this.lastName.TabIndex = 100;
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(178, 150);
            this.firstName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(124, 22);
            this.firstName.TabIndex = 99;
            // 
            // view
            // 
            this.view.Location = new System.Drawing.Point(622, 674);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(114, 23);
            this.view.TabIndex = 103;
            this.view.Text = "View";
            this.view.UseVisualStyleBackColor = true;
            this.view.Click += new System.EventHandler(this.view_Click);
            // 
            // viewLB
            // 
            this.viewLB.AutoSize = true;
            this.viewLB.Location = new System.Drawing.Point(593, 643);
            this.viewLB.Name = "viewLB";
            this.viewLB.Size = new System.Drawing.Size(207, 17);
            this.viewLB.TabIndex = 104;
            this.viewLB.Text = "Must Save Medical Record First";
            // 
            // chosenSymptomsLB
            // 
            this.chosenSymptomsLB.AutoSize = true;
            this.chosenSymptomsLB.Location = new System.Drawing.Point(848, 133);
            this.chosenSymptomsLB.Name = "chosenSymptomsLB";
            this.chosenSymptomsLB.Size = new System.Drawing.Size(125, 17);
            this.chosenSymptomsLB.TabIndex = 115;
            this.chosenSymptomsLB.Text = "Chosen Symptoms";
            // 
            // remove
            // 
            this.remove.Location = new System.Drawing.Point(836, 292);
            this.remove.Name = "remove";
            this.remove.Size = new System.Drawing.Size(75, 23);
            this.remove.TabIndex = 114;
            this.remove.Text = "Remove";
            this.remove.UseVisualStyleBackColor = true;
            this.remove.Click += new System.EventHandler(this.remove_Click);
            // 
            // checkedList
            // 
            this.checkedList.FormattingEnabled = true;
            this.checkedList.Location = new System.Drawing.Point(836, 163);
            this.checkedList.Name = "checkedList";
            this.checkedList.Size = new System.Drawing.Size(223, 123);
            this.checkedList.Sorted = true;
            this.checkedList.TabIndex = 113;
            // 
            // combo
            // 
            this.combo.FormattingEnabled = true;
            this.combo.Location = new System.Drawing.Point(496, 163);
            this.combo.Name = "combo";
            this.combo.Size = new System.Drawing.Size(266, 24);
            this.combo.Sorted = true;
            this.combo.TabIndex = 112;
            this.combo.Text = "Select Symptoms";
            this.combo.SelectedIndexChanged += new System.EventHandler(this.combo_SelectedIndexChanged);
            // 
            // symptomsLB
            // 
            this.symptomsLB.AutoSize = true;
            this.symptomsLB.Location = new System.Drawing.Point(493, 143);
            this.symptomsLB.Name = "symptomsLB";
            this.symptomsLB.Size = new System.Drawing.Size(140, 17);
            this.symptomsLB.TabIndex = 111;
            this.symptomsLB.Text = "Symptoms (Optional)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 225);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 116;
            this.label1.Text = "SSN:";
            // 
            // ssn
            // 
            this.ssn.AutoSize = true;
            this.ssn.Location = new System.Drawing.Point(178, 225);
            this.ssn.Name = "ssn";
            this.ssn.Size = new System.Drawing.Size(0, 17);
            this.ssn.TabIndex = 117;
            // 
            // diagnosis
            // 
            this.diagnosis.Location = new System.Drawing.Point(496, 354);
            this.diagnosis.Name = "diagnosis";
            this.diagnosis.Size = new System.Drawing.Size(266, 96);
            this.diagnosis.TabIndex = 118;
            this.diagnosis.Text = "";
            // 
            // notes
            // 
            this.notes.Location = new System.Drawing.Point(496, 504);
            this.notes.Name = "notes";
            this.notes.Size = new System.Drawing.Size(266, 110);
            this.notes.TabIndex = 119;
            this.notes.Text = "";
            this.notes.TextChanged += new System.EventHandler(this.notes_TextChanged);
            // 
            // exitDateError
            // 
            this.exitDateError.AutoSize = true;
            this.exitDateError.ForeColor = System.Drawing.Color.Red;
            this.exitDateError.Location = new System.Drawing.Point(218, 374);
            this.exitDateError.Name = "exitDateError";
            this.exitDateError.Size = new System.Drawing.Size(0, 17);
            this.exitDateError.TabIndex = 121;
            // 
            // dnrError
            // 
            this.dnrError.AutoSize = true;
            this.dnrError.ForeColor = System.Drawing.Color.Red;
            this.dnrError.Location = new System.Drawing.Point(139, 624);
            this.dnrError.Name = "dnrError";
            this.dnrError.Size = new System.Drawing.Size(0, 17);
            this.dnrError.TabIndex = 122;
            // 
            // insurerError
            // 
            this.insurerError.AutoSize = true;
            this.insurerError.ForeColor = System.Drawing.Color.Red;
            this.insurerError.Location = new System.Drawing.Point(191, 559);
            this.insurerError.Name = "insurerError";
            this.insurerError.Size = new System.Drawing.Size(0, 17);
            this.insurerError.TabIndex = 123;
            // 
            // physicianError
            // 
            this.physicianError.AutoSize = true;
            this.physicianError.ForeColor = System.Drawing.Color.Red;
            this.physicianError.Location = new System.Drawing.Point(127, 493);
            this.physicianError.Name = "physicianError";
            this.physicianError.Size = new System.Drawing.Size(0, 17);
            this.physicianError.TabIndex = 124;
            // 
            // roomNumberError
            // 
            this.roomNumberError.AutoSize = true;
            this.roomNumberError.ForeColor = System.Drawing.Color.Red;
            this.roomNumberError.Location = new System.Drawing.Point(158, 433);
            this.roomNumberError.Name = "roomNumberError";
            this.roomNumberError.Size = new System.Drawing.Size(0, 17);
            this.roomNumberError.TabIndex = 125;
            // 
            // diagnosisError
            // 
            this.diagnosisError.AutoSize = true;
            this.diagnosisError.ForeColor = System.Drawing.Color.Red;
            this.diagnosisError.Location = new System.Drawing.Point(647, 329);
            this.diagnosisError.Name = "diagnosisError";
            this.diagnosisError.Size = new System.Drawing.Size(0, 17);
            this.diagnosisError.TabIndex = 126;
            // 
            // notesError
            // 
            this.notesError.AutoSize = true;
            this.notesError.ForeColor = System.Drawing.Color.Red;
            this.notesError.Location = new System.Drawing.Point(619, 484);
            this.notesError.Name = "notesError";
            this.notesError.Size = new System.Drawing.Size(0, 17);
            this.notesError.TabIndex = 127;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F);
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(114, 271);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(0, 26);
            this.message.TabIndex = 128;
            // 
            // symptomsMessage
            // 
            this.symptomsMessage.AutoSize = true;
            this.symptomsMessage.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.symptomsMessage.Location = new System.Drawing.Point(493, 126);
            this.symptomsMessage.Name = "symptomsMessage";
            this.symptomsMessage.Size = new System.Drawing.Size(0, 17);
            this.symptomsMessage.TabIndex = 129;
            // 
            // dateComparisonError
            // 
            this.dateComparisonError.AutoSize = true;
            this.dateComparisonError.ForeColor = System.Drawing.Color.Red;
            this.dateComparisonError.Location = new System.Drawing.Point(210, 374);
            this.dateComparisonError.Name = "dateComparisonError";
            this.dateComparisonError.Size = new System.Drawing.Size(0, 17);
            this.dateComparisonError.TabIndex = 130;
            // 
            // HomeLabel
            // 
            this.HomeLabel.AutoSize = true;
            this.HomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomeLabel.Location = new System.Drawing.Point(372, 9);
            this.HomeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HomeLabel.Name = "HomeLabel";
            this.HomeLabel.Size = new System.Drawing.Size(261, 91);
            this.HomeLabel.TabIndex = 131;
            this.HomeLabel.Text = "HRAS";
            this.HomeLabel.Click += new System.EventHandler(this.HomeLabel_Click);
            // 
            // HRAS_Medical_Record_Edit_View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 724);
            this.Controls.Add(this.HomeLabel);
            this.Controls.Add(this.dateComparisonError);
            this.Controls.Add(this.symptomsMessage);
            this.Controls.Add(this.message);
            this.Controls.Add(this.notesError);
            this.Controls.Add(this.diagnosisError);
            this.Controls.Add(this.roomNumberError);
            this.Controls.Add(this.physicianError);
            this.Controls.Add(this.insurerError);
            this.Controls.Add(this.dnrError);
            this.Controls.Add(this.exitDateError);
            this.Controls.Add(this.notes);
            this.Controls.Add(this.diagnosis);
            this.Controls.Add(this.ssn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chosenSymptomsLB);
            this.Controls.Add(this.remove);
            this.Controls.Add(this.checkedList);
            this.Controls.Add(this.combo);
            this.Controls.Add(this.symptomsLB);
            this.Controls.Add(this.viewLB);
            this.Controls.Add(this.view);
            this.Controls.Add(this.firstNameTitle);
            this.Controls.Add(this.lastNameTitle);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.MedicalRecordEditLB);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.save);
            this.Controls.Add(this.notesLB);
            this.Controls.Add(this.diagnosisLB);
            this.Controls.Add(this.entryDateLB);
            this.Controls.Add(this.exitDateLB);
            this.Controls.Add(this.roomNumberLB);
            this.Controls.Add(this.physicianLB);
            this.Controls.Add(this.insurerLB);
            this.Controls.Add(this.dnrLB);
            this.Controls.Add(this.dnr);
            this.Controls.Add(this.insurer);
            this.Controls.Add(this.physician);
            this.Controls.Add(this.roomNumber);
            this.Controls.Add(this.exitDate);
            this.Controls.Add(this.entryDate);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "HRAS_Medical_Record_Edit_View";
            this.Text = "Medical Record Edit";
            this.Load += new System.EventHandler(this.HRAS_Patient_View_View_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(HMI_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MedicalRecordEditLB;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label notesLB;
        private System.Windows.Forms.Label diagnosisLB;
        private System.Windows.Forms.Label entryDateLB;
        private System.Windows.Forms.Label exitDateLB;
        private System.Windows.Forms.Label roomNumberLB;
        private System.Windows.Forms.Label physicianLB;
        private System.Windows.Forms.Label insurerLB;
        private System.Windows.Forms.Label dnrLB;
        private System.Windows.Forms.TextBox dnr;
        private System.Windows.Forms.TextBox insurer;
        private System.Windows.Forms.TextBox physician;
        private System.Windows.Forms.TextBox roomNumber;
        private System.Windows.Forms.TextBox exitDate;
        private System.Windows.Forms.TextBox entryDate;
        private System.Windows.Forms.Label firstNameTitle;
        private System.Windows.Forms.Label lastNameTitle;
        private System.Windows.Forms.Label lastName;
        private System.Windows.Forms.Label firstName;
        private System.Windows.Forms.Button view;
        private System.Windows.Forms.Label viewLB;
        private System.Windows.Forms.Label chosenSymptomsLB;
        private System.Windows.Forms.Button remove;
        private System.Windows.Forms.CheckedListBox checkedList;
        private System.Windows.Forms.ComboBox combo;
        private System.Windows.Forms.Label symptomsLB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ssn;
        private System.Windows.Forms.RichTextBox diagnosis;
        private System.Windows.Forms.RichTextBox notes;
        private System.Windows.Forms.Label exitDateError;
        private System.Windows.Forms.Label dnrError;
        private System.Windows.Forms.Label insurerError;
        private System.Windows.Forms.Label physicianError;
        private System.Windows.Forms.Label roomNumberError;
        private System.Windows.Forms.Label diagnosisError;
        private System.Windows.Forms.Label notesError;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label symptomsMessage;
        private System.Windows.Forms.Label dateComparisonError;
        private System.Windows.Forms.Label HomeLabel;
    }
}