﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using HRAS_Middleware;
using System.Collections.Generic;

namespace HRAS
{
    public partial class HRAS_Diagnosis_Wizard_View : Form
    {
        List<Diagnosis> list;
        DiagnosisWizardHandler con;
        Symptom s;
        public HRAS_Diagnosis_Wizard_View()
        {
            InitializeComponent();
            con = new DiagnosisWizardHandler();
            list = new List<Diagnosis>();
            s = new Symptom();
            try
            {
                con.deleteSymptoms(HRAS_Login_View.getUser());
                symptomLabel.Text = con.getSymptom(HRAS_Login_View.getUser());
                s.name = symptomLabel.Text;
            }
            catch(Exception a) { dBProblem.Visible = true; }
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            deleteSymptoms();
            Hide();
            HRAS_Home_View home = new HRAS_Home_View();
            home.Closed += (s, args) => Close();
            home.Show();
        }

        private void Logout_Button_Click(object sender, EventArgs e)
        {
            deleteSymptoms();
            Hide();
            HRAS_Login_View loginWindow = new HRAS_Login_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void deleteSymptoms()
        {
            try { con.deleteSymptoms(HRAS_Login_View.getUser()); }
            catch (SqlException a) { dBProblem.Visible = true; }
        }
        private void QuestionToAskTB_TextChanged(object sender, EventArgs e)
        {
        }

        private void ToHomePageLB_Click(object sender, EventArgs e)
        {
        }

        private void YesRButton_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void NoRButton_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void Next_Button_Click(object sender, EventArgs e)
        {
            if (noDataLabel.Visible) return;
            dBProblem.Visible = false;
            bool symptomCheck = false;
            bool buttonCheck = false;

            if (YesRButton.Checked) symptomCheck = true;
            if (YesRButton.Checked || NoRButton.Checked) buttonCheck = true;

            if(buttonCheck)
            {
                try
                {
                    con.insertSymptom(HRAS_Login_View.getUser(), s, symptomCheck);
                    list = con.getDiagnoses(HRAS_Login_View.getUser());
                    displayDiagnoses(list);
                    symptomLabel.Text = con.getSymptom(HRAS_Login_View.getUser());
                    s.name = symptomLabel.Text;
                }
                catch (SqlException a) { dBProblem.Visible = true; }
                catch (Exception a) { noDataLabel.Visible = true; }
            }   
        }

        private void displayDiagnoses(List<Diagnosis> list)
        {
            diagnosesListBox.Items.Clear();
            String temp;
            for(int i = 0; i < list.Count; i++)
            {
                temp = "";
                temp += list[i].name;
                temp += " " + (Math.Round((list[i].probability * 100), 3)) + "%";
                diagnosesListBox.Items.Add(temp);
            }
        }
        private void homeScreen_Click(object sender, EventArgs e)
        {
            Hide();
            HRAS_Home_View loginWindow = new HRAS_Home_View();
            loginWindow.Closed += (s, args) => Close();
            loginWindow.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void HRAS_Diagnosis_Wizard_View_Load(object sender, EventArgs e)
        {

        }

     
    }
}