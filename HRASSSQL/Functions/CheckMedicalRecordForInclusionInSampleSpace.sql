USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 04/04/2016
-- Description:	Returns 0 if Medical Record should be included in the sample space
--				Returns a value > 0 if Medical Record should not be included in the sample space
-- =============================================
CREATE FUNCTION CheckMedicalRecordForInclusionInSampleSpace 
(
	-- Add the parameters for the function here
	@Username					varchar(15)		= NULL,
	@Patient_SSN				char(9)			= NULL,
	@Entry_Date					datetime		= NULL
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = COUNT(*) FROM HRAS_User_Symptom AS B
	WHERE B.User_Username = @Username
	AND ((SELECT COUNT(*) FROM Medical_Record_Symptom AS C
			WHERE @Patient_SSN = C.Med_Rec_Patient_SSN AND @Entry_Date = C.Med_Rec_Entry_Date
			AND B.Symptom_Name = C.Symptom_Name) 
			!= Has_Symptom) 

	-- Return the result of the function
	RETURN @Result

END
GO

