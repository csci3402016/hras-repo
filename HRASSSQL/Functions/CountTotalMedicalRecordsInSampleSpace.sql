USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 04/04/2016
-- Description:	
-- =============================================
CREATE FUNCTION CountTotalMedicalRecordsInSampleSpace 
(
	-- Add the parameters for the function here
	@Username					varchar(15)		= NULL
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = COUNT(*) FROM Medical_Record AS A
	WHERE dbo.CheckMedicalRecordForInclusionInSampleSpace(@Username, Patient_SSN, Entry_Date) = 0

	-- Return the result of the function
	RETURN @Result

END
GO

