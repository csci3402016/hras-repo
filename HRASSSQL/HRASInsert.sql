USE HRAS

INSERT INTO Patient VALUES ('123456789', 'Nikola', 'Kuzmanovski', 'A', 1, 1, '1995/10/15', '1600 Catlin Avenue', 'Ognjan Prica 33/14', 'Superior', 'WI', '54880')
INSERT INTO Patient VALUES ('123456788', 'King', 'TOAST', 'K', 1, 1, '1993/11/12', 'Toast Street', 'Secret Toast Street', 'Duluth', 'MN', '54888')
INSERT INTO Patient VALUES ('123456787', 'Stephen', 'Gant', 'S', 1, 1, '1993/11/12', 'Stephen Street', 'Secret Stephen Street', 'Duluth', 'MN', '54888')
INSERT INTO Patient VALUES ('123456786', 'Josh', 'Letter', 'J', 1, 1, '1993/11/11', 'Josh Street', 'Secret Josh Street', 'Superior', 'WI', '54880')
INSERT INTO Patient VALUES ('123456785', 'Pavle', 'Bulatovic', 'P', 1, 1, '1995/11/16', 'Pavle Street', 'Secret Pavle Street', 'Oakdale', 'MN', '54888')

INSERT INTO Room VALUES ('123')
INSERT INTO Room VALUES ('124')
INSERT INTO Room VALUES ('125')
INSERT INTO Room VALUES ('126')
INSERT INTO Room VALUES ('127')
INSERT INTO Room VALUES ('128')
INSERT INTO Room VALUES ('223')
INSERT INTO Room VALUES ('323')

INSERT INTO HRAS_User VALUES ('Prof_Rosenberg', 'CSCI3402016Spring', 1)
INSERT INTO HRAS_User VALUES ('Doc_Rosenberg', 'GotMyPHD', 0)
INSERT INTO HRAS_User VALUES ('Rosenberg', 'JustCollege', 0)

INSERT INTO Inventory_Item VALUES ('1', 5, 'Gives Student A', 500, 99999, 0)
INSERT INTO Inventory_Item VALUES ('2', 10, 'Gives Student B', 400, 9999, 0)
INSERT INTO Inventory_Item VALUES ('3', 15, 'Gives Student C', 300, 999, 0)
INSERT INTO Inventory_Item VALUES ('4', 10, 'Gives Student D', 200, 99, 0)
INSERT INTO Inventory_Item VALUES ('5', 5, 'Gives Student F', 100, 9, 0)

INSERT INTO Symptom VALUES ('Plain lazy')
INSERT INTO Symptom VALUES ('Just lost')
INSERT INTO Symptom VALUES ('Hardworking')
INSERT INTO Symptom VALUES ('Does not understand stuff')
INSERT INTO Symptom VALUES ('Bugs me too much')
INSERT INTO Symptom VALUES ('Likes bacon')
INSERT INTO Symptom VALUES ('Legendary slacker')

INSERT INTO Medical_Record VALUES ('123456789', '2004-05-23T14:25:10', '123', '2004-05-23T14:27:10', 'Professor Doctor Rosenberg', 'This person is beyond help', 'LEGENDARY', 'Overpaid Tool', 1)
INSERT INTO Medical_Record VALUES ('123456789', '2004-05-23T14:26:10', '123', '2004-05-23T15:26:10', 'Professor Doctor Rosenberg', 'This person is beyond help', 'LEGENDARY', 'Overpaid Tool', 1)
INSERT INTO Medical_Record VALUES ('123456789', '2004-05-23T14:27:10', '124', NULL, 'Professor Doctor Rosenberg', 'This person is beyond help', 'LEGENDARY', 'Extremely Overpaid Tool', 0)  
INSERT INTO Medical_Record VALUES ('123456788', '2004-05-10T14:24:10', '125', NULL, 'Doctor Blabla', 'Diagnosis1', 'Give more paint killer', 'Overpaid Tool1', 0)
INSERT INTO Medical_Record VALUES ('123456786', '2004-05-23T14:32:10', '126', '2005-05-23T14:26:10', 'Doctor Hehe', 'Diagnosis1', 'Give more morphine', 'Overpaid Tool123', 1)
INSERT INTO Medical_Record VALUES ('123456785', '2004-05-23T14:12:10', '323', NULL, 'Doctor MEH', NULL, NULL, NULL, 0)

INSERT INTO Audit_Medical_Record VALUES ('123456785', '2004-05-23T14:12:10', '2015-05-23T14:12:10', 'Prof_Rosenberg', 'Notes', 'legedary', 'LEGENDARY')

INSERT INTO Medical_Record_Lock VALUES ('123456785', '2004-05-23T14:12:10', 'Doc_Rosenberg', '2004-05-23T14:12:10')  

INSERT INTO Inventory_Usage VALUES ('123456789', '2004-05-23T14:27:10', '5', '2015-05-23T14:27:10', 'Prof_Rosenberg', 99999)

INSERT INTO Room_Rate VALUES ('2004-05-23T14:00:00', '123', 80)