You need to have sql server management studio or similar to create your DB.
1.In Object explorer find your server.
2.Maximize your server and find Databases.
3.Right click on Databases and click on New Database.
4.Type in name HRAS for your database name and click ok.
5.Open HRASTableCreation.sql in SQL Server Management Studio and click !Execute.
6.Open all files in StoredProcedures folder in SQL Server Management Studio and !Execute each one.
7.Follow direction from storedProceduresReedme to connect to your DB.
8.Copy all files in DataDump folder to HRAS\HRAS_DataImport\bin\Debug.
9.Run DataImport to populate the DB.
