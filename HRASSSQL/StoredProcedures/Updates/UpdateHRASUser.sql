USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/17/2016
-- Description:	Update User
-- =============================================
CREATE PROCEDURE UpdateHRASUser 
	-- Add the parameters for the stored procedure here
	@Username						varchar(15)		= NULL,
	@NewPassword					varchar(35)		= NULL,
	@NewIs_Inventory_Administator	bit				= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE HRAS_User
	SET Password = @NewPassword, Is_Inventory_Administator= @NewIs_Inventory_Administator
	WHERE Username = @Username
END
GO
