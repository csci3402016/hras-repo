USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/17/2016
-- Description:	Update Inventory Item
-- =============================================
CREATE PROCEDURE UpdateInventoryItem 
	-- Add the parameters for the stored procedure here
	@Stock_ID					varchar(5)		= NULL,
	@NewQuantity				int				= NULL,
	@NewDescription				varchar(100)	= NULL,
	@NewSize					real			= NULL,
	@NewCost					real			= NULL,
	@NewIs_Virtual				bit				= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Inventory_Item 
	SET Quantity = @NewQuantity, Description = @NewDescription, Size = @NewSize, Cost = @NewCost, Is_Virtual = @NewIs_Virtual
	WHERE Stock_ID = @Stock_ID
END
GO
