USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/17/2016
-- Description:	Update Patient
-- =============================================
CREATE PROCEDURE UpdatePatient 
	-- Add the parameters for the stored procedure here
	@SSN						char(9)			 = NULL,
	@NewFirst_Name				varchar(50)		 = NULL,
	@NewLast_Name				varchar(25)		 = NULL,
	@NewMiddle_Initial			char(1)			 = NULL,
	@NewGender					bit				 = NULL,
	@NewOrgan_Donor				bit				 = NULL,
	@NewBirth_Date				date			 = NULL,
	@NewAddress_Line_1			varchar(35)		 = NULL,
	@NewAddress_Line_2			varchar(35)		 = NULL,
	@NewAddress_City			varchar(25)		 = NULL,
	@NewAddress_State			char(2)			 = NULL,
	@NewAddress_Zip_Code		char(5)			 = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Patient
	SET First_Name = @NewFirst_Name, Last_Name = @NewLast_Name, Middle_Initial = @NewMiddle_Initial, Gender = @NewGender, Organ_Donor = @NewOrgan_Donor, Birth_Date = @NewBirth_Date, Address_Line_1 = @NewAddress_Line_1, Address_Line_2 = @NewAddress_Line_2, Address_City = @NewAddress_City, Address_State = @NewAddress_State, Address_Zip_Code = @NewAddress_Zip_Code
	WHERE SSN = @SSN
END
GO
