USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kumzanovski
-- Create date: 3/17/2016
-- Description:	Update Medical Record
-- =============================================
CREATE PROCEDURE UpdateMedicalRecord 
	-- Add the parameters for the stored procedure here

	@Patient_SSN					char(9)			 = NULL,
	@Entry_Date						datetime		 = NULL,
	@NewExit_Date					datetime		 = NULL,
	@NewAttending_Physician			varchar(100)	 = NULL,
	@NewDiagnosis					varchar(200)	 = NULL,
	@NewNotes						varchar(8000)	 = NULL,
	@NewInsurer						varchar(50)		 = NULL,
	@DNR							bit              = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Medical_Record
	SET Exit_Date = @NewExit_Date, Attending_Physician = @NewAttending_Physician, Diagnosis = @NewDiagnosis, Notes = @NewNotes, Insurer = @NewInsurer ,DNR = @DNR
	WHERE Patient_SSN = @Patient_SSN AND Entry_Date = @Entry_Date 
END
GO
