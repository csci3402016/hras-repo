To learn how to connect to a DB just follow the steps here https://msdn.microsoft.com/en-us/library/bb655884(v=vs.90).aspx
Stored procedures are SQL queries stored on the DB that you can call in your C# code to communicate with the DB

If you are executing a stored procedure that just reads data look at this https://msdn.microsoft.com/en-us/library/d7125bke.aspx
It is the same as what we did in class. You just call the query that is already stored in the DB.
Look at the code on DTL from Rosenberg to understand how this would work.
Stick to the Microsoft page for connecting to the DB. It is very clear. 


If you are storing something on the DB. Like inserting a new row in a table follow the example below.
The stored procedure below is called InsertRoom. It has one parameter @Room_Number of type varchar(5).

	    SqlConnection con = new SqlConnection(Properties.Settings.Default.HRASConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "InsertRoom";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@RoomNumber", SqlDbType.VarChar, 5).Value = "333";

            con.Open();
            cmd.ExecuteNonQuery();
            

            con.Close();
			
To read about the parameters a stored procedure accepts just open the .sql file named after it.
You will find everything about it there.

Thought this might be useful as well. You don't have to specify the length. Just name and type and then insert value.
DateTime in C#: https://msdn.microsoft.com/en-us/library/system.datetime(v=vs.110).aspx
            
			SqlConnection con = new SqlConnection(Properties.Settings.Default.HRASConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UpdatePatient";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@SSN", SqlDbType.Char).Value = "123456789";
            cmd.Parameters.Add("@NewFirst_Name", SqlDbType.VarChar).Value = "Nikola";
            cmd.Parameters.Add("@NewLast_Name", SqlDbType.VarChar).Value = "Kuzmanovski";
            cmd.Parameters.Add("@NewMiddle_Initial", SqlDbType.Char).Value = "B";
            cmd.Parameters.Add("@NewGender", SqlDbType.Bit).Value = 1;
            cmd.Parameters.Add("@NewOrgan_Donor", SqlDbType.Bit).Value = 1;
            cmd.Parameters.Add("@NewBirth_Date", SqlDbType.Date).Value = new DateTime (1995, 12, 12);
            cmd.Parameters.Add("@NewAddress_Line_1", SqlDbType.VarChar).Value = "Blob";
            cmd.Parameters.Add("@NewAddress_Line_2", SqlDbType.VarChar).Value = "Blob2";
            cmd.Parameters.Add("@NewAddress_City", SqlDbType.VarChar).Value = "Blob3";
            cmd.Parameters.Add("@NewAddress_State", SqlDbType.Char).Value = "MN";
            cmd.Parameters.Add("@NewAddress_Zip_Code", SqlDbType.Char).Value = "12345";

            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            con.Close();
            con.Close();
			
If you want to make a parameter NULL then just don't pass it like in the next example

			SqlConnection con = new SqlConnection(Properties.Settings.Default.HRASConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "UpdateMedicalRecord";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@Patient_SSN", SqlDbType.Char).Value = "123456786";
            cmd.Parameters.Add("@Entry_Date", SqlDbType.DateTime).Value = new DateTime(2004, 05, 23, 14, 32, 10);
            cmd.Parameters.Add("@NewAttending_Physician", SqlDbType.VarChar).Value = "Nikola";
            cmd.Parameters.Add("@NewNotes", SqlDbType.VarChar).Value = "hahahahahahah";
            cmd.Parameters.Add("@NewDiagnosis", SqlDbType.VarChar).Value = "prc";
            cmd.Parameters.Add("@NewInsurer", SqlDbType.VarChar).Value = "Tool";
			//Exit_Date is not passed it will default to NULL.
			// All parameters default to NULL if you don't pass anything.

            con.Open();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch(SqlException a)
            {
            } //Put breakpoint here. If your code breaks look at a in the debug mode to find the error it is giving you. This way you can know if it is your code or the DB.
            con.Close();
            con.Close();
			
Reading from the DB example
			SqlConnection con = new SqlConnection(Properties.Settings.Default.HRASConnectionString);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            String x = "Rosenberg";
            String y = "JustCollege";


            cmd.CommandText = "SelectHRASUser";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;

            cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = x;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = y;

            con.Open();
            reader = cmd.ExecuteReader();

            String s = "";
            while(reader.Read())
            {
                s += reader.GetString(0);
                s += reader.GetString(1);
            }

            con.Close();