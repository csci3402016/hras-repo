USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/17/2016
-- Description:	
-- =============================================
CREATE PROCEDURE PatientInnerJoinMedicalRecord 
	-- Add the parameters for the stored procedure here
	@SSN							char(9)			= NULL			
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Patient
	INNER JOIN Medical_Record
	ON SSN = @SSN AND Patient_SSN = @SSN
END
GO
