USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 04/04/2016
-- Description:	Return symptom to be asked in the diagnosis wizard
-- =============================================
CREATE PROCEDURE GetSymptom 
	-- Add the parameters for the stored procedure here
	@Username					varchar(15)		= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @TotalRecords int
	SET @TotalRecords = (SELECT COUNT(*) FROM Medical_Record AS A
						WHERE (SELECT COUNT(*) FROM HRAS_User_Symptom AS B
								WHERE B.User_Username = @Username
								AND ((SELECT COUNT(*) FROM Medical_Record_Symptom AS C
								WHERE A.Patient_SSN = C.Med_Rec_Patient_SSN AND A.Entry_Date = C.Med_Rec_Entry_Date
								AND B.Symptom_Name = C.Symptom_Name) 
								!= Has_Symptom)) = 0)

	SELECT TOP(1) Symptom_Name, ABS(((CAST(COUNT(Symptom_Name) AS FLOAT) / @TotalRecords) - 0.5)) AS "Probability" 
	FROM (SELECT Patient_SSN, Entry_Date FROM Medical_Record AS A
	WHERE (SELECT COUNT(*) FROM HRAS_User_Symptom AS B
			WHERE B.User_Username = @Username
			AND ((SELECT COUNT(*) FROM Medical_Record_Symptom AS C
			WHERE A.Patient_SSN = C.Med_Rec_Patient_SSN AND A.Entry_Date = C.Med_Rec_Entry_Date
			AND B.Symptom_Name = C.Symptom_Name) 
			!= Has_Symptom)) = 0) AS Records
	INNER JOIN Medical_Record_Symptom
	ON Records.Patient_SSN = Med_Rec_Patient_SSN AND Records.Entry_Date = Med_Rec_Entry_Date
	WHERE Medical_Record_Symptom.Symptom_Name != ALL(SELECT Symptom_Name FROM HRAS_User_Symptom WHERE HRAS_User_Symptom.User_Username = @Username)
	GROUP BY Symptom_Name
	ORDER BY Probability ASC, Symptom_Name ASC
END
GO
