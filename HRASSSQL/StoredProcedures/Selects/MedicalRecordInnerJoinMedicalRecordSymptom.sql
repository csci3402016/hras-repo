USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/18/2016
-- Description:	
-- =============================================
CREATE PROCEDURE MedicalRecordInnerJoinMedicalRecordSymptom 
	-- Add the parameters for the stored procedure here
	@Patient_SSN				char(9)			= NULL,
	@Entry_Date					datetime		= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Medical_Record
	INNER JOIN Medical_Record_Symptom
	ON Patient_SSN = Med_Rec_Patient_SSN AND Entry_Date = Med_Rec_Entry_Date
	WHERE Patient_SSN = @Patient_SSN AND Entry_Date = @Entry_Date
END
GO
