USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/17/2016
-- Description:	Select patient by name or SSN
-- =============================================
CREATE PROCEDURE SelectPatientByNameOrSSN 
	-- Add the parameters for the stored procedure here
	@SSN						char(9)			= NULL,
	@First_Name					varchar(50)		= NULL,
	@Last_Name					varchar(25)		= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Patient
	WHERE SSN = @SSN OR First_Name = @First_Name OR Last_Name = @Last_Name
END
GO
