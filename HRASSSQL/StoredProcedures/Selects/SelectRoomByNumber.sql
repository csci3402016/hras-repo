USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pavle Bulatovic
-- Create date: 4/02/2016
-- Description:	Select room by number
-- =============================================
CREATE PROCEDURE SelectRoomByNumber 
	-- Add the parameters for the stored procedure here
	@Room_Number						varchar(9)			= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Room
	WHERE Room_Number = @Room_Number
END
GO
