USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 04/04/2016
-- Description:	Get n most probable diagnoses for the patient
-- =============================================
CREATE PROCEDURE GetMostProbableDiagnoses 
	-- Add the parameters for the stored procedure here
	@Username					varchar(15)		= NULL,
	@n							int				= 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @TotalRecords int
	SET @TotalRecords = (SELECT COUNT(*) FROM Medical_Record AS A
						WHERE (SELECT COUNT(*) FROM HRAS_User_Symptom AS B
								WHERE B.User_Username = @Username
								AND ((SELECT COUNT(*) FROM Medical_Record_Symptom AS C
								WHERE A.Patient_SSN = C.Med_Rec_Patient_SSN AND A.Entry_Date = C.Med_Rec_Entry_Date
								AND B.Symptom_Name = C.Symptom_Name) 
								!= Has_Symptom)) = 0)

	SELECT TOP(@n) Diagnosis, CAST(COUNT(Diagnosis) AS FLOAT) / @TotalRecords AS "Probability" FROM Medical_Record AS A
	WHERE (SELECT COUNT(*) FROM HRAS_User_Symptom AS B
			WHERE B.User_Username = @Username
			AND ((SELECT COUNT(*) FROM Medical_Record_Symptom AS C
			WHERE A.Patient_SSN = C.Med_Rec_Patient_SSN AND A.Entry_Date = C.Med_Rec_Entry_Date
			AND B.Symptom_Name = C.Symptom_Name) 
			!= Has_Symptom)) = 0
	AND Diagnosis IS NOT NULL
	GROUP BY Diagnosis
	ORDER BY Probability DESC
END
GO
