USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 04/09/2016
-- Description:	
-- =============================================
CREATE PROCEDURE SelectPatients 
	-- Add the parameters for the stored procedure here
	@SSN						char(9)			= NULL,
	@First_Name					varchar(50)		= NULL,
	@Last_Name					varchar(25)		= NULL,
	@Room_Number				varchar(9)		= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SSN, First_Name, Last_Name, Middle_Initial, Gender, Organ_Donor, Birth_Date, Address_Line_1, Address_Line_2, Address_City, Address_State, Address_Zip_Code, COUNT(Room_Number)
	FROM Patient LEFT JOIN Medical_Record
	ON SSN = Patient_SSN
	WHERE (SSN = @SSN OR @SSN IS NULL) AND (First_Name = @First_Name OR @First_Name IS NULL) AND (Last_Name = @Last_Name OR @Last_Name IS NULL) AND (Room_Number = @Room_Number OR @Room_Number IS NULL)
	GROUP BY SSN, First_Name, Last_Name, Middle_Initial, Gender, Organ_Donor, Birth_Date, Address_Line_1, Address_Line_2, Address_City, Address_State, Address_Zip_Code
END
GO
