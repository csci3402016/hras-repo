USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pavle Bulatovic
-- Create date: 4/7/2016
-- Description:	Select Item by Pk or Unique key
-- =============================================
CREATE PROCEDURE SelectInventoryItemByPKOrUniqueKey
	-- Add the parameters for the stored procedure here
	@Description				varchar(100)	= NULL,
	@Size						real            = NULL,
	@Stock_ID                   varchar(5)      = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Inventory_Item
	WHERE (Description = @Description AND Size = @Size) OR (Stock_ID = @Stock_ID)
END
GO
