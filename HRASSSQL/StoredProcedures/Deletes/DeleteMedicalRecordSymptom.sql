USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/27/2016
-- Description:	Delete Medical_Record_Symptom
-- =============================================
CREATE PROCEDURE DeleteMedicalRecordSymptom 
	-- Add the parameters for the stored procedure here
	@Symptom_Name				varchar(25)		= NULL,
	@Med_Rec_Patient_SSN		char(9)			= NULL,
	@Med_Rec_Entry_Date			datetime		= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Medical_Record_Symptom
	WHERE Symptom_Name = @Symptom_Name AND Med_Rec_Entry_Date = @Med_Rec_Entry_Date AND Med_Rec_Patient_SSN = @Med_Rec_Patient_SSN
END
GO
