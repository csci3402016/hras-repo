USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/15/2016
-- Description:	Insert InventoryItem
-- =============================================
CREATE PROCEDURE InsertInventoryItem 
	-- Add the parameters for the stored procedure here
	@Stock_ID					varchar(5)		 = NULL,
	@Quantity					int				 = NULL,
	@Description				varchar(100)	 = NULL,
	@Size						real			 = NULL,
	@Cost						real			 = NULL,
	@Is_Virtual					bit				 = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Inventory_Item VALUES (@Stock_ID, @Quantity, @Description, @Size, @Cost, @Is_Virtual)
END
GO
