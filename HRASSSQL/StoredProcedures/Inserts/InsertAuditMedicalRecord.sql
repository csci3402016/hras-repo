USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/15/2016
-- Description:	Insert AuditMedicalRecord
-- =============================================
CREATE PROCEDURE InsertAuditMedicalRecord 
	-- Add the parameters for the stored procedure here
	@Med_Rec_Patient_SSN		char(9)			 = NULL,
	@Med_Rec_Entry_Date			datetime		 = NULL,
	@Date_Changed				datetime		 = NULL,
	@User_Username				varchar(15)		 = NULL,
	@Field_Changed				varchar(8000)	 = NULL,
	@Previous_Value				varchar(8000)	 = NULL,
	@New_Value					varchar(8000)	 = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Audit_Medical_Record VALUES (@Med_Rec_Patient_SSN, @Med_Rec_Entry_Date, @Date_Changed, @User_Username, @Field_Changed, @Previous_Value, @New_Value)
END
GO
