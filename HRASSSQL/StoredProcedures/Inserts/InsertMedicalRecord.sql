USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/15/2016
-- Description:	Insert Medical_Record
-- =============================================
CREATE PROCEDURE InsertMedicalRecord 
	-- Add the parameters for the stored procedure here
	@Patient_SSN				char(9)			= NULL,
	@Entry_Date					datetime		= NULL,
	@Room_Number				varchar(9)		= NULL,
	@Exit_Date					datetime		= NULL,
	@Attending_Physician		varchar(100)	= NULL,
	@Diagnosis					varchar(200)	= NULL,
	@Notes						varchar(8000)	= NULL,
	@Insurer					varchar(50)		= NULL,
	@DNR						bit				= NULL		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Medical_Record VALUES (@Patient_SSN, @Entry_Date, @Room_Number, @Exit_Date, @Attending_Physician, @Diagnosis, @Notes, @Insurer, @DNR)
END
GO
