USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/15/2016
-- Description:	InsertPatient
-- =============================================
CREATE PROCEDURE InsertPatient 
	-- Add the parameters for the stored procedure here
	@SSN						char(9)			 = NULL,
	@First_Name					varchar(50)		 = NULL,
	@Last_Name					varchar(25)		 = NULL,
	@Middle_Initial				char(1)			 = NULL,
	@Gender						bit				 = NULL,
	@Organ_Donor				bit				 = NULL,
	@Birth_Date					date			 = NULL,
	@Address_Line_1				varchar(35)		 = NULL,
	@Address_Line_2				varchar(35)		 = NULL,
	@Address_City				varchar(25)		 = NULL,
	@Address_State				char(2)			 = NULL,
	@Address_Zip_Code			char(5)			 = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Patient VALUES (@SSN, @First_Name, @Last_Name, @Middle_Initial, @Gender, @Organ_Donor, @Birth_Date, @Address_Line_1, @Address_Line_2, @Address_City, @Address_State, @Address_Zip_Code)
END
GO
