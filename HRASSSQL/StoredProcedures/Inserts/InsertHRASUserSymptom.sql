USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 04/04/2016
-- Description:	
-- =============================================
CREATE PROCEDURE InsertHRASUserSymptom 
	-- Add the parameters for the stored procedure here
	@User_Username				varchar(15)		= NULL,
	@Symptom_Name				varchar(25)		= NULL,
	@Has_Symptom				bit				= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO HRAS_User_Symptom VALUES(@User_Username, @Symptom_Name, @Has_Symptom)
END
GO
