USE HRAS
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nikola Kuzmanovski
-- Create date: 3/15/2016
-- Description:	Insert RoomRate
-- =============================================
CREATE PROCEDURE InsertRoomRate 
	-- Add the parameters for the stored procedure here
	@Effective_Date				datetime		 = NULL,
	@Room_Number				varchar(9)		 = NULL,
	@Hourly_Rate				real			 = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Room_Rate VALUES (@Effective_Date, @Room_Number, @Hourly_Rate)
END
GO
