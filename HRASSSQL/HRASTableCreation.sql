USE HRAS

CREATE TABLE Patient
(
	SSN							char(9)			NOT NULL,
	First_Name					varchar(50)		NOT NULL,
	Last_Name					varchar(25)		NOT NULL,
	Middle_Initial				char(1)			NOT NULL,
	Gender						bit				NOT NULL,
	Organ_Donor					bit				NOT NULL,
	Birth_Date					date			NOT NULL,
	Address_Line_1				varchar(35)		NOT NULL,
	Address_Line_2				varchar(35)				,
	Address_City				varchar(25)		NOT NULL,
	Address_State				char(2)			NOT NULL,
	Address_Zip_Code			char(5)			NOT NULL,

	CONSTRAINT PK_Patient PRIMARY KEY (SSN)
)

CREATE TABLE Room
(
	Room_Number					varchar(9)		NOT NULL,
	CONSTRAINT PK_Room PRIMARY KEY (Room_Number)
)

CREATE TABLE Medical_Record
(
	Patient_SSN					char(9)			NOT NULL,
	Entry_Date					datetime		NOT NULL,
	Room_Number					varchar(9)		NOT NULL,
	Exit_Date					datetime				,
	Attending_Physician			varchar(100)	NOT NULL,
	Diagnosis					varchar(200)			,
	Notes						varchar(8000)			,
	Insurer						varchar(50)				,
	DNR							bit				NOT NULL,

	CONSTRAINT	PK_Medical_Record PRIMARY KEY (Patient_SSN, Entry_Date),
	CONSTRAINT	FK_Medical_Record_Patient FOREIGN KEY (Patient_SSN) REFERENCES Patient (SSN),
	CONSTRAINT	FK_Medical_Record_Room FOREIGN KEY(Room_Number) REFERENCES Room (Room_Number), 
)

CREATE TABLE Room_Rate
(
	Effective_Date				datetime		NOT NULL,
	Room_Number					varchar(9)		NOT NULL,
	Hourly_Rate					real			NOT NULL,

	CONSTRAINT PK_Room_Rate PRIMARY KEY (Effective_Date, Room_Number),
	CONSTRAINT FK_Room_Rate_Room FOREIGN KEY (Room_Number) REFERENCES Room (Room_Number)
)

CREATE TABLE Symptom
(
	Name						varchar(25)		NOT NULL,
	CONSTRAINT	PK_Symptom	PRIMARY KEY (Name)
)

CREATE TABLE Medical_Record_Symptom
(
	Symptom_Name				varchar(25)		NOT NULL,
	Med_Rec_Patient_SSN			char(9)			NOT NULL,
	Med_Rec_Entry_Date			datetime		NOT NULL,

	CONSTRAINT PK_Medical_Record_Symptom PRIMARY KEY (Med_Rec_Patient_SSN, Symptom_Name, Med_Rec_Entry_Date),
	CONSTRAINT FK_Medical_Record_Symptom_Medical_Record FOREIGN KEY (Med_Rec_Patient_SSN, Med_Rec_Entry_Date) REFERENCES Medical_Record (Patient_SSN, Entry_Date),
	CONSTRAINT FK_MEDICAL_Reocrd_Symptom_Symptom FOREIGN KEY (Symptom_Name) REFERENCES Symptom (Name)
)

CREATE TABLE HRAS_User
(
	Username					varchar(15)		NOT NULL,
	Password					varchar(35)		NOT NULL,
	Is_Inventory_Administator	bit				NOT NULL,

	CONSTRAINT PK_User PRIMARY KEY (Username)
)

CREATE TABLE Inventory_Item
(
	Stock_ID					varchar(5)		NOT NULL,
	Quantity					int				,
	Description					varchar(100)	NOT NULL,
	Size						real			NOT NULL,
	Cost						real			NOT NULL,
	Is_Virtual					bit				NOT NULL,
	
	CONSTRAINT PK_Inventory_Item PRIMARY KEY (Stock_ID),
	CONSTRAINT UN_Inventory_Item UNIQUE (Description, Size)						
)

CREATE TABLE Inventory_Usage
(
	Med_Rec_Patient_SSN			char(9)			NOT NULL,
	Med_Rec_Entry_Date			datetime		NOT NULL,
	Item_Stock_ID				varchar(5)		NOT NULL,
	Use_Date					datetime		NOT NULL, 
	User_Username				varchar(15)		NOT NULL,
	Quantity					int				NOT NULL,

	CONSTRAINT PK_Inventory_Usage PRIMARY KEY (Med_Rec_Patient_SSN, Med_Rec_Entry_Date, Item_Stock_ID, Use_Date),
	CONSTRAINT FK_Inventory_Usage_Medical_Record FOREIGN KEY (Med_Rec_Patient_SSN, Med_Rec_Entry_Date) REFERENCES Medical_Record (Patient_SSN, Entry_Date),
	CONSTRAINT FK_Inventory_Usage_HRAS_User FOREIGN KEY (User_Username) REFERENCES HRAS_User (Username),
	CONSTRAINT FK_Inventory_Usage_Inventory_Item FOREIGN KEY (Item_Stock_ID) REFERENCES Inventory_Item (Stock_ID)
)

CREATE TABLE Audit_Medical_Record
(
	Med_Rec_Patient_SSN			char(9)			NOT NULL,
	Med_Rec_Entry_Date			datetime		NOT NULL,
	Date_Changed				datetime		NOT NULL,
	User_Username				varchar(15)		NOT NULL,
	Field_Changed				varchar(200)	NOT NULL,
	Previous_Value				varchar(8000)	NOT NULL,
	New_Value					varchar(8000)	NOT NULL,

	CONSTRAINT PK_Audit_Medical_Record PRIMARY KEY (Med_Rec_Patient_SSN, Med_Rec_Entry_Date, Date_Changed, Field_Changed),
	CONSTRAINT FK_Audit_Medical_Record_Medical_Record FOREIGN KEY (Med_Rec_Patient_SSN, Med_Rec_Entry_Date) REFERENCES Medical_Record(Patient_SSN, Entry_Date),
	CONSTRAINT FK_Audit_Medical_Record_HRAS_User FOREIGN KEY (User_Username) REFERENCES HRAS_User (Username)
)

CREATE TABLE Medical_Record_Lock
(
	Med_Rec_Patient_SSN			char(9)			NOT NULL,
	Med_Rec_Entry_Date			datetime		NOT NULL,
	User_Username				varchar(15)		NOT NULL,
	Lock_Date					datetime		NOT NULL,

	CONSTRAINT PK_Medical_Record_Lock PRIMARY KEY (Med_Rec_Patient_SSN, Med_Rec_Entry_Date),
	CONSTRAINT FK_Medical_Record_Lock_Medical_Record FOREIGN KEY (Med_Rec_Patient_SSN, Med_Rec_Entry_Date) REFERENCES Medical_Record(Patient_SSN, Entry_Date),
	CONSTRAINT FK_Medical_Record_Lock_HRAS_User FOREIGN KEY (User_Username) REFERENCES HRAS_User (Username)
)

CREATE TABLE HRAS_User_Symptom
(
	User_Username				varchar(15)		NOT NULL,
	Symptom_Name				varchar(25)		NOT NULL,
	Has_Symptom					bit				NOT NULL,

	CONSTRAINT PK_HRAS_User_Symptom PRIMARY KEY (User_Username, Symptom_Name),
	CONSTRAINT FK_HRAS_User_Symptom_HRAS_User FOREIGN KEY (User_Username) REFERENCES HRAS_User(Username),
	CONSTRAINT FK_HRAS_User_Symptom_Symptom FOREIGN KEY (Symptom_Name) REFERENCES Symptom(Name)
)